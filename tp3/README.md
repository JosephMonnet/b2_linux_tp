# TP3 : systemd

<br>

## I. Services systemd

### 1. Intro

#### 🌞 Utilisez la ligne de commande pour sortir les infos suivantes :


- |CLI| afficher le nombre de services systemd dispos sur la machine

```
[vagrant@localhost ~]$ systemctl list-units --all --type=service | grep "units listed" | cut -d' ' -f1
107
```

- |CLI| afficher le nombre de services systemd actifs ("running") sur la machine

```
[vagrant@localhost ~]$ systemctl list-units --all --type=service --state=running | grep "units listed" | cut -d' ' -f1
17
```

- |CLI| afficher le nombre de services systemd qui ont échoué ("failed") ou qui sont inactifs ("exited") sur la machine

```
[vagrant@localhost ~]$ systemctl list-units --all --type=service --state=failed --state=exited | grep "units listed" | cut -d' ' -f1
18
```

- |CLI| afficher la liste des services systemd qui démarrent automatiquement au boot ("enabled")

```
[vagrant@localhost ~]$ sudo systemctl list-unit-files --all --type=service | grep "enabled"
auditd.service                                enabled
autovt@.service                               enabled
chronyd.service                               enabled
crond.service                                 enabled
dbus-org.freedesktop.nm-dispatcher.service    enabled
getty@.service                                enabled
irqbalance.service                            enabled
NetworkManager-dispatcher.service             enabled
NetworkManager-wait-online.service            enabled
NetworkManager.service                        enabled
postfix.service                               enabled
qemu-guest-agent.service                      enabled
rhel-autorelabel-mark.service                 enabled
rhel-autorelabel.service                      enabled
rhel-configure.service                        enabled
rhel-dmesg.service                            enabled
rhel-domainname.service                       enabled
rhel-import-state.service                     enabled
rhel-loadmodules.service                      enabled
rhel-readonly.service                         enabled
rpcbind.service                               enabled
rsyslog.service                               enabled
sshd.service                                  enabled
systemd-readahead-collect.service             enabled
systemd-readahead-drop.service                enabled
systemd-readahead-replay.service              enabled
tuned.service                                 enabled
vboxadd-service.service                       enabled
vboxadd.service                               enabled
vgauthd.service                               enabled
vmtoolsd-init.service                         enabled
vmtoolsd.service                              enabled
```

### 2. Analyse d'un service

#### 🌞 Etudiez le service nginx.service

- Déterminer le path de l'unité nginx.service

```
[vagrant@localhost ~]$ systemctl status nginx.service
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
   Active: inactive (dead)
```

- Afficher son contenu et expliquer les lignes qui comportent :

```
[vagrant@localhost ~]$ systemctl cat nginx.service
# /usr/lib/systemd/system/nginx.service
[Unit]
Description=The nginx HTTP and reverse proxy server
After=network.target remote-fs.target nss-lookup.target

[Service]
Type=forking
PIDFile=/run/nginx.pid
# Nginx will fail to start if /run/nginx.pid already exists but has the wrong
# SELinux context. This might happen when running `nginx -t` from the cmdline.
# https://bugzilla.redhat.com/show_bug.cgi?id=1268621
ExecStartPre=/usr/bin/rm -f /run/nginx.pid
ExecStartPre=/usr/sbin/nginx -t
ExecStart=/usr/sbin/nginx
ExecReload=/bin/kill -s HUP $MAINPID
KillSignal=SIGQUIT
TimeoutStopSec=5
KillMode=process
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```

<u> *ExecStart* </u>: Permet d'indiquer la commande à exécuter au lancement du service. Ce paramètre est obligatoire pour tout les types de service. <br>

<u> *ExecStartPre* </u> : C’est la commande qui sera exécutée avant ExecStart (on s’en serait douté !). Tous les processus bifurqués par des processus appelés via ExecStartPre seront tués avant l'exécution du prochain processus de service. <br>

<u> *PIDFile* </u> : Le PidFile est le nom du fichier dans lequel le serveur enregistre son identifiant de processus (PID). Le PID par défaut est /var/run/nginx.pid <br>

<u> *Type* </u> : Défini comment le processus va se lancer. Trois types existent, le type simple (lance un processus principal), le type forking (lance un processus parent et un processus fils, puis arrête le processus parent apèrs démarrage, type courant dans les système UNIX), et le type oneshot.<br>

<u> *ExecReload* </u> : Commande à executer lorsqu'on demande au service de se recharger avec un reload.<br>

<u> *Description* </u> :  Permet de donner une description du service qui apparaîtra lors de l'utilisation de la commande systemctl status <nom_du_service>. <br>

<u> *After* </u>: Permet d'indiquer quel pré-requis est nécessaire pour le fonctionnement du service. <br>

#### 🌞 |CLI| Listez tous les services qui contiennent la ligne WantedBy=multi-user.target

```
[vagrant@localhost ~]$ sudo find / -type f -name *service -exec grep -H 'WantedBy=multi-user.target' {} \; 2> /dev/null | cut -d ':' -f1
/etc/systemd/system/web_tp3.service
/usr/lib/systemd/system/rpcbind.service
/usr/lib/systemd/system/rdisc.service
/usr/lib/systemd/system/tcsd.service
/usr/lib/systemd/system/sshd.service
/usr/lib/systemd/system/rhel-configure.service
/usr/lib/systemd/system/rsyslog.service
/usr/lib/systemd/system/irqbalance.service
/usr/lib/systemd/system/cpupower.service
/usr/lib/systemd/system/crond.service
/usr/lib/systemd/system/rpc-rquotad.service
/usr/lib/systemd/system/wpa_supplicant.service
/usr/lib/systemd/system/chrony-wait.service
/usr/lib/systemd/system/chronyd.service
/usr/lib/systemd/system/NetworkManager.service
/usr/lib/systemd/system/ebtables.service
/usr/lib/systemd/system/gssproxy.service
/usr/lib/systemd/system/tuned.service
/usr/lib/systemd/system/firewalld.service
/usr/lib/systemd/system/nfs-server.service
/usr/lib/systemd/system/vboxadd-service.service
/usr/lib/systemd/system/rsyncd.service
/usr/lib/systemd/system/vmtoolsd.service
/usr/lib/systemd/system/postfix.service
/usr/lib/systemd/system/auditd.service
/usr/lib/systemd/system/nginx.service
/usr/lib/systemd/system/vboxadd.service
```

### 3. Création d'un service

#### A. Serveur web

🌞 Créez une unité de service qui lance un serveur web

```
[vagrant@localhost ~]$ cd /etc/systemd/system
[vagrant@localhost system]$ sudo touch web_tp3.service
[vagrant@localhost system]$ sudo vim web_tp3.service

[Unit]
Description=Server web Tp3
Require=firewalld.service

[Service]
Type=simple
ExecStartPre=/usr/bin/sudo /usr/bin/firewall-cmd --add-port=77/tcp --permanent
ExecStartPre=/usr/bin/sudo /usr/bin/firewall-cmd --reload
ExecStart=/usr/bin/python2 -m SimpleHTTPServer 8888
ExecStopPost=/usr/bin/sudo /usr/bin/firewall-cmd --remove-port=77/tcp --permanent
ExecStopPost=/usr/bin/sudo /usr/bin/firewall-cmd --reload

User=admin_tp3
Group=group_tp3
Environment=PORT=77
```

🌞 Lancer le service

*Prouver qu'il est en cours de fonctionnement pour systemd*

```
[vagrant@localhost system]$ sudo systemctl daemon-reload
[vagrant@localhost system]$ su admin_tp3
Password:
[admin_tp3@localhost system]$ sudo systemctl start web_tp3
[admin_tp3@localhost system]$ sudo systemctl status web_tp3
● web_tp3.service - Server web Tp3
   Loaded: loaded (/etc/systemd/system/web_tp3.service; static; vendor preset: disabled)
   Active: active (running) since Wed 2020-10-07 09:58:47 UTC; 20min ago
 Main PID: 3731 (python2)
   CGroup: /system.slice/web_tp3.service
           └─3731 /usr/bin/python2 -m SimpleHTTPServer 8888

Oct 07 09:58:44 localhost.localdomain systemd[1]: Starting Server web Tp3...
Oct 07 09:58:44 localhost.localdomain sudo[3698]: admin_tp3 : TTY=unknown ; PWD=/ ; USER=root ; COMMAND=/usr/bin/firewall-cmd --add-port=77/tcp --permanent
Oct 07 09:58:45 localhost.localdomain sudo[3702]: admin_tp3 : TTY=unknown ; PWD=/ ; USER=root ; COMMAND=/usr/bin/firewall-cmd --reload
Oct 07 09:58:47 localhost.localdomain systemd[1]: Started Server web Tp3.
Oct 07 10:00:00 localhost.localdomain systemd[1]: [/etc/systemd/system/web_tp3.service:3] Unknown lvalue 'Require' in section 'Unit'
```

*Prouver que le serveur web est bien fonctionnel*

```
[vagrant@localhost system]$ curl 192.168.3.11:8888
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN"><html>
<title>Directory listing for /</title>
<body>
<h2>Directory listing for /</h2>
<hr>
<ul>
<li><a href="bin/">bin@</a>
<li><a href="boot/">boot/</a>
<li><a href="dev/">dev/</a>
<li><a href="etc/">etc/</a>
<li><a href="home/">home/</a>
<li><a href="lib/">lib@</a>
<li><a href="lib64/">lib64@</a>
<li><a href="media/">media/</a>
<li><a href="mnt/">mnt/</a>
<li><a href="opt/">opt/</a>
<li><a href="proc/">proc/</a>
<li><a href="root/">root/</a>
<li><a href="run/">run/</a>
<li><a href="sbin/">sbin@</a>
<li><a href="srv/">srv/</a>
<li><a href="swapfile">swapfile</a>
<li><a href="sys/">sys/</a>
<li><a href="tmp/">tmp/</a>
<li><a href="usr/">usr/</a>
<li><a href="var/">var/</a>
<li><a href="VBox.log">VBox.log</a>
</ul>
<hr>
</body>
</html>
```

*Faites en sorte que le service s'allume au démarrage de la machine*

```
[admin_tp3@localhost system]$ cat web_tp3.service

[Unit]
Description=Server web Tp3
Requires=firewalld.service

[Service]
Type=simple
ExecStartPre=/usr/bin/sudo /usr/bin/firewall-cmd --add-port=8888/tcp --permanent
ExecStartPre=/usr/bin/sudo /usr/bin/firewall-cmd --reload
ExecStart=/usr/bin/python2 -m SimpleHTTPServer 8888
ExecStopPost=/usr/bin/sudo /usr/bin/firewall-cmd --remove-port=8888/tcp --permanent
ExecStopPost=/usr/bin/sudo /usr/bin/firewall-cmd --reload

User=admin_tp3
Group=group_tp3
Environment=PORT=8888

[Install]
WantedBy=multi-user.target

[admin_tp3@localhost system]$ sudo systemctl enable web_tp3.service
Created symlink from /etc/systemd/system/multi-user.target.wants/web_tp3.service to /etc/systemd/system/web_tp3.service.
[admin_tp3@localhost system]$ sudo systemctl daemon-reload
[admin_tp3@localhost system]$ sudo systemctl start web_tp3.service
[admin_tp3@localhost system]$ sudo systemctl status web_tp3.service
● web_tp3.service - Server web Tp3
   Loaded: loaded (/etc/systemd/system/web_tp3.service; enabled; vendor preset: disabled)
   Active: active (running) since Thu 2020-10-08 12:58:26 UTC; 1s ago
  Process: 3755 ExecStartPre=/usr/bin/sudo /usr/bin/firewall-cmd --reload (code=exited, status=0/SUCCESS)
  Process: 3751 ExecStartPre=/usr/bin/sudo /usr/bin/firewall-cmd --add-port=8888/tcp --permanent (code=exited, status=0/SUCCESS)
 Main PID: 3786 (python2)
   CGroup: /system.slice/web_tp3.service
           └─3786 /usr/bin/python2 -m SimpleHTTPServer 8888

Oct 08 12:58:25 localhost.localdomain systemd[1]: Starting Server web Tp3...
Oct 08 12:58:25 localhost.localdomain sudo[3751]: admin_tp3 : TTY=unknown ; PWD=/ ; USER=root ; COMMAND=/usr/bin/firewall-cmd --add-port=8888/tcp --permanent
Oct 08 12:58:25 localhost.localdomain sudo[3755]: admin_tp3 : TTY=unknown ; PWD=/ ; USER=root ; COMMAND=/usr/bin/firewall-cmd --reload
Oct 08 12:58:26 localhost.localdomain systemd[1]: Started Server web Tp3.
```

#### B. Sauvegarde

🌞 Créez une unité de service qui déclenche une sauvegarde avec votre script

* Un script qui se lance AVANT la sauvegarde, qui effectue les tests <br>

```
[admin_tp3@localhost ~]$ cat before_save.sh
#!/bin/bash

# Un script qui se lance AVANT la sauvegarde, qui effectue les test

# echo $$ > /var/run/dossier_pid/pid
if [ ! -d "/srv/site1/" ]
then
        echo "Le répertoire /srv/site1 n'existe pas !"
        exit 7
else
        mkdir -p /home/admin_tp3/site1_save
fi
```

* Un script de sauvegarde <br>

```
[admin_tp3@localhost ~]$ cat tp3_backup.sh
#!/bin/bash

echo $$ > /var/run/dossier_pid/pid
tar -czf /home/admin_tp3/site1_save/site1_$(date +"%Y%m%d_%H%M%S").tar.gz /srv/site1
```

* Un script qui s'exécute APRES la sauvegarde, et qui effectue la rotation (ne garder que les 7 sauvegardes les plus récentes) <br>

```
[admin_tp3@localhost ~]$ cat after_save.sh
#!/bin/bash

# Un script qui s'exécute APRES la sauvegarde, et qui effectue la rotation

## echo $$ > /var/run/dossier_pid/pid
if [[ $(ls -1 /home/admin_tp3/site1_save | wc -l) == 8 ]]
then
        rm "$(ls -t | tail -1)"
        echo "Supprimé le fichier le plus ancien, trop de sauvegardes"
fi
```

   * Une fois fait, utilisez les clauses ExecStartPre, ExecStart et ExecStartPost pour les lancer au bon moment

```
[admin_tp3@localhost system]$ cat backup_tp3.service
[Unit]
Description=Service qui va lancer un script de sauvegarde

[Service]
Environment="BEFORE_SCRIPT=/home/admin_tp3/before_save.sh"
Environment="SCRIPT=/home/admin_tp3/tp3_backup.sh"
Environment="AFTER_SCRIPT=/home/admin_tp3/after_save.sh"
User=admin_tp3
PIDFile=/var/run/dossier_pid/pid
ExecStartPre=/usr/bin/sh ${BEFORE_SCRIPT}
ExecStart=/usr/bin/sh ${SCRIPT}
ExecStartPost=/usr/bin/sh ${AFTER_SCRIPT}

[Install]
WantedBy=multi-user.target
```

<u>Lancement du service</u>

```
[admin_tp3@localhost system]$ sudo systemctl daemon-reload
[admin_tp3@localhost system]$ sudo systemctl start backup_tp3
[admin_tp3@localhost system]$ sudo systemctl status backup_tp3
● backup_tp3.service - Service qui va lancer un script de sauvegarde
   Loaded: loaded (/etc/systemd/system/backup_tp3.service; enabled; vendor preset: disabled)
   Active: inactive (dead) since Fri 2020-10-09 10:20:34 UTC; 4s ago
  Process: 4942 ExecStartPost=/usr/bin/sh ${AFTER_SCRIPT} (code=exited, status=0/SUCCESS)
  Process: 4941 ExecStart=/usr/bin/sh ${SCRIPT} (code=exited, status=0/SUCCESS)
  Process: 4939 ExecStartPre=/usr/bin/sh ${BEFORE_SCRIPT} (code=exited, status=0/SUCCESS)
 Main PID: 4941 (code=exited, status=0/SUCCESS)

Oct 09 10:20:34 localhost.localdomain systemd[1]: Starting Service qui va lancer un script de sauvegarde...
Oct 09 10:20:34 localhost.localdomain sh[4941]: tar: Removing leading `/' from member names
Oct 09 10:20:34 localhost.localdomain systemd[1]: Started Service qui va lancer un script de sauvegarde.
```

🌞 Ecrire un fichier .timer systemd

```
[vagrant@localhost system]$ cat backup_tp3.timer
[Unit]
Description=sauvegarde horaire
Requires=backup_tp3.service

[Timer]

Unit=backup_tp3.service
OnCalendar=0/1:00:00

[Install]
WantedBy=timers.target
```

<u>Lancement du service</u>

```
[vagrant@localhost system]$ sudo systemctl stop backup_tp3.service
[vagrant@localhost system]$ sudo systemctl start backup_tp3.timer
[vagrant@localhost system]$ sudo systemctl enable backup_tp3.timer
Created symlink from /etc/systemd/system/timers.target.wants/backup_tp3.timer to /etc/systemd/system/backup_tp3.timer.
[vagrant@localhost system]$ sudo systemctl status backup_tp3.timer
● backup_tp3.timer - sauvegarde horaire
   Loaded: loaded (/etc/systemd/system/backup_tp3.timer; enabled; vendor preset: disabled)
   Active: active (waiting) since Fri 2020-10-09 13:36:24 UTC; 25s ago

Oct 09 13:36:24 localhost.localdomain systemd[1]: Started sauvegarde horaire.
```

<br>

## II. Autres features

### 1. Gestion de boot

#### 🌞 Utilisez systemd-analyze plot pour récupérer une diagramme du boot, au format SVG

```
[vagrant@localhost system]$ systemd-analyze plot
<?xml version="1.0" standalone="no"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg width="1055px" height="2310px" version="1.1" xmlns="http://www.w3.org/2000/svg">

<!-- This file is a systemd-analyze SVG file. It is best rendered in a   -->
<!-- browser such as Chrome, Chromium or Firefox. Other applications     -->
<!-- that render these files properly but much slower are ImageMagick,   -->
<!-- gimp, inkscape, etc. To display the files on your system, just      -->
<!-- point your browser to this file.                                    -->

<!-- This plot was generated by systemd-analyze version 219              -->

<defs>
  <style type="text/css">
    <![CDATA[
      rect       { stroke-width: 1; stroke-opacity: 0; }
      rect.background   { fill: rgb(255,255,255); }
      rect.activating   { fill: rgb(255,0,0); fill-opacity: 0.7; }
      rect.active       { fill: rgb(200,150,150); fill-opacity: 0.7; }
      rect.deactivating { fill: rgb(150,100,100); fill-opacity: 0.7; }
      rect.kernel       { fill: rgb(150,150,150); fill-opacity: 0.7; }
      rect.initrd       { fill: rgb(150,150,150); fill-opacity: 0.7; }
      rect.firmware     { fill: rgb(150,150,150); fill-opacity: 0.7; }
      rect.loader       { fill: rgb(150,150,150); fill-opacity: 0.7; }
      rect.userspace    { fill: rgb(150,150,150); fill-opacity: 0.7; }
      rect.security     { fill: rgb(144,238,144); fill-opacity: 0.7; }
      rect.generators   { fill: rgb(102,204,255); fill-opacity: 0.7; }
      rect.unitsload    { fill: rgb( 82,184,255); fill-opacity: 0.7; }
      rect.box   { fill: rgb(240,240,240); stroke: rgb(192,192,192); }
      line       { stroke: rgb(64,64,64); stroke-width: 1; }
//    line.sec1  { }
      line.sec5  { stroke-width: 2; }
      line.sec01 { stroke: rgb(224,224,224); stroke-width: 1; }
      text       { font-family: Verdana, Helvetica; font-size: 14px; }
      text.left  { font-family: Verdana, Helvetica; font-size: 14px; text-anchor: start; }
      text.right { font-family: Verdana, Helvetica; font-size: 14px; text-anchor: end; }
      text.sec   { font-size: 10px; }
    ]]>
   </style>
</defs>

<rect class="background" width="100%" height="100%" />
<text x="20" y="50">Startup finished in 356ms (kernel) + 574ms (initrd) + 8.822s (userspace) = 9.753s</text><text x="20" y="30">CentOS Linux 7 (Core) localhost.localdomain (Linux 3.10.0-1127.19.1.el7.x86_64 #1 SMP Tue Aug 25 17:23:54 UTC 2020) x86-64 kvm</text><g transform="translate(20.000,100)">
<rect class="box" x="0" y="0" width="975.366" height="2060.000" />
  <line class="sec5" x1="0.000" y1="0" x2="0.000" y2="2060.000" />
  <text class="sec" x="0.000" y="-5.000" >0.0s</text>
  <line class="sec01" x1="10.000" y1="0" x2="10.000" y2="2060.000" />
  <line class="sec01" x1="20.000" y1="0" x2="20.000" y2="2060.000" />
  <line class="sec01" x1="30.000" y1="0" x2="30.000" y2="2060.000" />
  <line class="sec01" x1="40.000" y1="0" x2="40.000" y2="2060.000" />
  <line class="sec01" x1="50.000" y1="0" x2="50.000" y2="2060.000" />
  <line class="sec01" x1="60.000" y1="0" x2="60.000" y2="2060.000" />
  <line class="sec01" x1="70.000" y1="0" x2="70.000" y2="2060.000" />
  <line class="sec01" x1="80.000" y1="0" x2="80.000" y2="2060.000" />
  <line class="sec01" x1="90.000" y1="0" x2="90.000" y2="2060.000" />
  <line class="sec1" x1="100.000" y1="0" x2="100.000" y2="2060.000" />
  <text class="sec" x="100.000" y="-5.000" >1.0s</text>
  <line class="sec01" x1="110.000" y1="0" x2="110.000" y2="2060.000" />
  <line class="sec01" x1="120.000" y1="0" x2="120.000" y2="2060.000" />
  <line class="sec01" x1="130.000" y1="0" x2="130.000" y2="2060.000" />
  <line class="sec01" x1="140.000" y1="0" x2="140.000" y2="2060.000" />
  <line class="sec01" x1="150.000" y1="0" x2="150.000" y2="2060.000" />
  <line class="sec01" x1="160.000" y1="0" x2="160.000" y2="2060.000" />
  <line class="sec01" x1="170.000" y1="0" x2="170.000" y2="2060.000" />
  <line class="sec01" x1="180.000" y1="0" x2="180.000" y2="2060.000" />
  <line class="sec01" x1="190.000" y1="0" x2="190.000" y2="2060.000" />
  <line class="sec1" x1="200.000" y1="0" x2="200.000" y2="2060.000" />
  <text class="sec" x="200.000" y="-5.000" >2.0s</text>
  <line class="sec01" x1="210.000" y1="0" x2="210.000" y2="2060.000" />
  <line class="sec01" x1="220.000" y1="0" x2="220.000" y2="2060.000" />
  <line class="sec01" x1="230.000" y1="0" x2="230.000" y2="2060.000" />
  <line class="sec01" x1="240.000" y1="0" x2="240.000" y2="2060.000" />
  <line class="sec01" x1="250.000" y1="0" x2="250.000" y2="2060.000" />
  <line class="sec01" x1="260.000" y1="0" x2="260.000" y2="2060.000" />
  <line class="sec01" x1="270.000" y1="0" x2="270.000" y2="2060.000" />
  <line class="sec01" x1="280.000" y1="0" x2="280.000" y2="2060.000" />
  <line class="sec01" x1="290.000" y1="0" x2="290.000" y2="2060.000" />
  <line class="sec1" x1="300.000" y1="0" x2="300.000" y2="2060.000" />
  <text class="sec" x="300.000" y="-5.000" >3.0s</text>
  <line class="sec01" x1="310.000" y1="0" x2="310.000" y2="2060.000" />
  <line class="sec01" x1="320.000" y1="0" x2="320.000" y2="2060.000" />
  <line class="sec01" x1="330.000" y1="0" x2="330.000" y2="2060.000" />
  <line class="sec01" x1="340.000" y1="0" x2="340.000" y2="2060.000" />
  <line class="sec01" x1="350.000" y1="0" x2="350.000" y2="2060.000" />
  <line class="sec01" x1="360.000" y1="0" x2="360.000" y2="2060.000" />
  <line class="sec01" x1="370.000" y1="0" x2="370.000" y2="2060.000" />
  <line class="sec01" x1="380.000" y1="0" x2="380.000" y2="2060.000" />
  <line class="sec01" x1="390.000" y1="0" x2="390.000" y2="2060.000" />
  <line class="sec1" x1="400.000" y1="0" x2="400.000" y2="2060.000" />
  <text class="sec" x="400.000" y="-5.000" >4.0s</text>
  <line class="sec01" x1="410.000" y1="0" x2="410.000" y2="2060.000" />
  <line class="sec01" x1="420.000" y1="0" x2="420.000" y2="2060.000" />
  <line class="sec01" x1="430.000" y1="0" x2="430.000" y2="2060.000" />
  <line class="sec01" x1="440.000" y1="0" x2="440.000" y2="2060.000" />
  <line class="sec01" x1="450.000" y1="0" x2="450.000" y2="2060.000" />
  <line class="sec01" x1="460.000" y1="0" x2="460.000" y2="2060.000" />
  <line class="sec01" x1="470.000" y1="0" x2="470.000" y2="2060.000" />
  <line class="sec01" x1="480.000" y1="0" x2="480.000" y2="2060.000" />
  <line class="sec01" x1="490.000" y1="0" x2="490.000" y2="2060.000" />
  <line class="sec5" x1="500.000" y1="0" x2="500.000" y2="2060.000" />
  <text class="sec" x="500.000" y="-5.000" >5.0s</text>
  <line class="sec01" x1="510.000" y1="0" x2="510.000" y2="2060.000" />
  <line class="sec01" x1="520.000" y1="0" x2="520.000" y2="2060.000" />
  <line class="sec01" x1="530.000" y1="0" x2="530.000" y2="2060.000" />
  <line class="sec01" x1="540.000" y1="0" x2="540.000" y2="2060.000" />
  <line class="sec01" x1="550.000" y1="0" x2="550.000" y2="2060.000" />
  <line class="sec01" x1="560.000" y1="0" x2="560.000" y2="2060.000" />
  <line class="sec01" x1="570.000" y1="0" x2="570.000" y2="2060.000" />
  <line class="sec01" x1="580.000" y1="0" x2="580.000" y2="2060.000" />
  <line class="sec01" x1="590.000" y1="0" x2="590.000" y2="2060.000" />
  <line class="sec1" x1="600.000" y1="0" x2="600.000" y2="2060.000" />
  <text class="sec" x="600.000" y="-5.000" >6.0s</text>
  <line class="sec01" x1="610.000" y1="0" x2="610.000" y2="2060.000" />
  <line class="sec01" x1="620.000" y1="0" x2="620.000" y2="2060.000" />
  <line class="sec01" x1="630.000" y1="0" x2="630.000" y2="2060.000" />
  <line class="sec01" x1="640.000" y1="0" x2="640.000" y2="2060.000" />
  <line class="sec01" x1="650.000" y1="0" x2="650.000" y2="2060.000" />
  <line class="sec01" x1="660.000" y1="0" x2="660.000" y2="2060.000" />
  <line class="sec01" x1="670.000" y1="0" x2="670.000" y2="2060.000" />
  <line class="sec01" x1="680.000" y1="0" x2="680.000" y2="2060.000" />
  <line class="sec01" x1="690.000" y1="0" x2="690.000" y2="2060.000" />
  <line class="sec1" x1="700.000" y1="0" x2="700.000" y2="2060.000" />
  <text class="sec" x="700.000" y="-5.000" >7.0s</text>
  <line class="sec01" x1="710.000" y1="0" x2="710.000" y2="2060.000" />
  <line class="sec01" x1="720.000" y1="0" x2="720.000" y2="2060.000" />
  <line class="sec01" x1="730.000" y1="0" x2="730.000" y2="2060.000" />
  <line class="sec01" x1="740.000" y1="0" x2="740.000" y2="2060.000" />
  <line class="sec01" x1="750.000" y1="0" x2="750.000" y2="2060.000" />
  <line class="sec01" x1="760.000" y1="0" x2="760.000" y2="2060.000" />
  <line class="sec01" x1="770.000" y1="0" x2="770.000" y2="2060.000" />
  <line class="sec01" x1="780.000" y1="0" x2="780.000" y2="2060.000" />
  <line class="sec01" x1="790.000" y1="0" x2="790.000" y2="2060.000" />
  <line class="sec1" x1="800.000" y1="0" x2="800.000" y2="2060.000" />
  <text class="sec" x="800.000" y="-5.000" >8.0s</text>
  <line class="sec01" x1="810.000" y1="0" x2="810.000" y2="2060.000" />
  <line class="sec01" x1="820.000" y1="0" x2="820.000" y2="2060.000" />
  <line class="sec01" x1="830.000" y1="0" x2="830.000" y2="2060.000" />
  <line class="sec01" x1="840.000" y1="0" x2="840.000" y2="2060.000" />
  <line class="sec01" x1="850.000" y1="0" x2="850.000" y2="2060.000" />
  <line class="sec01" x1="860.000" y1="0" x2="860.000" y2="2060.000" />
  <line class="sec01" x1="870.000" y1="0" x2="870.000" y2="2060.000" />
  <line class="sec01" x1="880.000" y1="0" x2="880.000" y2="2060.000" />
  <line class="sec01" x1="890.000" y1="0" x2="890.000" y2="2060.000" />
  <line class="sec1" x1="900.000" y1="0" x2="900.000" y2="2060.000" />
  <text class="sec" x="900.000" y="-5.000" >9.0s</text>
  <line class="sec01" x1="910.000" y1="0" x2="910.000" y2="2060.000" />
  <line class="sec01" x1="920.000" y1="0" x2="920.000" y2="2060.000" />
  <line class="sec01" x1="930.000" y1="0" x2="930.000" y2="2060.000" />
  <line class="sec01" x1="940.000" y1="0" x2="940.000" y2="2060.000" />
  <line class="sec01" x1="950.000" y1="0" x2="950.000" y2="2060.000" />
  <line class="sec01" x1="960.000" y1="0" x2="960.000" y2="2060.000" />
  <line class="sec01" x1="970.000" y1="0" x2="970.000" y2="2060.000" />
  <rect class="kernel" x="0.000" y="0.000" width="35.639" height="19.000" />
  <text class="left" x="5.000" y="14.000">kernel</text>
  <rect class="initrd" x="35.639" y="20.000" width="57.497" height="19.000" />
  <text class="left" x="40.639" y="34.000">initrd</text>
  <rect class="active" x="93.137" y="40.000" width="882.229" height="19.000" />
  <rect class="security" x="93.508" y="40.000" width="0.216" height="19.000" />
  <rect class="generators" x="96.637" y="40.000" width="1.852" height="19.000" />
  <rect class="unitsload" x="98.588" y="40.000" width="8.087" height="19.000" />
  <text class="left" x="98.137" y="54.000">systemd</text>
  <rect class="activating" x="106.857" y="60.000" width="5.830" height="19.000" />
  <rect class="active" x="112.687" y="60.000" width="862.679" height="19.000" />
  <rect class="deactivating" x="975.366" y="60.000" width="0.000" height="19.000" />
  <text class="left" x="111.857" y="74.000">systemd-journald.service (58ms)</text>
  <rect class="activating" x="106.888" y="80.000" width="0.000" height="19.000" />
  <rect class="active" x="106.888" y="80.000" width="868.478" height="19.000" />
  <rect class="deactivating" x="975.366" y="80.000" width="0.000" height="19.000" />
  <text class="left" x="111.888" y="94.000">system-selinux\x2dpolicy\x2dmigrate\x2dlocal\x2dchanges.slice</text>
  <rect class="activating" x="106.909" y="100.000" width="3.386" height="19.000" />
  <rect class="active" x="110.295" y="100.000" width="865.070" height="19.000" />
  <rect class="deactivating" x="975.366" y="100.000" width="0.000" height="19.000" />
  <text class="left" x="111.909" y="114.000">sys-kernel-debug.mount (33ms)</text>
  <rect class="activating" x="106.913" y="120.000" width="0.000" height="19.000" />
  <rect class="active" x="106.913" y="120.000" width="868.453" height="19.000" />
  <rect class="deactivating" x="975.366" y="120.000" width="0.000" height="19.000" />
  <text class="left" x="111.913" y="134.000">systemd-ask-password-wall.path</text>
  <rect class="activating" x="106.918" y="140.000" width="0.000" height="19.000" />
  <rect class="active" x="106.918" y="140.000" width="868.447" height="19.000" />
  <rect class="deactivating" x="975.366" y="140.000" width="0.000" height="19.000" />
  <text class="left" x="111.918" y="154.000">systemd-udevd-kernel.socket</text>
  <rect class="activating" x="106.945" y="160.000" width="2.024" height="19.000" />
  <rect class="active" x="108.968" y="160.000" width="866.397" height="19.000" />
  <rect class="deactivating" x="975.366" y="160.000" width="0.000" height="19.000" />
  <text class="left" x="111.945" y="174.000">kmod-static-nodes.service (20ms)</text>
  <rect class="activating" x="106.982" y="180.000" width="3.315" height="19.000" />
  <rect class="active" x="110.298" y="180.000" width="865.068" height="19.000" />
  <rect class="deactivating" x="975.366" y="180.000" width="0.000" height="19.000" />
  <text class="left" x="111.982" y="194.000">dev-mqueue.mount (33ms)</text>
  <rect class="activating" x="106.986" y="200.000" width="0.000" height="19.000" />
  <rect class="active" x="106.986" y="200.000" width="868.380" height="19.000" />
  <rect class="deactivating" x="975.366" y="200.000" width="0.000" height="19.000" />
  <text class="left" x="111.986" y="214.000">systemd-udevd-control.socket</text>
  <rect class="activating" x="107.007" y="220.000" width="3.392" height="19.000" />
  <rect class="active" x="110.399" y="220.000" width="864.967" height="19.000" />
  <rect class="deactivating" x="975.366" y="220.000" width="0.000" height="19.000" />
  <text class="left" x="112.007" y="234.000">systemd-remount-fs.service (33ms)</text>
  <rect class="activating" x="107.034" y="240.000" width="60.466" height="19.000" />
  <rect class="active" x="167.500" y="240.000" width="807.866" height="19.000" />
  <rect class="deactivating" x="975.366" y="240.000" width="0.000" height="19.000" />
  <text class="left" x="112.034" y="254.000">systemd-vconsole-setup.service (604ms)</text>
  <rect class="activating" x="107.037" y="260.000" width="0.000" height="19.000" />
  <rect class="active" x="107.037" y="260.000" width="868.329" height="19.000" />
  <rect class="deactivating" x="975.366" y="260.000" width="0.000" height="19.000" />
  <text class="left" x="112.037" y="274.000">systemd-initctl.socket</text>
  <rect class="activating" x="107.043" y="280.000" width="0.000" height="19.000" />
  <rect class="active" x="107.043" y="280.000" width="868.323" height="19.000" />
  <rect class="deactivating" x="975.366" y="280.000" width="0.000" height="19.000" />
  <text class="left" x="112.043" y="294.000">user.slice</text>
  <rect class="activating" x="107.048" y="300.000" width="0.000" height="19.000" />
  <rect class="active" x="107.048" y="300.000" width="868.318" height="19.000" />
  <rect class="deactivating" x="975.366" y="300.000" width="0.000" height="19.000" />
  <text class="left" x="112.048" y="314.000">system-getty.slice</text>
  <rect class="activating" x="107.070" y="320.000" width="3.626" height="19.000" />
  <rect class="active" x="110.696" y="320.000" width="864.669" height="19.000" />
  <rect class="deactivating" x="975.366" y="320.000" width="0.000" height="19.000" />
  <text class="left" x="112.070" y="334.000">systemd-sysctl.service (36ms)</text>
  <rect class="activating" x="107.080" y="340.000" width="0.000" height="19.000" />
  <rect class="active" x="107.080" y="340.000" width="868.285" height="19.000" />
  <rect class="deactivating" x="975.366" y="340.000" width="0.000" height="19.000" />
  <text class="left" x="112.080" y="354.000">proc-sys-fs-binfmt_misc.automount</text>
  <rect class="activating" x="107.102" y="360.000" width="3.196" height="19.000" />
  <rect class="active" x="110.299" y="360.000" width="865.067" height="19.000" />
  <rect class="deactivating" x="975.366" y="360.000" width="0.000" height="19.000" />
  <text class="left" x="112.102" y="374.000">dev-hugepages.mount (31ms)</text>
  <rect class="activating" x="107.106" y="380.000" width="0.000" height="19.000" />
  <rect class="active" x="107.106" y="380.000" width="868.259" height="19.000" />
  <rect class="deactivating" x="975.366" y="380.000" width="0.000" height="19.000" />
  <text class="left" x="112.106" y="394.000">systemd-ask-password-console.path</text>
  <rect class="activating" x="107.111" y="400.000" width="0.000" height="19.000" />
  <rect class="active" x="107.111" y="400.000" width="868.255" height="19.000" />
  <rect class="deactivating" x="975.366" y="400.000" width="0.000" height="19.000" />
  <text class="left" x="112.111" y="414.000">paths.target</text>
  <rect class="activating" x="107.131" y="420.000" width="7.368" height="19.000" />
  <rect class="active" x="114.499" y="420.000" width="860.867" height="19.000" />
  <rect class="deactivating" x="975.366" y="420.000" width="0.000" height="19.000" />
  <text class="left" x="112.131" y="434.000">rhel-domainname.service (73ms)</text>
  <rect class="activating" x="107.133" y="440.000" width="0.000" height="19.000" />
  <rect class="active" x="107.133" y="440.000" width="868.233" height="19.000" />
  <rect class="deactivating" x="975.366" y="440.000" width="0.000" height="19.000" />
  <text class="left" x="112.133" y="454.000">cryptsetup.target</text>
  <rect class="activating" x="107.390" y="460.000" width="0.000" height="19.000" />
  <rect class="active" x="107.390" y="460.000" width="867.976" height="19.000" />
  <rect class="deactivating" x="975.366" y="460.000" width="0.000" height="19.000" />
  <text class="left" x="112.390" y="474.000">systemd-shutdownd.socket</text>
  <rect class="activating" x="107.394" y="480.000" width="0.000" height="19.000" />
  <rect class="active" x="107.394" y="480.000" width="867.971" height="19.000" />
  <rect class="deactivating" x="975.366" y="480.000" width="0.000" height="19.000" />
  <text class="left" x="112.394" y="494.000">slices.target</text>
  <rect class="activating" x="107.400" y="500.000" width="0.000" height="19.000" />
  <rect class="active" x="107.400" y="500.000" width="867.966" height="19.000" />
  <rect class="deactivating" x="975.366" y="500.000" width="0.000" height="19.000" />
  <text class="left" x="112.400" y="514.000">rpcbind.target</text>
  <rect class="activating" x="107.492" y="520.000" width="45.702" height="19.000" />
  <rect class="active" x="153.193" y="520.000" width="822.172" height="19.000" />
  <rect class="deactivating" x="975.366" y="520.000" width="0.000" height="19.000" />
  <text class="left" x="112.492" y="534.000">dev-sda1.device (457ms)</text>
  <rect class="activating" x="109.018" y="540.000" width="10.325" height="19.000" />
  <rect class="active" x="119.343" y="540.000" width="856.023" height="19.000" />
  <rect class="deactivating" x="975.366" y="540.000" width="0.000" height="19.000" />
  <text class="left" x="114.018" y="554.000">systemd-tmpfiles-setup-dev.service (103ms)</text>
  <rect class="activating" x="110.626" y="560.000" width="4.300" height="19.000" />
  <rect class="active" x="114.926" y="560.000" width="860.440" height="19.000" />
  <rect class="deactivating" x="975.366" y="560.000" width="0.000" height="19.000" />
  <text class="left" x="115.626" y="574.000">systemd-udev-trigger.service (42ms)</text>
  <rect class="activating" x="110.658" y="580.000" width="11.911" height="19.000" />
  <rect class="active" x="122.569" y="580.000" width="852.796" height="19.000" />
  <rect class="deactivating" x="975.366" y="580.000" width="0.000" height="19.000" />
  <text class="left" x="115.658" y="594.000">rhel-readonly.service (119ms)</text>
  <rect class="activating" x="110.677" y="600.000" width="70.829" height="19.000" />
  <rect class="active" x="181.506" y="600.000" width="793.860" height="19.000" />
  <rect class="deactivating" x="975.366" y="600.000" width="0.000" height="19.000" />
  <text class="left" x="115.677" y="614.000">swapfile.swap (708ms)</text>
  <rect class="activating" x="112.734" y="620.000" width="6.813" height="19.000" />
  <rect class="active" x="119.547" y="620.000" width="855.819" height="19.000" />
  <rect class="deactivating" x="975.366" y="620.000" width="0.000" height="19.000" />
  <text class="left" x="117.734" y="634.000">systemd-journal-flush.service (68ms)</text>
  <rect class="activating" x="119.374" y="640.000" width="6.149" height="19.000" />
  <rect class="active" x="125.523" y="640.000" width="849.843" height="19.000" />
  <rect class="deactivating" x="975.366" y="640.000" width="0.000" height="19.000" />
  <text class="left" x="124.374" y="654.000">systemd-udevd.service (61ms)</text>
  <rect class="activating" x="119.377" y="660.000" width="0.000" height="19.000" />
  <rect class="active" x="119.377" y="660.000" width="855.989" height="19.000" />
  <rect class="deactivating" x="975.366" y="660.000" width="0.000" height="19.000" />
  <text class="left" x="124.377" y="674.000">local-fs-pre.target</text>
  <rect class="activating" x="119.399" y="680.000" width="10.130" height="19.000" />
  <rect class="active" x="129.529" y="680.000" width="845.837" height="19.000" />
  <rect class="deactivating" x="975.366" y="680.000" width="0.000" height="19.000" />
  <text class="left" x="124.399" y="694.000">vagrant.mount (101ms)</text>
  <rect class="activating" x="122.600" y="700.000" width="1.530" height="19.000" />
  <rect class="active" x="124.130" y="700.000" width="851.236" height="19.000" />
  <rect class="deactivating" x="975.366" y="700.000" width="0.000" height="19.000" />
  <text class="left" x="127.600" y="714.000">systemd-random-seed.service (15ms)</text>
  <rect class="activating" x="122.603" y="720.000" width="0.000" height="19.000" />
  <rect class="active" x="122.603" y="720.000" width="852.763" height="19.000" />
  <rect class="deactivating" x="975.366" y="720.000" width="0.000" height="19.000" />
  <text class="left" x="127.603" y="734.000">local-fs.target</text>
  <rect class="activating" x="122.628" y="740.000" width="3.244" height="19.000" />
  <rect class="active" x="125.872" y="740.000" width="0.000" height="19.000" />
  <rect class="deactivating" x="125.872" y="740.000" width="0.000" height="19.000" />
  <text class="left" x="127.628" y="754.000">nfs-config.service (32ms)</text>
  <rect class="activating" x="122.907" y="760.000" width="2.308" height="19.000" />
  <rect class="active" x="125.214" y="760.000" width="850.151" height="19.000" />
  <rect class="deactivating" x="975.366" y="760.000" width="0.000" height="19.000" />
  <text class="left" x="127.907" y="774.000">systemd-tmpfiles-setup.service (23ms)</text>
  <rect class="activating" x="125.241" y="780.000" width="42.711" height="19.000" />
  <rect class="active" x="167.952" y="780.000" width="807.414" height="19.000" />
  <rect class="deactivating" x="975.366" y="780.000" width="0.000" height="19.000" />
  <text class="left" x="130.241" y="794.000">auditd.service (427ms)</text>
  <rect class="activating" x="125.330" y="800.000" width="27.520" height="19.000" />
  <rect class="active" x="152.849" y="800.000" width="822.517" height="19.000" />
  <rect class="deactivating" x="975.366" y="800.000" width="0.000" height="19.000" />
  <text class="left" x="130.329" y="814.000">var-lib-nfs-rpc_pipefs.mount (275ms)</text>
  <rect class="activating" x="127.986" y="820.000" width="0.000" height="19.000" />
  <rect class="active" x="127.986" y="820.000" width="847.379" height="19.000" />
  <rect class="deactivating" x="975.366" y="820.000" width="0.000" height="19.000" />
  <text class="left" x="132.986" y="834.000">sys-module-configfs.device</text>
  <rect class="activating" x="141.040" y="840.000" width="0.000" height="19.000" />
  <rect class="active" x="141.040" y="840.000" width="834.326" height="19.000" />
  <rect class="deactivating" x="975.366" y="840.000" width="0.000" height="19.000" />
  <text class="left" x="146.040" y="854.000">dev-ttyS0.device</text>
  <rect class="activating" x="141.040" y="860.000" width="0.000" height="19.000" />
  <rect class="active" x="141.040" y="860.000" width="834.325" height="19.000" />
  <rect class="deactivating" x="975.366" y="860.000" width="0.000" height="19.000" />
  <text class="left" x="146.040" y="874.000">sys-devices-platform-serial8250-tty-ttyS0.device</text>
  <rect class="activating" x="141.769" y="880.000" width="0.000" height="19.000" />
  <rect class="active" x="141.769" y="880.000" width="833.596" height="19.000" />
  <rect class="deactivating" x="975.366" y="880.000" width="0.000" height="19.000" />
  <text class="left" x="146.769" y="894.000">dev-ttyS1.device</text>
  <rect class="activating" x="141.769" y="900.000" width="0.000" height="19.000" />
  <rect class="active" x="141.769" y="900.000" width="833.596" height="19.000" />
  <rect class="deactivating" x="975.366" y="900.000" width="0.000" height="19.000" />
  <text class="left" x="146.769" y="914.000">sys-devices-platform-serial8250-tty-ttyS1.device</text>
  <rect class="activating" x="142.154" y="920.000" width="0.000" height="19.000" />
  <rect class="active" x="142.154" y="920.000" width="833.212" height="19.000" />
  <rect class="deactivating" x="975.366" y="920.000" width="0.000" height="19.000" />
  <text class="left" x="147.154" y="934.000">dev-ttyS3.device</text>
  <rect class="activating" x="142.154" y="940.000" width="0.000" height="19.000" />
  <rect class="active" x="142.154" y="940.000" width="833.212" height="19.000" />
  <rect class="deactivating" x="975.366" y="940.000" width="0.000" height="19.000" />
  <text class="left" x="147.154" y="954.000">sys-devices-platform-serial8250-tty-ttyS3.device</text>
  <rect class="activating" x="142.197" y="960.000" width="0.000" height="19.000" />
  <rect class="active" x="142.197" y="960.000" width="833.169" height="19.000" />
  <rect class="deactivating" x="975.366" y="960.000" width="0.000" height="19.000" />
  <text class="left" x="147.197" y="974.000">dev-ttyS2.device</text>
  <rect class="activating" x="142.197" y="980.000" width="0.000" height="19.000" />
  <rect class="active" x="142.197" y="980.000" width="833.169" height="19.000" />
  <rect class="deactivating" x="975.366" y="980.000" width="0.000" height="19.000" />
  <text class="left" x="147.197" y="994.000">sys-devices-platform-serial8250-tty-ttyS2.device</text>
  <rect class="activating" x="152.052" y="1000.000" width="0.000" height="19.000" />
  <rect class="active" x="152.052" y="1000.000" width="823.314" height="19.000" />
  <rect class="deactivating" x="975.366" y="1000.000" width="0.000" height="19.000" />
  <text class="left" x="157.052" y="1014.000">dev-disk-by\x2dpath-pci\x2d0000:00:01.1\x2data\x2d1.0.device</text>
  <rect class="activating" x="152.052" y="1020.000" width="0.000" height="19.000" />
  <rect class="active" x="152.052" y="1020.000" width="823.314" height="19.000" />
  <rect class="deactivating" x="975.366" y="1020.000" width="0.000" height="19.000" />
  <text class="left" x="157.052" y="1034.000">dev-disk-by\x2did-ata\x2dVBOX_HARDDISK_VBebe6a963\x2dbea0cad1.device</text>
  <rect class="activating" x="152.052" y="1040.000" width="0.000" height="19.000" />
  <rect class="active" x="152.052" y="1040.000" width="823.313" height="19.000" />
  <rect class="deactivating" x="975.366" y="1040.000" width="0.000" height="19.000" />
  <text class="left" x="157.052" y="1054.000">dev-sda.device</text>
  <rect class="activating" x="152.052" y="1060.000" width="0.000" height="19.000" />
  <rect class="active" x="152.052" y="1060.000" width="823.313" height="19.000" />
  <rect class="deactivating" x="975.366" y="1060.000" width="0.000" height="19.000" />
  <text class="left" x="157.052" y="1074.000">sys-devices-pci0000:00-0000:00:01.1-ata1-host0-target0:0:0-0:0:0:0-block-sda.device</text>
  <rect class="activating" x="152.859" y="1080.000" width="0.000" height="19.000" />
  <rect class="active" x="152.859" y="1080.000" width="822.507" height="19.000" />
  <rect class="deactivating" x="975.366" y="1080.000" width="0.000" height="19.000" />
  <text class="left" x="157.859" y="1094.000">rpc_pipefs.target</text>
  <rect class="activating" x="153.193" y="1100.000" width="0.000" height="19.000" />
  <rect class="active" x="153.193" y="1100.000" width="822.173" height="19.000" />
  <rect class="deactivating" x="975.366" y="1100.000" width="0.000" height="19.000" />
  <text class="left" x="158.193" y="1114.000">dev-disk-by\x2duuid-1c419d6c\x2d5064\x2d4a2b\x2d953c\x2d05b2c67edb15.device</text>
  <rect class="activating" x="153.193" y="1120.000" width="0.000" height="19.000" />
  <rect class="active" x="153.193" y="1120.000" width="822.172" height="19.000" />
  <rect class="deactivating" x="975.366" y="1120.000" width="0.000" height="19.000" />
  <text class="left" x="158.193" y="1134.000">dev-disk-by\x2dpath-pci\x2d0000:00:01.1\x2data\x2d1.0\x2dpart1.device</text>
  <rect class="activating" x="153.193" y="1140.000" width="0.000" height="19.000" />
  <rect class="active" x="153.193" y="1140.000" width="822.172" height="19.000" />
  <rect class="deactivating" x="975.366" y="1140.000" width="0.000" height="19.000" />
  <text class="left" x="158.193" y="1154.000">dev-disk-by\x2did-ata\x2dVBOX_HARDDISK_VBebe6a963\x2dbea0cad1\x2dpart1.device</text>
  <rect class="activating" x="153.193" y="1160.000" width="0.000" height="19.000" />
  <rect class="active" x="153.193" y="1160.000" width="822.172" height="19.000" />
  <rect class="deactivating" x="975.366" y="1160.000" width="0.000" height="19.000" />
  <text class="left" x="158.193" y="1174.000">sys-devices-pci0000:00-0000:00:01.1-ata1-host0-target0:0:0-0:0:0:0-block-sda-sda1.device</text>
  <rect class="activating" x="167.982" y="1180.000" width="1.370" height="19.000" />
  <rect class="active" x="169.351" y="1180.000" width="806.014" height="19.000" />
  <rect class="deactivating" x="975.366" y="1180.000" width="0.000" height="19.000" />
  <text class="left" x="172.982" y="1194.000">systemd-update-utmp.service (13ms)</text>
  <rect class="activating" x="181.511" y="1200.000" width="0.000" height="19.000" />
  <rect class="active" x="181.511" y="1200.000" width="793.855" height="19.000" />
  <rect class="deactivating" x="975.366" y="1200.000" width="0.000" height="19.000" />
  <text class="left" x="186.511" y="1214.000">swap.target</text>
  <rect class="activating" x="181.512" y="1220.000" width="0.000" height="19.000" />
  <rect class="active" x="181.512" y="1220.000" width="793.853" height="19.000" />
  <rect class="deactivating" x="975.366" y="1220.000" width="0.000" height="19.000" />
  <text class="left" x="186.512" y="1234.000">sysinit.target</text>
  <rect class="activating" x="181.515" y="1240.000" width="0.000" height="19.000" />
  <rect class="active" x="181.515" y="1240.000" width="793.851" height="19.000" />
  <rect class="deactivating" x="975.366" y="1240.000" width="0.000" height="19.000" />
  <text class="left" x="186.515" y="1254.000">rpcbind.socket</text>
  <rect class="activating" x="181.602" y="1260.000" width="23.155" height="19.000" />
  <rect class="active" x="204.757" y="1260.000" width="770.608" height="19.000" />
  <rect class="deactivating" x="975.366" y="1260.000" width="0.000" height="19.000" />
  <text class="left" x="186.602" y="1274.000">rpcbind.service (231ms)</text>
  <rect class="activating" x="181.605" y="1280.000" width="0.000" height="19.000" />
  <rect class="active" x="181.605" y="1280.000" width="793.760" height="19.000" />
  <rect class="deactivating" x="975.366" y="1280.000" width="0.000" height="19.000" />
  <text class="left" x="186.605" y="1294.000">systemd-tmpfiles-clean.timer</text>
  <rect class="activating" x="181.607" y="1300.000" width="0.000" height="19.000" />
  <rect class="active" x="181.607" y="1300.000" width="793.758" height="19.000" />
  <rect class="deactivating" x="975.366" y="1300.000" width="0.000" height="19.000" />
  <text class="left" x="186.607" y="1314.000">timers.target</text>
  <rect class="activating" x="181.610" y="1320.000" width="0.000" height="19.000" />
  <rect class="active" x="181.610" y="1320.000" width="793.756" height="19.000" />
  <rect class="deactivating" x="975.366" y="1320.000" width="0.000" height="19.000" />
  <text class="left" x="186.610" y="1334.000">dbus.socket</text>
  <rect class="activating" x="181.611" y="1340.000" width="0.000" height="19.000" />
  <rect class="active" x="181.611" y="1340.000" width="793.754" height="19.000" />
  <rect class="deactivating" x="975.366" y="1340.000" width="0.000" height="19.000" />
  <text class="left" x="186.611" y="1354.000">sockets.target</text>
  <rect class="activating" x="181.613" y="1360.000" width="0.000" height="19.000" />
  <rect class="active" x="181.613" y="1360.000" width="793.753" height="19.000" />
  <rect class="deactivating" x="975.366" y="1360.000" width="0.000" height="19.000" />
  <text class="left" x="186.613" y="1374.000">basic.target</text>
  <rect class="activating" x="181.641" y="1380.000" width="30.851" height="19.000" />
  <rect class="active" x="212.492" y="1380.000" width="762.874" height="19.000" />
  <rect class="deactivating" x="975.366" y="1380.000" width="0.000" height="19.000" />
  <text class="left" x="186.641" y="1394.000">polkit.service (308ms)</text>
  <rect class="activating" x="181.742" y="1400.000" width="0.000" height="19.000" />
  <rect class="active" x="181.742" y="1400.000" width="793.623" height="19.000" />
  <rect class="deactivating" x="975.366" y="1400.000" width="0.000" height="19.000" />
  <text class="left" x="186.742" y="1414.000">dbus.service</text>
  <rect class="activating" x="202.966" y="1420.000" width="9.698" height="19.000" />
  <rect class="active" x="212.664" y="1420.000" width="762.702" height="19.000" />
  <rect class="deactivating" x="975.366" y="1420.000" width="0.000" height="19.000" />
  <text class="left" x="207.966" y="1434.000">gssproxy.service (96ms)</text>
  <rect class="activating" x="203.413" y="1440.000" width="0.000" height="19.000" />
  <rect class="active" x="203.413" y="1440.000" width="3.831" height="19.000" />
  <rect class="deactivating" x="207.244" y="1440.000" width="0.000" height="19.000" />
  <text class="left" x="208.413" y="1454.000">irqbalance.service</text>
  <rect class="activating" x="203.452" y="1460.000" width="6.755" height="19.000" />
  <rect class="active" x="210.207" y="1460.000" width="765.158" height="19.000" />
  <rect class="deactivating" x="975.366" y="1460.000" width="0.000" height="19.000" />
  <text class="left" x="208.452" y="1474.000">rhel-dmesg.service (67ms)</text>
  <rect class="activating" x="203.473" y="1480.000" width="4.430" height="19.000" />
  <rect class="active" x="207.903" y="1480.000" width="767.463" height="19.000" />
  <rect class="deactivating" x="975.366" y="1480.000" width="0.000" height="19.000" />
  <text class="left" x="208.473" y="1494.000">systemd-logind.service (44ms)</text>
  <rect class="activating" x="203.493" y="1500.000" width="769.359" height="19.000" />
  <rect class="active" x="972.853" y="1500.000" width="2.513" height="19.000" />
  <rect class="deactivating" x="975.366" y="1500.000" width="0.000" height="19.000" />
  <text class="left" x="208.493" y="1514.000">vboxadd.service (7.693s)</text>
  <rect class="activating" x="204.688" y="1520.000" width="8.417" height="19.000" />
  <rect class="active" x="213.105" y="1520.000" width="762.261" height="19.000" />
  <rect class="deactivating" x="975.366" y="1520.000" width="0.000" height="19.000" />
  <text class="left" x="209.688" y="1534.000">chronyd.service (84ms)</text>
  <rect class="activating" x="212.647" y="1540.000" width="171.645" height="19.000" />
  <rect class="active" x="384.292" y="1540.000" width="591.074" height="19.000" />
  <rect class="deactivating" x="975.366" y="1540.000" width="0.000" height="19.000" />
  <text class="left" x="217.647" y="1554.000">firewalld.service (1.716s)</text>
  <rect class="activating" x="212.677" y="1560.000" width="0.000" height="19.000" />
  <rect class="active" x="212.677" y="1560.000" width="762.689" height="19.000" />
  <rect class="deactivating" x="975.366" y="1560.000" width="0.000" height="19.000" />
  <text class="left" x="217.677" y="1574.000">nfs-client.target</text>
  <rect class="activating" x="212.679" y="1580.000" width="0.000" height="19.000" />
  <rect class="active" x="212.679" y="1580.000" width="762.687" height="19.000" />
  <rect class="deactivating" x="975.366" y="1580.000" width="0.000" height="19.000" />
  <text class="left" x="217.679" y="1594.000">remote-fs-pre.target</text>
  <rect class="activating" x="212.680" y="1600.000" width="0.000" height="19.000" />
  <rect class="active" x="212.680" y="1600.000" width="762.685" height="19.000" />
  <rect class="deactivating" x="975.366" y="1600.000" width="0.000" height="19.000" />
  <text class="left" x="217.680" y="1614.000">remote-fs.target</text>
  <rect class="activating" x="212.708" y="1620.000" width="1.339" height="19.000" />
  <rect class="active" x="214.047" y="1620.000" width="761.319" height="19.000" />
  <rect class="deactivating" x="975.366" y="1620.000" width="0.000" height="19.000" />
  <text class="left" x="217.708" y="1634.000">systemd-user-sessions.service (13ms)</text>
  <rect class="activating" x="214.152" y="1640.000" width="0.000" height="19.000" />
  <rect class="active" x="214.152" y="1640.000" width="761.213" height="19.000" />
  <rect class="deactivating" x="975.366" y="1640.000" width="0.000" height="19.000" />
  <text class="left" x="219.152" y="1654.000">crond.service</text>
  <rect class="activating" x="214.192" y="1660.000" width="0.000" height="19.000" />
  <rect class="active" x="214.192" y="1660.000" width="761.174" height="19.000" />
  <rect class="deactivating" x="975.366" y="1660.000" width="0.000" height="19.000" />
  <text class="left" x="219.192" y="1674.000">getty@tty1.service</text>
  <rect class="activating" x="214.199" y="1680.000" width="0.000" height="19.000" />
  <rect class="active" x="214.199" y="1680.000" width="761.167" height="19.000" />
  <rect class="deactivating" x="975.366" y="1680.000" width="0.000" height="19.000" />
  <text class="left" x="219.199" y="1694.000">getty.target</text>
  <rect class="activating" x="384.302" y="1700.000" width="0.000" height="19.000" />
  <rect class="active" x="384.302" y="1700.000" width="591.063" height="19.000" />
  <rect class="deactivating" x="975.366" y="1700.000" width="0.000" height="19.000" />
  <text class="left" x="389.302" y="1714.000">network-pre.target</text>
  <rect class="activating" x="538.321" y="1720.000" width="0.000" height="19.000" />
  <rect class="active" x="538.321" y="1720.000" width="437.045" height="19.000" />
  <rect class="deactivating" x="975.366" y="1720.000" width="0.000" height="19.000" />
  <text class="right" x="533.321" y="1734.000">sys-devices-pci0000:00-0000:00:05.0-sound-card0.device</text>
  <rect class="activating" x="538.324" y="1740.000" width="0.000" height="19.000" />
  <rect class="active" x="538.324" y="1740.000" width="437.042" height="19.000" />
  <rect class="deactivating" x="975.366" y="1740.000" width="0.000" height="19.000" />
  <text class="right" x="533.324" y="1754.000">sound.target</text>
  <rect class="activating" x="616.677" y="1760.000" width="0.000" height="19.000" />
  <rect class="active" x="616.677" y="1760.000" width="358.689" height="19.000" />
  <rect class="deactivating" x="975.366" y="1760.000" width="0.000" height="19.000" />
  <text class="right" x="611.677" y="1774.000">sys-subsystem-net-devices-eth1.device</text>
  <rect class="activating" x="616.677" y="1780.000" width="0.000" height="19.000" />
  <rect class="active" x="616.677" y="1780.000" width="358.688" height="19.000" />
  <rect class="deactivating" x="975.366" y="1780.000" width="0.000" height="19.000" />
  <text class="right" x="611.677" y="1794.000">sys-devices-pci0000:00-0000:00:08.0-net-eth1.device</text>
  <rect class="activating" x="617.266" y="1800.000" width="0.000" height="19.000" />
  <rect class="active" x="617.266" y="1800.000" width="358.100" height="19.000" />
  <rect class="deactivating" x="975.366" y="1800.000" width="0.000" height="19.000" />
  <text class="right" x="612.266" y="1814.000">sys-subsystem-net-devices-eth0.device</text>
  <rect class="activating" x="617.266" y="1820.000" width="0.000" height="19.000" />
  <rect class="active" x="617.266" y="1820.000" width="358.099" height="19.000" />
  <rect class="deactivating" x="975.366" y="1820.000" width="0.000" height="19.000" />
  <text class="right" x="612.266" y="1834.000">sys-devices-pci0000:00-0000:00:03.0-net-eth0.device</text>
  <rect class="activating" x="820.579" y="1840.000" width="64.073" height="19.000" />
  <rect class="active" x="884.652" y="1840.000" width="90.714" height="19.000" />
  <rect class="deactivating" x="975.366" y="1840.000" width="0.000" height="19.000" />
  <text class="right" x="815.579" y="1854.000">network.service (640ms)</text>
  <rect class="activating" x="884.668" y="1860.000" width="0.000" height="19.000" />
  <rect class="active" x="884.668" y="1860.000" width="90.698" height="19.000" />
  <rect class="deactivating" x="975.366" y="1860.000" width="0.000" height="19.000" />
  <text class="right" x="879.668" y="1874.000">network.target</text>
  <rect class="activating" x="884.693" y="1880.000" width="76.895" height="19.000" />
  <rect class="active" x="961.588" y="1880.000" width="13.778" height="19.000" />
  <rect class="deactivating" x="975.366" y="1880.000" width="0.000" height="19.000" />
  <text class="right" x="879.693" y="1894.000">postfix.service (768ms)</text>
  <rect class="activating" x="884.795" y="1900.000" width="5.449" height="19.000" />
  <rect class="active" x="890.244" y="1900.000" width="85.122" height="19.000" />
  <rect class="deactivating" x="975.366" y="1900.000" width="0.000" height="19.000" />
  <text class="right" x="879.795" y="1914.000">sshd.service (54ms)</text>
  <rect class="activating" x="884.798" y="1920.000" width="0.000" height="19.000" />
  <rect class="active" x="884.798" y="1920.000" width="90.568" height="19.000" />
  <rect class="deactivating" x="975.366" y="1920.000" width="0.000" height="19.000" />
  <text class="right" x="879.798" y="1934.000">network-online.target</text>
  <rect class="activating" x="884.827" y="1940.000" width="1.369" height="19.000" />
  <rect class="active" x="886.196" y="1940.000" width="0.000" height="19.000" />
  <rect class="deactivating" x="886.196" y="1940.000" width="0.000" height="19.000" />
  <text class="right" x="879.827" y="1954.000">rpc-statd-notify.service (13ms)</text>
  <rect class="activating" x="884.857" y="1960.000" width="71.012" height="19.000" />
  <rect class="active" x="955.868" y="1960.000" width="19.497" height="19.000" />
  <rect class="deactivating" x="975.366" y="1960.000" width="0.000" height="19.000" />
  <text class="right" x="879.857" y="1974.000">tuned.service (710ms)</text>
  <rect class="activating" x="884.970" y="1980.000" width="3.778" height="19.000" />
  <rect class="active" x="888.748" y="1980.000" width="86.618" height="19.000" />
  <rect class="deactivating" x="975.366" y="1980.000" width="0.000" height="19.000" />
  <text class="right" x="879.970" y="1994.000">rsyslog.service (37ms)</text>
  <rect class="activating" x="972.895" y="2000.000" width="1.980" height="19.000" />
  <rect class="active" x="974.875" y="2000.000" width="0.491" height="19.000" />
  <rect class="deactivating" x="975.366" y="2000.000" width="0.000" height="19.000" />
  <text class="right" x="967.895" y="2014.000">vboxadd-service.service (19ms)</text>
  <rect class="activating" x="974.891" y="2020.000" width="0.000" height="19.000" />
  <rect class="active" x="974.891" y="2020.000" width="0.474" height="19.000" />
  <rect class="deactivating" x="975.366" y="2020.000" width="0.000" height="19.000" />
  <text class="right" x="969.891" y="2034.000">multi-user.target</text>
  <rect class="activating" x="974.932" y="2040.000" width="0.403" height="19.000" />
  <rect class="active" x="975.335" y="2040.000" width="0.000" height="19.000" />
  <rect class="deactivating" x="975.335" y="2040.000" width="0.000" height="19.000" />
  <text class="right" x="969.932" y="2054.000">systemd-update-utmp-runlevel.service (4ms)</text>
</g>
<g transform="translate(20,100)">
  <rect class="activating" x="0.000" y="2080.000" width="30.000" height="19.000" />
  <text class="left" x="45.000" y="2094.000">Activating</text>
  <rect class="active" x="0.000" y="2100.000" width="30.000" height="19.000" />
  <text class="left" x="45.000" y="2114.000">Active</text>
  <rect class="deactivating" x="0.000" y="2120.000" width="30.000" height="19.000" />
  <text class="left" x="45.000" y="2134.000">Deactivating</text>
  <rect class="security" x="0.000" y="2140.000" width="30.000" height="19.000" />
  <text class="left" x="45.000" y="2154.000">Setting up security module</text>
  <rect class="generators" x="0.000" y="2160.000" width="30.000" height="19.000" />
  <text class="left" x="45.000" y="2174.000">Generators</text>
  <rect class="unitsload" x="0.000" y="2180.000" width="30.000" height="19.000" />
  <text class="left" x="45.000" y="2194.000">Loading unit files</text>
</g>

</svg>
```

* Déterminer les 3 services les plus lents à démarrer <br>
        - vboxadd.service (7.693s) <br>
        - firewalld.service (1.716s) <br>
        - swapfile.swap (708ms)

### 2. Gestion de l'heure

#### 🌞 Utilisez la commande timedatectl

* Déterminer votre fuseau horaire (```Time zone```)
* Déterminer si vous êtes synchronisés avec un serveur NTP (```NTP synchronized```)

```
[vagrant@localhost ~]$ timedatectl
      Local time: Fri 2020-10-09 13:56:52 UTC
  Universal time: Fri 2020-10-09 13:56:52 UTC
        RTC time: Fri 2020-10-09 10:40:47
       Time zone: UTC (UTC, +0000)
     NTP enabled: yes
NTP synchronized: yes
 RTC in local TZ: no
      DST active: n/a
```

* Changer le fuseau horaire

```
[vagrant@localhost ~]$ timedatectl set-timezone Africa/Djibouti
==== AUTHENTICATING FOR org.freedesktop.timedate1.set-timezone ===
Authentication is required to set the system timezone.
Authenticating as: admin_tp3
Password:
==== AUTHENTICATION COMPLETE ===
[vagrant@localhost ~]$ timedatectl
timedatectl
      Local time: Fri 2020-10-09 17:02:23 EAT
  Universal time: Fri 2020-10-09 14:02:23 UTC
        RTC time: Fri 2020-10-09 10:46:17
       Time zone: Africa/Djibouti (EAT, +0300)
     NTP enabled: yes
NTP synchronized: yes
 RTC in local TZ: no
      DST active: n/a
```

### 3. Gestion des noms et de la résolution de noms

#### 🌞 Utilisez hostnamectl

* Déterminer votre hostname actuel (```Static hostname```)

```
[vagrant@localhost ~]$ hostnamectl
   Static hostname: localhost.localdomain
         Icon name: computer-vm
           Chassis: vm
        Machine ID: 0a80a8489901a94a89fd76eefbe31947
           Boot ID: d5edbe01e07240d19501e774ccf884d3
    Virtualization: kvm
  Operating System: CentOS Linux 7 (Core)
       CPE OS Name: cpe:/o:centos:centos:7
            Kernel: Linux 3.10.0-1127.19.1.el7.x86_64
      Architecture: x86-64
```

* Changer votre hostname

```
[vagrant@localhost ~]$ hostnamectl set-hostname ouchkiehost.ouchkiedomain
==== AUTHENTICATING FOR org.freedesktop.hostname1.set-static-hostname ===
Authentication is required to set the statically configured local host name, as well as the pretty host name.
Authenticating as: admin_tp3
Password:
==== AUTHENTICATION COMPLETE ===
[vagrant@localhost ~]$ hostnamectl
   Static hostname: ouchkiehost.ouchkiedomain
         Icon name: computer-vm
           Chassis: vm
        Machine ID: 0a80a8489901a94a89fd76eefbe31947
           Boot ID: d5edbe01e07240d19501e774ccf884d3
    Virtualization: kvm
  Operating System: CentOS Linux 7 (Core)
       CPE OS Name: cpe:/o:centos:centos:7
            Kernel: Linux 3.10.0-1127.19.1.el7.x86_64
      Architecture: x86-64
```