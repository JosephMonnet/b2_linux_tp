# TP2 : Déploiement automatisé

<br>

## I. Déploiement simple

<br>

### 🌞 Créer un Vagrantfile qui :

#### Utilise la box centos/7

```
PS C:\Users\josep\Desktop\studies\ynov\b2\linux\vagrant> cat .\Vagrantfile

Vagrant.configure("2")do|config|
  config.vm.box="centos/7"

  # Ajoutez cette ligne afin d'accÃ©lÃ©rer le dÃ©marrage de la VM (si une erreur 'vbguest' est levÃ©e, voir la note un peu plus bas)
  config.vbguest.auto_update = false

  # DÃ©sactive les updates auto qui peuvent ralentir le lancement de la machine
  config.vm.box_check_update = false

  # La ligne suivante permet de dÃ©sactiver le montage d'un dossier partagÃ© (ne marche pas tout le temps directement suivant vos OS, versions d'OS, etc.)
  config.vm.synced_folder ".", "/vagrant", disabled: true
end

PS C:\Users\josep\Desktop\studies\ynov\b2\linux\vagrant> vagrant up
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Box 'centos/7' could not be found. Attempting to find and install...
    default: Box Provider: virtualbox
    default: Box Version: >= 0
==> default: Loading metadata for box 'centos/7'
    default: URL: https://vagrantcloud.com/centos/7
==> default: Adding box 'centos/7' (v2004.01) for provider: virtualbox
    default: Downloading: https://vagrantcloud.com/centos/boxes/7/versions/2004.01/providers/virtualbox.box
Download redirected to host: cloud.centos.org
    default:
    default: Calculating and comparing box checksum...
==> default: Successfully added box 'centos/7' (v2004.01) for 'virtualbox'!
There are errors in the configuration of this machine. Please fix
the following errors and try again:

Vagrant:
* Unknown configuration section 'vbguest'.

PS C:\Users\josep\Desktop\studies\ynov\b2\linux\vagrant> vagrant plugin install vagrant-vbguest
Installing the 'vagrant-vbguest' plugin. This can take a few minutes...
Fetching micromachine-3.0.0.gem
Fetching vagrant-vbguest-0.25.0.gem
Installed the plugin 'vagrant-vbguest (0.25.0)'!
PS C:\Users\josep\Desktop\studies\ynov\b2\linux\vagrant> vagrant up
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Importing base box 'centos/7'...
==> default: Matching MAC address for NAT networking...
==> default: Setting the name of the VM: vagrant_default_1601364863176_60118
==> default: Clearing any previously set network interfaces...
==> default: Preparing network interfaces based on configuration...
    default: Adapter 1: nat
==> default: Forwarding ports...
    default: 22 (guest) => 2222 (host) (adapter 1)
==> default: Booting VM...
==> default: Waiting for machine to boot. This may take a few minutes...
    default: SSH address: 127.0.0.1:2222
    default: SSH username: vagrant
    default: SSH auth method: private key
    default: Warning: Connection reset. Retrying...
    default: Warning: Connection aborted. Retrying...
    default:
    default: Vagrant insecure key detected. Vagrant will automatically replace
    default: this with a newly generated keypair for better security.
    default:
    default: Inserting generated public key within guest...
    default: Removing insecure key from the guest if it's present...
    default: Key inserted! Disconnecting and reconnecting using new SSH key...
==> default: Machine booted and ready!
==> default: Checking for guest additions in VM...
    default: No guest additions were detected on the base box for this VM! Guest
    default: additions are required for forwarded ports, shared folders, host only
    default: networking, and more. If SSH fails on this machine, please install
    default: the guest additions and repackage the box to continue.
    default:
    default: This is not an error message; everything may continue to work properly,
    default: in which case you may ignore this message.
PS C:\Users\josep\Desktop\studies\ynov\b2\linux\vagrant> vagrant status
Current machine states:

default                   running (virtualbox)

The VM is running. To stop this VM, you can run `vagrant halt` to
shut it down forcefully, or you can run `vagrant suspend` to simply
suspend the virtual machine. In either case, to restart it again,
simply run `vagrant up`.
```

#### Crée une seule VM

* 1Go RAM
* Ajout d'une IP statique 192.168.2.11/24
* Définition d'un nom (vagrantvm_tp2)
* Définition d'un hostname

*Vagrantfile*

```
Vagrant.configure("2")do|config|
  config.vm.box="centos/7"

  # Ajoutez cette ligne afin d'accélérer le démarrage de la VM (si une erreur 'vbguest' est levée, voir la note un peu plus bas)
  config.vbguest.auto_update = false

  # Désactive les updates auto qui peuvent ralentir le lancement de la machine
  config.vm.box_check_update = false 

  # La ligne suivante permet de désactiver le montage d'un dossier partagé (ne marche pas tout le temps directement suivant vos OS, versions d'OS, etc.)
  config.vm.synced_folder ".", "/vagrant", disabled: true

  config.vm.provider :virtualbox do |vb|
  # Configure 1G de Ram sur la VM
    vb.customize ["modifyvm", :id, "--memory", "1024"]
    vb.name = "vagrantvm_tp2"
  end

  # Définition d'un hostname
  config.vm.hostname = "myhost.local"

  # Définition d'une IP
  config.vm.network "private_network", ip: "192.168.2.11"
end
```

### 🌞 Modifier le Vagrantfile

#### La machine exécute un script shell au démarrage qui installe le paquet vim

*Script*

```
#!/bin/bash

sudo yum install vim
```


*Ajout de l'appel du script dans le vagrantfile*

```
[...]

  # Appel du script
  config.vm.provision "shell", path: "script.sh"
end

[...]
```

#### Ajout d'un deuxième disque de 5Go à la VM

*Ajout du disque de 5 Go dans le vagrantfile*

```
Vagrant.configure("2")do|config|
  config.vm.box="centos7-custom"
  file_to_disk = 'VagrantDisk.vdi'

[...]

  config.vm.provider :virtualbox do |vb|
  # Configure 1G de Ram sur la VM
    vb.customize ["modifyvm", :id, "--memory", "1024"]
    vb.name = "centos7-custom"
    vb.customize ['createhd', '--filename', file_to_disk, '--size', 5 * 1024]
    vb.customize ['storageattach', :id, '--storagectl', 'IDE', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', file_to_disk]
  end

[...]

PS C:\Users\josep\Desktop\studies\ynov\b2\linux\vagrant> vagrant ssh
Last login: Tue Sep 29 10:16:54 2020 from 10.0.2.2
[vagrant@myhost ~]$ lsblk
NAME   MAJ:MIN RM SIZE RO TYPE MOUNTPOINT
sda      8:0    0  40G  0 disk
└─sda1   8:1    0  40G  0 part /
sdb      8:16   0   5G  0 disk
```

<br>

## II. Re-package

<br>

### 🌞 Repackager une box, que vous appelerez b2-tp2-centos en partant de la box centos/7, qui comprend : 

* Une mise à jour système yum update

```
[vagrant@myhost ~]$ sudo yum update
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: centos.mirror.ate.info
 * extras: centos.mirror.ate.info
 * updates: centos.mirror.ate.info
Resolving Dependencies
--> Running transaction check
---> Package bind-export-libs.x86_64 32:9.11.4-16.P2.el7_8.2 will be updated
---> Package bind-export-libs.x86_64 32:9.11.4-16.P2.el7_8.6 will be an update
---> Package binutils.x86_64 0:2.27-43.base.el7 will be updated
---> Package binutils.x86_64 0:2.27-43.base.el7_8.1 will be an update
---> Package ca-certificates.noarch 0:2019.2.32-76.el7_7 will be updated
---> Package ca-certificates.noarch 0:2020.2.41-70.0.el7_8 will be an update
---> Package curl.x86_64 0:7.29.0-57.el7 will be updated
---> Package curl.x86_64 0:7.29.0-57.el7_8.1 will be an update
---> Package dbus.x86_64 1:1.10.24-13.el7_6 will be updated
---> Package dbus.x86_64 1:1.10.24-14.el7_8 will be an update
---> Package dbus-libs.x86_64 1:1.10.24-13.el7_6 will be updated
---> Package dbus-libs.x86_64 1:1.10.24-14.el7_8 will be an update
---> Package device-mapper.x86_64 7:1.02.164-7.el7_8.1 will be updated
---> Package device-mapper.x86_64 7:1.02.164-7.el7_8.2 will be an update
---> Package device-mapper-libs.x86_64 7:1.02.164-7.el7_8.1 will be updated
---> Package device-mapper-libs.x86_64 7:1.02.164-7.el7_8.2 will be an update
---> Package grub2.x86_64 1:2.02-0.81.el7.centos will be updated
---> Package grub2.x86_64 1:2.02-0.86.el7.centos will be an update
---> Package grub2-common.noarch 1:2.02-0.81.el7.centos will be updated
---> Package grub2-common.noarch 1:2.02-0.86.el7.centos will be an update
---> Package grub2-pc.x86_64 1:2.02-0.81.el7.centos will be updated
---> Package grub2-pc.x86_64 1:2.02-0.86.el7.centos will be an update
---> Package grub2-pc-modules.noarch 1:2.02-0.81.el7.centos will be updated
---> Package grub2-pc-modules.noarch 1:2.02-0.86.el7.centos will be an update
---> Package grub2-tools.x86_64 1:2.02-0.81.el7.centos will be updated
---> Package grub2-tools.x86_64 1:2.02-0.86.el7.centos will be an update
---> Package grub2-tools-extra.x86_64 1:2.02-0.81.el7.centos will be updated
---> Package grub2-tools-extra.x86_64 1:2.02-0.86.el7.centos will be an update
---> Package grub2-tools-minimal.x86_64 1:2.02-0.81.el7.centos will be updated
---> Package grub2-tools-minimal.x86_64 1:2.02-0.86.el7.centos will be an update
---> Package kernel.x86_64 0:3.10.0-1127.19.1.el7 will be installed
---> Package kernel-tools.x86_64 0:3.10.0-1127.el7 will be updated
---> Package kernel-tools.x86_64 0:3.10.0-1127.19.1.el7 will be an update
---> Package kernel-tools-libs.x86_64 0:3.10.0-1127.el7 will be updated
---> Package kernel-tools-libs.x86_64 0:3.10.0-1127.19.1.el7 will be an update
---> Package libcurl.x86_64 0:7.29.0-57.el7 will be updated
---> Package libcurl.x86_64 0:7.29.0-57.el7_8.1 will be an update
---> Package libwbclient.x86_64 0:4.10.4-10.el7 will be updated
---> Package libwbclient.x86_64 0:4.10.4-11.el7_8 will be an update
---> Package nfs-utils.x86_64 1:1.3.0-0.66.el7 will be updated
---> Package nfs-utils.x86_64 1:1.3.0-0.66.el7_8 will be an update
---> Package open-vm-tools.x86_64 0:10.3.10-2.el7 will be updated
---> Package open-vm-tools.x86_64 0:10.3.10-2.el7_8.1 will be an update
---> Package python-perf.x86_64 0:3.10.0-1127.el7 will be updated
---> Package python-perf.x86_64 0:3.10.0-1127.19.1.el7 will be an update
---> Package rsyslog.x86_64 0:8.24.0-52.el7 will be updated
---> Package rsyslog.x86_64 0:8.24.0-52.el7_8.2 will be an update
---> Package samba-client-libs.x86_64 0:4.10.4-10.el7 will be updated
---> Package samba-client-libs.x86_64 0:4.10.4-11.el7_8 will be an update
---> Package samba-common.noarch 0:4.10.4-10.el7 will be updated
---> Package samba-common.noarch 0:4.10.4-11.el7_8 will be an update
---> Package samba-common-libs.x86_64 0:4.10.4-10.el7 will be updated
---> Package samba-common-libs.x86_64 0:4.10.4-11.el7_8 will be an update
---> Package samba-libs.x86_64 0:4.10.4-10.el7 will be updated
---> Package samba-libs.x86_64 0:4.10.4-11.el7_8 will be an update
---> Package selinux-policy.noarch 0:3.13.1-266.el7 will be updated
---> Package selinux-policy.noarch 0:3.13.1-266.el7_8.1 will be an update
---> Package selinux-policy-targeted.noarch 0:3.13.1-266.el7 will be updated
---> Package selinux-policy-targeted.noarch 0:3.13.1-266.el7_8.1 will be an update
---> Package systemd.x86_64 0:219-73.el7_8.5 will be updated
---> Package systemd.x86_64 0:219-73.el7_8.9 will be an update
---> Package systemd-libs.x86_64 0:219-73.el7_8.5 will be updated
---> Package systemd-libs.x86_64 0:219-73.el7_8.9 will be an update
---> Package systemd-sysv.x86_64 0:219-73.el7_8.5 will be updated
---> Package systemd-sysv.x86_64 0:219-73.el7_8.9 will be an update
---> Package yum-plugin-fastestmirror.noarch 0:1.1.31-53.el7 will be updated
---> Package yum-plugin-fastestmirror.noarch 0:1.1.31-54.el7_8 will be an update
---> Package yum-utils.noarch 0:1.1.31-53.el7 will be updated
---> Package yum-utils.noarch 0:1.1.31-54.el7_8 will be an update
--> Finished Dependency Resolution

Dependencies Resolved

=============================================================================================================================================================================================================================================
 Package                                                           Arch                                            Version                                                            Repository                                        Size
=============================================================================================================================================================================================================================================
Installing:
 kernel                                                            x86_64                                          3.10.0-1127.19.1.el7                                               updates                                           50 M
Updating:
 bind-export-libs                                                  x86_64                                          32:9.11.4-16.P2.el7_8.6                                            updates                                          1.1 M
 binutils                                                          x86_64                                          2.27-43.base.el7_8.1                                               updates                                          5.9 M
 ca-certificates                                                   noarch                                          2020.2.41-70.0.el7_8                                               updates                                          382 k
 curl                                                              x86_64                                          7.29.0-57.el7_8.1                                                  updates                                          271 k
 dbus                                                              x86_64                                          1:1.10.24-14.el7_8                                                 updates                                          245 k
 dbus-libs                                                         x86_64                                          1:1.10.24-14.el7_8                                                 updates                                          169 k
 device-mapper                                                     x86_64                                          7:1.02.164-7.el7_8.2                                               updates                                          295 k
 device-mapper-libs                                                x86_64                                          7:1.02.164-7.el7_8.2                                               updates                                          324 k
 grub2                                                             x86_64                                          1:2.02-0.86.el7.centos                                             updates                                           32 k
 grub2-common                                                      noarch                                          1:2.02-0.86.el7.centos                                             updates                                          729 k
 grub2-pc                                                          x86_64                                          1:2.02-0.86.el7.centos                                             updates                                           32 k
 grub2-pc-modules                                                  noarch                                          1:2.02-0.86.el7.centos                                             updates                                          850 k
 grub2-tools                                                       x86_64                                          1:2.02-0.86.el7.centos                                             updates                                          1.8 M
 grub2-tools-extra                                                 x86_64                                          1:2.02-0.86.el7.centos                                             updates                                          1.0 M
 grub2-tools-minimal                                               x86_64                                          1:2.02-0.86.el7.centos                                             updates                                          174 k
 kernel-tools                                                      x86_64                                          3.10.0-1127.19.1.el7                                               updates                                          8.1 M
 kernel-tools-libs                                                 x86_64                                          3.10.0-1127.19.1.el7                                               updates                                          8.0 M
 libcurl                                                           x86_64                                          7.29.0-57.el7_8.1                                                  updates                                          223 k
 libwbclient                                                       x86_64                                          4.10.4-11.el7_8                                                    updates                                          113 k
 nfs-utils                                                         x86_64                                          1:1.3.0-0.66.el7_8                                                 updates                                          412 k
 open-vm-tools                                                     x86_64                                          10.3.10-2.el7_8.1                                                  updates                                          674 k
 python-perf                                                       x86_64                                          3.10.0-1127.19.1.el7                                               updates                                          8.1 M
 rsyslog                                                           x86_64                                          8.24.0-52.el7_8.2                                                  updates                                          621 k
 samba-client-libs                                                 x86_64                                          4.10.4-11.el7_8                                                    updates                                          5.0 M
 samba-common                                                      noarch                                          4.10.4-11.el7_8                                                    updates                                          212 k
 samba-common-libs                                                 x86_64                                          4.10.4-11.el7_8                                                    updates                                          176 k
 samba-libs                                                        x86_64                                          4.10.4-11.el7_8                                                    updates                                          271 k
 selinux-policy                                                    noarch                                          3.13.1-266.el7_8.1                                                 updates                                          497 k
 selinux-policy-targeted                                           noarch                                          3.13.1-266.el7_8.1                                                 updates                                          7.0 M
 systemd                                                           x86_64                                          219-73.el7_8.9                                                     updates                                          5.1 M
 systemd-libs                                                      x86_64                                          219-73.el7_8.9                                                     updates                                          416 k
 systemd-sysv                                                      x86_64                                          219-73.el7_8.9                                                     updates                                           94 k
 yum-plugin-fastestmirror                                          noarch                                          1.1.31-54.el7_8                                                    updates                                           34 k
 yum-utils                                                         noarch                                          1.1.31-54.el7_8                                                    updates                                          122 k

Transaction Summary
=============================================================================================================================================================================================================================================
Install   1 Package
Upgrade  34 Packages

Total download size: 108 M
Is this ok [y/d/N]: y
Downloading packages:
No Presto metadata available for updates
warning: /var/cache/yum/x86_64/7/updates/packages/curl-7.29.0-57.el7_8.1.x86_64.rpm: Header V3 RSA/SHA256 Signature, key ID f4a80eb5: NOKEY                                                                ] 403 kB/s | 1.5 MB  00:04:31 ETA
Public key for curl-7.29.0-57.el7_8.1.x86_64.rpm is not installed
(1/35): curl-7.29.0-57.el7_8.1.x86_64.rpm                                                                                                                                                                             | 271 kB  00:00:02
(2/35): bind-export-libs-9.11.4-16.P2.el7_8.6.x86_64.rpm                                                                                                                                                              | 1.1 MB  00:00:02
(3/35): ca-certificates-2020.2.41-70.0.el7_8.noarch.rpm                                                                                                                                                               | 382 kB  00:00:02
(4/35): device-mapper-1.02.164-7.el7_8.2.x86_64.rpm                                                                                                                                                                   | 295 kB  00:00:00
(5/35): dbus-libs-1.10.24-14.el7_8.x86_64.rpm                                                                                                                                                                         | 169 kB  00:00:00
(6/35): dbus-1.10.24-14.el7_8.x86_64.rpm                                                                                                                                                                              | 245 kB  00:00:02
(7/35): grub2-2.02-0.86.el7.centos.x86_64.rpm                                                                                                                                                                         |  32 kB  00:00:00
(8/35): grub2-pc-2.02-0.86.el7.centos.x86_64.rpm                                                                                                                                                                      |  32 kB  00:00:00
(9/35): grub2-pc-modules-2.02-0.86.el7.centos.noarch.rpm                                                                                                                                                              | 850 kB  00:00:00
(10/35): grub2-tools-extra-2.02-0.86.el7.centos.x86_64.rpm                                                                                                                                                            | 1.0 MB  00:00:01
(11/35): grub2-tools-minimal-2.02-0.86.el7.centos.x86_64.rpm                                                                                                                                                          | 174 kB  00:00:00
(12/35): grub2-common-2.02-0.86.el7.centos.noarch.rpm                                                                                                                                                                 | 729 kB  00:00:02
(13/35): device-mapper-libs-1.02.164-7.el7_8.2.x86_64.rpm                                                                                                                                                             | 324 kB  00:00:03
(14/35): grub2-tools-2.02-0.86.el7.centos.x86_64.rpm                                                                                                                                                                  | 1.8 MB  00:00:04
(15/35): binutils-2.27-43.base.el7_8.1.x86_64.rpm                                                                                                                                                                     | 5.9 MB  00:00:10
(16/35): libwbclient-4.10.4-11.el7_8.x86_64.rpm                                                                                                                                                                       | 113 kB  00:00:00
(17/35): nfs-utils-1.3.0-0.66.el7_8.x86_64.rpm                                                                                                                                                                        | 412 kB  00:00:00
(18/35): libcurl-7.29.0-57.el7_8.1.x86_64.rpm                                                                                                                                                                         | 223 kB  00:00:04
(19/35): open-vm-tools-10.3.10-2.el7_8.1.x86_64.rpm                                                                                                                                                                   | 674 kB  00:00:06
(20/35): python-perf-3.10.0-1127.19.1.el7.x86_64.rpm                                                                                                                                                                  | 8.1 MB  00:00:06
(21/35): kernel-tools-3.10.0-1127.19.1.el7.x86_64.rpm                                                                                                                                                                 | 8.1 MB  00:00:14
(22/35): samba-client-libs-4.10.4-11.el7_8.x86_64.rpm                                                                                                                                                                 | 5.0 MB  00:00:02
(23/35): samba-common-libs-4.10.4-11.el7_8.x86_64.rpm                                                                                                                                                                 | 176 kB  00:00:00
(24/35): samba-libs-4.10.4-11.el7_8.x86_64.rpm                                                                                                                                                                        | 271 kB  00:00:00
(25/35): selinux-policy-3.13.1-266.el7_8.1.noarch.rpm                                                                                                                                                                 | 497 kB  00:00:00
(26/35): selinux-policy-targeted-3.13.1-266.el7_8.1.noarch.rpm                                                                                                                                                        | 7.0 MB  00:00:03
(27/35): samba-common-4.10.4-11.el7_8.noarch.rpm                                                                                                                                                                      | 212 kB  00:00:05
(28/35): systemd-219-73.el7_8.9.x86_64.rpm                                                                                                                                                                            | 5.1 MB  00:00:01
(29/35): systemd-sysv-219-73.el7_8.9.x86_64.rpm                                                                                                                                                                       |  94 kB  00:00:00
(30/35): yum-plugin-fastestmirror-1.1.31-54.el7_8.noarch.rpm                                                                                                                                                          |  34 kB  00:00:00
(31/35): yum-utils-1.1.31-54.el7_8.noarch.rpm                                                                                                                                                                         | 122 kB  00:00:00
(32/35): systemd-libs-219-73.el7_8.9.x86_64.rpm                                                                                                                                                                       | 416 kB  00:00:01
(33/35): rsyslog-8.24.0-52.el7_8.2.x86_64.rpm                                                                                                                                                                         | 621 kB  00:00:09
(34/35): kernel-tools-libs-3.10.0-1127.19.1.el7.x86_64.rpm                                                                                                                                                            | 8.0 MB  00:00:24
(35/35): kernel-3.10.0-1127.19.1.el7.x86_64.rpm                                                                                                                                                                       |  50 MB  00:00:32
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Total                                                                                                                                                                                                        2.9 MB/s | 108 MB  00:00:37
Retrieving key from file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
Importing GPG key 0xF4A80EB5:
 Userid     : "CentOS-7 Key (CentOS 7 Official Signing Key) <security@centos.org>"
 Fingerprint: 6341 ab27 53d7 8a78 a7c2 7bb1 24c6 a8a7 f4a8 0eb5
 Package    : centos-release-7-8.2003.0.el7.centos.x86_64 (@anaconda)
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
Is this ok [y/N]: y
Running transaction check
Running transaction test
Transaction test succeeded
Running transaction
  Updating   : systemd-libs-219-73.el7_8.9.x86_64                                                                                                                                                                                       1/69
  Updating   : 1:grub2-common-2.02-0.86.el7.centos.noarch                                                                                                                                                                               2/69
  Updating   : libcurl-7.29.0-57.el7_8.1.x86_64                                                                                                                                                                                         3/69
  Updating   : 1:grub2-pc-modules-2.02-0.86.el7.centos.noarch                                                                                                                                                                           4/69
  Updating   : 1:dbus-libs-1.10.24-14.el7_8.x86_64                                                                                                                                                                                      5/69
  Updating   : systemd-219-73.el7_8.9.x86_64                                                                                                                                                                                            6/69
  Updating   : 1:dbus-1.10.24-14.el7_8.x86_64                                                                                                                                                                                           7/69
  Updating   : samba-common-4.10.4-11.el7_8.noarch                                                                                                                                                                                      8/69
  Updating   : libwbclient-4.10.4-11.el7_8.x86_64                                                                                                                                                                                       9/69
  Updating   : samba-libs-4.10.4-11.el7_8.x86_64                                                                                                                                                                                       10/69
  Updating   : samba-common-libs-4.10.4-11.el7_8.x86_64                                                                                                                                                                                11/69
  Updating   : samba-client-libs-4.10.4-11.el7_8.x86_64                                                                                                                                                                                12/69
  Updating   : 7:device-mapper-libs-1.02.164-7.el7_8.2.x86_64                                                                                                                                                                          13/69
  Updating   : 7:device-mapper-1.02.164-7.el7_8.2.x86_64                                                                                                                                                                               14/69
  Updating   : 1:grub2-tools-minimal-2.02-0.86.el7.centos.x86_64                                                                                                                                                                       15/69
  Updating   : 1:grub2-tools-2.02-0.86.el7.centos.x86_64                                                                                                                                                                               16/69
  Updating   : 1:grub2-tools-extra-2.02-0.86.el7.centos.x86_64                                                                                                                                                                         17/69
  Updating   : 1:grub2-pc-2.02-0.86.el7.centos.x86_64                                                                                                                                                                                  18/69
  Updating   : selinux-policy-3.13.1-266.el7_8.1.noarch                                                                                                                                                                                19/69
  Updating   : kernel-tools-libs-3.10.0-1127.19.1.el7.x86_64                                                                                                                                                                           20/69
  Updating   : kernel-tools-3.10.0-1127.19.1.el7.x86_64                                                                                                                                                                                21/69
  Updating   : selinux-policy-targeted-3.13.1-266.el7_8.1.noarch                                                                                                                                                                       22/69
  Updating   : 1:grub2-2.02-0.86.el7.centos.x86_64                                                                                                                                                                                     23/69
  Updating   : 1:nfs-utils-1.3.0-0.66.el7_8.x86_64                                                                                                                                                                                     24/69
  Updating   : rsyslog-8.24.0-52.el7_8.2.x86_64                                                                                                                                                                                        25/69
  Updating   : systemd-sysv-219-73.el7_8.9.x86_64                                                                                                                                                                                      26/69
  Updating   : open-vm-tools-10.3.10-2.el7_8.1.x86_64                                                                                                                                                                                  27/69
  Updating   : curl-7.29.0-57.el7_8.1.x86_64                                                                                                                                                                                           28/69
  Installing : kernel-3.10.0-1127.19.1.el7.x86_64                                                                                                                                                                                      29/69
  Updating   : python-perf-3.10.0-1127.19.1.el7.x86_64                                                                                                                                                                                 30/69
  Updating   : yum-utils-1.1.31-54.el7_8.noarch                                                                                                                                                                                        31/69
  Updating   : binutils-2.27-43.base.el7_8.1.x86_64                                                                                                                                                                                    32/69
  Updating   : yum-plugin-fastestmirror-1.1.31-54.el7_8.noarch                                                                                                                                                                         33/69
  Updating   : ca-certificates-2020.2.41-70.0.el7_8.noarch                                                                                                                                                                             34/69
  Updating   : 32:bind-export-libs-9.11.4-16.P2.el7_8.6.x86_64                                                                                                                                                                         35/69
  Cleanup    : selinux-policy-targeted-3.13.1-266.el7.noarch                                                                                                                                                                           36/69
  Cleanup    : systemd-sysv-219-73.el7_8.5.x86_64                                                                                                                                                                                      37/69
  Cleanup    : 1:grub2-2.02-0.81.el7.centos.x86_64                                                                                                                                                                                     38/69
  Cleanup    : 1:grub2-pc-2.02-0.81.el7.centos.x86_64                                                                                                                                                                                  39/69
  Cleanup    : samba-common-libs-4.10.4-10.el7.x86_64                                                                                                                                                                                  40/69
  Cleanup    : samba-libs-4.10.4-10.el7.x86_64                                                                                                                                                                                         41/69
  Cleanup    : libwbclient-4.10.4-10.el7.x86_64                                                                                                                                                                                        42/69
  Cleanup    : samba-client-libs-4.10.4-10.el7.x86_64                                                                                                                                                                                  43/69
  Cleanup    : 1:grub2-tools-extra-2.02-0.81.el7.centos.x86_64                                                                                                                                                                         44/69
  Cleanup    : 1:grub2-tools-2.02-0.81.el7.centos.x86_64                                                                                                                                                                               45/69
  Cleanup    : 1:grub2-tools-minimal-2.02-0.81.el7.centos.x86_64                                                                                                                                                                       46/69
  Cleanup    : 1:nfs-utils-1.3.0-0.66.el7.x86_64                                                                                                                                                                                       47/69
  Cleanup    : 7:device-mapper-1.02.164-7.el7_8.1.x86_64                                                                                                                                                                               48/69
  Cleanup    : 7:device-mapper-libs-1.02.164-7.el7_8.1.x86_64                                                                                                                                                                          49/69
  Cleanup    : open-vm-tools-10.3.10-2.el7.x86_64                                                                                                                                                                                      50/69
  Cleanup    : rsyslog-8.24.0-52.el7.x86_64                                                                                                                                                                                            51/69
  Cleanup    : samba-common-4.10.4-10.el7.noarch                                                                                                                                                                                       52/69
  Cleanup    : 1:grub2-pc-modules-2.02-0.81.el7.centos.noarch                                                                                                                                                                          53/69
  Cleanup    : 1:dbus-1.10.24-13.el7_6.x86_64                                                                                                                                                                                          54/69
  Cleanup    : systemd-219-73.el7_8.5.x86_64                                                                                                                                                                                           55/69
  Cleanup    : 1:dbus-libs-1.10.24-13.el7_6.x86_64                                                                                                                                                                                     56/69
  Cleanup    : curl-7.29.0-57.el7.x86_64                                                                                                                                                                                               57/69
  Cleanup    : kernel-tools-3.10.0-1127.el7.x86_64                                                                                                                                                                                     58/69
  Cleanup    : 1:grub2-common-2.02-0.81.el7.centos.noarch                                                                                                                                                                              59/69
  Cleanup    : selinux-policy-3.13.1-266.el7.noarch                                                                                                                                                                                    60/69
  Cleanup    : yum-utils-1.1.31-53.el7.noarch                                                                                                                                                                                          61/69
  Cleanup    : yum-plugin-fastestmirror-1.1.31-53.el7.noarch                                                                                                                                                                           62/69
  Cleanup    : ca-certificates-2019.2.32-76.el7_7.noarch                                                                                                                                                                               63/69
  Cleanup    : kernel-tools-libs-3.10.0-1127.el7.x86_64                                                                                                                                                                                64/69
  Cleanup    : libcurl-7.29.0-57.el7.x86_64                                                                                                                                                                                            65/69
  Cleanup    : systemd-libs-219-73.el7_8.5.x86_64                                                                                                                                                                                      66/69
  Cleanup    : python-perf-3.10.0-1127.el7.x86_64                                                                                                                                                                                      67/69
  Cleanup    : binutils-2.27-43.base.el7.x86_64                                                                                                                                                                                        68/69
  Cleanup    : 32:bind-export-libs-9.11.4-16.P2.el7_8.2.x86_64                                                                                                                                                                         69/69
  Verifying  : samba-client-libs-4.10.4-11.el7_8.x86_64                                                                                                                                                                                 1/69
  Verifying  : 1:dbus-libs-1.10.24-14.el7_8.x86_64                                                                                                                                                                                      2/69
  Verifying  : kernel-tools-3.10.0-1127.19.1.el7.x86_64                                                                                                                                                                                 3/69
  Verifying  : 32:bind-export-libs-9.11.4-16.P2.el7_8.6.x86_64                                                                                                                                                                          4/69
  Verifying  : ca-certificates-2020.2.41-70.0.el7_8.noarch                                                                                                                                                                              5/69
  Verifying  : kernel-tools-libs-3.10.0-1127.19.1.el7.x86_64                                                                                                                                                                            6/69
  Verifying  : yum-plugin-fastestmirror-1.1.31-54.el7_8.noarch                                                                                                                                                                          7/69
  Verifying  : 1:grub2-2.02-0.86.el7.centos.x86_64                                                                                                                                                                                      8/69
  Verifying  : samba-common-4.10.4-11.el7_8.noarch                                                                                                                                                                                      9/69
  Verifying  : libwbclient-4.10.4-11.el7_8.x86_64                                                                                                                                                                                      10/69
  Verifying  : selinux-policy-3.13.1-266.el7_8.1.noarch                                                                                                                                                                                11/69
  Verifying  : binutils-2.27-43.base.el7_8.1.x86_64                                                                                                                                                                                    12/69
  Verifying  : rsyslog-8.24.0-52.el7_8.2.x86_64                                                                                                                                                                                        13/69
  Verifying  : 1:dbus-1.10.24-14.el7_8.x86_64                                                                                                                                                                                          14/69
  Verifying  : 1:grub2-common-2.02-0.86.el7.centos.noarch                                                                                                                                                                              15/69
  Verifying  : 1:grub2-pc-2.02-0.86.el7.centos.x86_64                                                                                                                                                                                  16/69
  Verifying  : yum-utils-1.1.31-54.el7_8.noarch                                                                                                                                                                                        17/69
  Verifying  : 7:device-mapper-1.02.164-7.el7_8.2.x86_64                                                                                                                                                                               18/69
  Verifying  : systemd-sysv-219-73.el7_8.9.x86_64                                                                                                                                                                                      19/69
  Verifying  : systemd-219-73.el7_8.9.x86_64                                                                                                                                                                                           20/69
  Verifying  : systemd-libs-219-73.el7_8.9.x86_64                                                                                                                                                                                      21/69
  Verifying  : curl-7.29.0-57.el7_8.1.x86_64                                                                                                                                                                                           22/69
  Verifying  : python-perf-3.10.0-1127.19.1.el7.x86_64                                                                                                                                                                                 23/69
  Verifying  : 7:device-mapper-libs-1.02.164-7.el7_8.2.x86_64                                                                                                                                                                          24/69
  Verifying  : 1:grub2-tools-2.02-0.86.el7.centos.x86_64                                                                                                                                                                               25/69
  Verifying  : samba-libs-4.10.4-11.el7_8.x86_64                                                                                                                                                                                       26/69
  Verifying  : 1:grub2-tools-minimal-2.02-0.86.el7.centos.x86_64                                                                                                                                                                       27/69
  Verifying  : open-vm-tools-10.3.10-2.el7_8.1.x86_64                                                                                                                                                                                  28/69
  Verifying  : 1:grub2-tools-extra-2.02-0.86.el7.centos.x86_64                                                                                                                                                                         29/69
  Verifying  : selinux-policy-targeted-3.13.1-266.el7_8.1.noarch                                                                                                                                                                       30/69
  Verifying  : libcurl-7.29.0-57.el7_8.1.x86_64                                                                                                                                                                                        31/69
  Verifying  : samba-common-libs-4.10.4-11.el7_8.x86_64                                                                                                                                                                                32/69
  Verifying  : 1:nfs-utils-1.3.0-0.66.el7_8.x86_64                                                                                                                                                                                     33/69
  Verifying  : kernel-3.10.0-1127.19.1.el7.x86_64                                                                                                                                                                                      34/69
  Verifying  : 1:grub2-pc-modules-2.02-0.86.el7.centos.noarch                                                                                                                                                                          35/69
  Verifying  : rsyslog-8.24.0-52.el7.x86_64                                                                                                                                                                                            36/69
  Verifying  : 1:grub2-2.02-0.81.el7.centos.x86_64                                                                                                                                                                                     37/69
  Verifying  : 1:grub2-tools-2.02-0.81.el7.centos.x86_64                                                                                                                                                                               38/69
  Verifying  : 1:grub2-common-2.02-0.81.el7.centos.noarch                                                                                                                                                                              39/69
  Verifying  : 1:grub2-pc-2.02-0.81.el7.centos.x86_64                                                                                                                                                                                  40/69
  Verifying  : systemd-219-73.el7_8.5.x86_64                                                                                                                                                                                           41/69
  Verifying  : 32:bind-export-libs-9.11.4-16.P2.el7_8.2.x86_64                                                                                                                                                                         42/69
  Verifying  : libcurl-7.29.0-57.el7.x86_64                                                                                                                                                                                            43/69
  Verifying  : curl-7.29.0-57.el7.x86_64                                                                                                                                                                                               44/69
  Verifying  : samba-common-4.10.4-10.el7.noarch                                                                                                                                                                                       45/69
  Verifying  : 7:device-mapper-libs-1.02.164-7.el7_8.1.x86_64                                                                                                                                                                          46/69
  Verifying  : ca-certificates-2019.2.32-76.el7_7.noarch                                                                                                                                                                               47/69
  Verifying  : samba-common-libs-4.10.4-10.el7.x86_64                                                                                                                                                                                  48/69
  Verifying  : samba-libs-4.10.4-10.el7.x86_64                                                                                                                                                                                         49/69
  Verifying  : libwbclient-4.10.4-10.el7.x86_64                                                                                                                                                                                        50/69
  Verifying  : selinux-policy-3.13.1-266.el7.noarch                                                                                                                                                                                    51/69
  Verifying  : 1:dbus-libs-1.10.24-13.el7_6.x86_64                                                                                                                                                                                     52/69
  Verifying  : binutils-2.27-43.base.el7.x86_64                                                                                                                                                                                        53/69
  Verifying  : 1:dbus-1.10.24-13.el7_6.x86_64                                                                                                                                                                                          54/69
  Verifying  : 7:device-mapper-1.02.164-7.el7_8.1.x86_64                                                                                                                                                                               55/69
  Verifying  : kernel-tools-3.10.0-1127.el7.x86_64                                                                                                                                                                                     56/69
  Verifying  : kernel-tools-libs-3.10.0-1127.el7.x86_64                                                                                                                                                                                57/69
  Verifying  : 1:nfs-utils-1.3.0-0.66.el7.x86_64                                                                                                                                                                                       58/69
  Verifying  : systemd-sysv-219-73.el7_8.5.x86_64                                                                                                                                                                                      59/69
  Verifying  : 1:grub2-pc-modules-2.02-0.81.el7.centos.noarch                                                                                                                                                                          60/69
  Verifying  : selinux-policy-targeted-3.13.1-266.el7.noarch                                                                                                                                                                           61/69
  Verifying  : samba-client-libs-4.10.4-10.el7.x86_64                                                                                                                                                                                  62/69
  Verifying  : python-perf-3.10.0-1127.el7.x86_64                                                                                                                                                                                      63/69
  Verifying  : 1:grub2-tools-minimal-2.02-0.81.el7.centos.x86_64                                                                                                                                                                       64/69
  Verifying  : 1:grub2-tools-extra-2.02-0.81.el7.centos.x86_64                                                                                                                                                                         65/69
  Verifying  : open-vm-tools-10.3.10-2.el7.x86_64                                                                                                                                                                                      66/69
  Verifying  : systemd-libs-219-73.el7_8.5.x86_64                                                                                                                                                                                      67/69
  Verifying  : yum-utils-1.1.31-53.el7.noarch                                                                                                                                                                                          68/69
  Verifying  : yum-plugin-fastestmirror-1.1.31-53.el7.noarch                                                                                                                                                                           69/69

Installed:
  kernel.x86_64 0:3.10.0-1127.19.1.el7

Updated:
  bind-export-libs.x86_64 32:9.11.4-16.P2.el7_8.6              binutils.x86_64 0:2.27-43.base.el7_8.1                   ca-certificates.noarch 0:2020.2.41-70.0.el7_8              curl.x86_64 0:7.29.0-57.el7_8.1
  dbus.x86_64 1:1.10.24-14.el7_8                               dbus-libs.x86_64 1:1.10.24-14.el7_8                      device-mapper.x86_64 7:1.02.164-7.el7_8.2                  device-mapper-libs.x86_64 7:1.02.164-7.el7_8.2
  grub2.x86_64 1:2.02-0.86.el7.centos                          grub2-common.noarch 1:2.02-0.86.el7.centos               grub2-pc.x86_64 1:2.02-0.86.el7.centos                     grub2-pc-modules.noarch 1:2.02-0.86.el7.centos
  grub2-tools.x86_64 1:2.02-0.86.el7.centos                    grub2-tools-extra.x86_64 1:2.02-0.86.el7.centos          grub2-tools-minimal.x86_64 1:2.02-0.86.el7.centos          kernel-tools.x86_64 0:3.10.0-1127.19.1.el7
  kernel-tools-libs.x86_64 0:3.10.0-1127.19.1.el7              libcurl.x86_64 0:7.29.0-57.el7_8.1                       libwbclient.x86_64 0:4.10.4-11.el7_8                       nfs-utils.x86_64 1:1.3.0-0.66.el7_8
  open-vm-tools.x86_64 0:10.3.10-2.el7_8.1                     python-perf.x86_64 0:3.10.0-1127.19.1.el7                rsyslog.x86_64 0:8.24.0-52.el7_8.2                         samba-client-libs.x86_64 0:4.10.4-11.el7_8
  samba-common.noarch 0:4.10.4-11.el7_8                        samba-common-libs.x86_64 0:4.10.4-11.el7_8               samba-libs.x86_64 0:4.10.4-11.el7_8                        selinux-policy.noarch 0:3.13.1-266.el7_8.1
  selinux-policy-targeted.noarch 0:3.13.1-266.el7_8.1          systemd.x86_64 0:219-73.el7_8.9                          systemd-libs.x86_64 0:219-73.el7_8.9                       systemd-sysv.x86_64 0:219-73.el7_8.9
  yum-plugin-fastestmirror.noarch 0:1.1.31-54.el7_8            yum-utils.noarch 0:1.1.31-54.el7_8

Complete!
```

* Installation de paquets additionels

*vim* 

```
[vagrant@myhost ~]$ sudo yum install vim
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: centos.mirror.ate.info
 * extras: centos.mirror.ate.info
 * updates: centos.mirror.ate.info
Resolving Dependencies
--> Running transaction check
---> Package vim-enhanced.x86_64 2:7.4.629-6.el7 will be installed
--> Processing Dependency: vim-common = 2:7.4.629-6.el7 for package: 2:vim-enhanced-7.4.629-6.el7.x86_64
--> Processing Dependency: perl(:MODULE_COMPAT_5.16.3) for package: 2:vim-enhanced-7.4.629-6.el7.x86_64
--> Processing Dependency: libperl.so()(64bit) for package: 2:vim-enhanced-7.4.629-6.el7.x86_64
--> Processing Dependency: libgpm.so.2()(64bit) for package: 2:vim-enhanced-7.4.629-6.el7.x86_64
--> Running transaction check
---> Package gpm-libs.x86_64 0:1.20.7-6.el7 will be installed
---> Package perl.x86_64 4:5.16.3-295.el7 will be installed
--> Processing Dependency: perl(Socket) >= 1.3 for package: 4:perl-5.16.3-295.el7.x86_64
--> Processing Dependency: perl(Scalar::Util) >= 1.10 for package: 4:perl-5.16.3-295.el7.x86_64
--> Processing Dependency: perl-macros for package: 4:perl-5.16.3-295.el7.x86_64
--> Processing Dependency: perl(threads::shared) for package: 4:perl-5.16.3-295.el7.x86_64
--> Processing Dependency: perl(threads) for package: 4:perl-5.16.3-295.el7.x86_64
--> Processing Dependency: perl(constant) for package: 4:perl-5.16.3-295.el7.x86_64
--> Processing Dependency: perl(Time::Local) for package: 4:perl-5.16.3-295.el7.x86_64
--> Processing Dependency: perl(Time::HiRes) for package: 4:perl-5.16.3-295.el7.x86_64
--> Processing Dependency: perl(Storable) for package: 4:perl-5.16.3-295.el7.x86_64
--> Processing Dependency: perl(Socket) for package: 4:perl-5.16.3-295.el7.x86_64
--> Processing Dependency: perl(Scalar::Util) for package: 4:perl-5.16.3-295.el7.x86_64
--> Processing Dependency: perl(Pod::Simple::XHTML) for package: 4:perl-5.16.3-295.el7.x86_64
--> Processing Dependency: perl(Pod::Simple::Search) for package: 4:perl-5.16.3-295.el7.x86_64
--> Processing Dependency: perl(Getopt::Long) for package: 4:perl-5.16.3-295.el7.x86_64
--> Processing Dependency: perl(Filter::Util::Call) for package: 4:perl-5.16.3-295.el7.x86_64
--> Processing Dependency: perl(File::Temp) for package: 4:perl-5.16.3-295.el7.x86_64
--> Processing Dependency: perl(File::Spec::Unix) for package: 4:perl-5.16.3-295.el7.x86_64
--> Processing Dependency: perl(File::Spec::Functions) for package: 4:perl-5.16.3-295.el7.x86_64
--> Processing Dependency: perl(File::Spec) for package: 4:perl-5.16.3-295.el7.x86_64
--> Processing Dependency: perl(File::Path) for package: 4:perl-5.16.3-295.el7.x86_64
--> Processing Dependency: perl(Exporter) for package: 4:perl-5.16.3-295.el7.x86_64
--> Processing Dependency: perl(Cwd) for package: 4:perl-5.16.3-295.el7.x86_64
--> Processing Dependency: perl(Carp) for package: 4:perl-5.16.3-295.el7.x86_64
---> Package perl-libs.x86_64 4:5.16.3-295.el7 will be installed
---> Package vim-common.x86_64 2:7.4.629-6.el7 will be installed
--> Processing Dependency: vim-filesystem for package: 2:vim-common-7.4.629-6.el7.x86_64
--> Running transaction check
---> Package perl-Carp.noarch 0:1.26-244.el7 will be installed
---> Package perl-Exporter.noarch 0:5.68-3.el7 will be installed
---> Package perl-File-Path.noarch 0:2.09-2.el7 will be installed
---> Package perl-File-Temp.noarch 0:0.23.01-3.el7 will be installed
---> Package perl-Filter.x86_64 0:1.49-3.el7 will be installed
---> Package perl-Getopt-Long.noarch 0:2.40-3.el7 will be installed
--> Processing Dependency: perl(Pod::Usage) >= 1.14 for package: perl-Getopt-Long-2.40-3.el7.noarch
--> Processing Dependency: perl(Text::ParseWords) for package: perl-Getopt-Long-2.40-3.el7.noarch
---> Package perl-PathTools.x86_64 0:3.40-5.el7 will be installed
---> Package perl-Pod-Simple.noarch 1:3.28-4.el7 will be installed
--> Processing Dependency: perl(Pod::Escapes) >= 1.04 for package: 1:perl-Pod-Simple-3.28-4.el7.noarch
--> Processing Dependency: perl(Encode) for package: 1:perl-Pod-Simple-3.28-4.el7.noarch
---> Package perl-Scalar-List-Utils.x86_64 0:1.27-248.el7 will be installed
---> Package perl-Socket.x86_64 0:2.010-5.el7 will be installed
---> Package perl-Storable.x86_64 0:2.45-3.el7 will be installed
---> Package perl-Time-HiRes.x86_64 4:1.9725-3.el7 will be installed
---> Package perl-Time-Local.noarch 0:1.2300-2.el7 will be installed
---> Package perl-constant.noarch 0:1.27-2.el7 will be installed
---> Package perl-macros.x86_64 4:5.16.3-295.el7 will be installed
---> Package perl-threads.x86_64 0:1.87-4.el7 will be installed
---> Package perl-threads-shared.x86_64 0:1.43-6.el7 will be installed
---> Package vim-filesystem.x86_64 2:7.4.629-6.el7 will be installed
--> Running transaction check
---> Package perl-Encode.x86_64 0:2.51-7.el7 will be installed
---> Package perl-Pod-Escapes.noarch 1:1.04-295.el7 will be installed
---> Package perl-Pod-Usage.noarch 0:1.63-3.el7 will be installed
--> Processing Dependency: perl(Pod::Text) >= 3.15 for package: perl-Pod-Usage-1.63-3.el7.noarch
--> Processing Dependency: perl-Pod-Perldoc for package: perl-Pod-Usage-1.63-3.el7.noarch
---> Package perl-Text-ParseWords.noarch 0:3.29-4.el7 will be installed
--> Running transaction check
---> Package perl-Pod-Perldoc.noarch 0:3.20-4.el7 will be installed
--> Processing Dependency: perl(parent) for package: perl-Pod-Perldoc-3.20-4.el7.noarch
--> Processing Dependency: perl(HTTP::Tiny) for package: perl-Pod-Perldoc-3.20-4.el7.noarch
---> Package perl-podlators.noarch 0:2.5.1-3.el7 will be installed
--> Running transaction check
---> Package perl-HTTP-Tiny.noarch 0:0.033-3.el7 will be installed
---> Package perl-parent.noarch 1:0.225-244.el7 will be installed
--> Finished Dependency Resolution

Dependencies Resolved

=============================================================================================================================================================================================================================================
 Package                                                            Arch                                               Version                                                        Repository                                        Size
=============================================================================================================================================================================================================================================
Installing:
 vim-enhanced                                                       x86_64                                             2:7.4.629-6.el7                                                base                                             1.1 M
Installing for dependencies:
 gpm-libs                                                           x86_64                                             1.20.7-6.el7                                                   base                                              32 k
 perl                                                               x86_64                                             4:5.16.3-295.el7                                               base                                             8.0 M
 perl-Carp                                                          noarch                                             1.26-244.el7                                                   base                                              19 k
 perl-Encode                                                        x86_64                                             2.51-7.el7                                                     base                                             1.5 M
 perl-Exporter                                                      noarch                                             5.68-3.el7                                                     base                                              28 k
 perl-File-Path                                                     noarch                                             2.09-2.el7                                                     base                                              26 k
 perl-File-Temp                                                     noarch                                             0.23.01-3.el7                                                  base                                              56 k
 perl-Filter                                                        x86_64                                             1.49-3.el7                                                     base                                              76 k
 perl-Getopt-Long                                                   noarch                                             2.40-3.el7                                                     base                                              56 k
 perl-HTTP-Tiny                                                     noarch                                             0.033-3.el7                                                    base                                              38 k
 perl-PathTools                                                     x86_64                                             3.40-5.el7                                                     base                                              82 k
 perl-Pod-Escapes                                                   noarch                                             1:1.04-295.el7                                                 base                                              51 k
 perl-Pod-Perldoc                                                   noarch                                             3.20-4.el7                                                     base                                              87 k
 perl-Pod-Simple                                                    noarch                                             1:3.28-4.el7                                                   base                                             216 k
 perl-Pod-Usage                                                     noarch                                             1.63-3.el7                                                     base                                              27 k
 perl-Scalar-List-Utils                                             x86_64                                             1.27-248.el7                                                   base                                              36 k
 perl-Socket                                                        x86_64                                             2.010-5.el7                                                    base                                              49 k
 perl-Storable                                                      x86_64                                             2.45-3.el7                                                     base                                              77 k
 perl-Text-ParseWords                                               noarch                                             3.29-4.el7                                                     base                                              14 k
 perl-Time-HiRes                                                    x86_64                                             4:1.9725-3.el7                                                 base                                              45 k
 perl-Time-Local                                                    noarch                                             1.2300-2.el7                                                   base                                              24 k
 perl-constant                                                      noarch                                             1.27-2.el7                                                     base                                              19 k
 perl-libs                                                          x86_64                                             4:5.16.3-295.el7                                               base                                             689 k
 perl-macros                                                        x86_64                                             4:5.16.3-295.el7                                               base                                              44 k
 perl-parent                                                        noarch                                             1:0.225-244.el7                                                base                                              12 k
 perl-podlators                                                     noarch                                             2.5.1-3.el7                                                    base                                             112 k
 perl-threads                                                       x86_64                                             1.87-4.el7                                                     base                                              49 k
 perl-threads-shared                                                x86_64                                             1.43-6.el7                                                     base                                              39 k
 vim-common                                                         x86_64                                             2:7.4.629-6.el7                                                base                                             5.9 M
 vim-filesystem                                                     x86_64                                             2:7.4.629-6.el7                                                base                                              11 k

Transaction Summary
=============================================================================================================================================================================================================================================
Install  1 Package (+30 Dependent packages)

Total download size: 18 M
Installed size: 60 M
Is this ok [y/d/N]: y
Downloading packages:
(1/31): perl-Carp-1.26-244.el7.noarch.rpm                                                                                                                                                                             |  19 kB  00:00:00
(2/31): perl-Exporter-5.68-3.el7.noarch.rpm                                                                                                                                                                           |  28 kB  00:00:00
(3/31): gpm-libs-1.20.7-6.el7.x86_64.rpm                                                                                                                                                                              |  32 kB  00:00:00
(4/31): perl-File-Path-2.09-2.el7.noarch.rpm                                                                                                                                                                          |  26 kB  00:00:00
(5/31): perl-File-Temp-0.23.01-3.el7.noarch.rpm                                                                                                                                                                       |  56 kB  00:00:00
(6/31): perl-Filter-1.49-3.el7.x86_64.rpm                                                                                                                                                                             |  76 kB  00:00:00
(7/31): perl-Getopt-Long-2.40-3.el7.noarch.rpm                                                                                                                                                                        |  56 kB  00:00:00
(8/31): perl-HTTP-Tiny-0.033-3.el7.noarch.rpm                                                                                                                                                                         |  38 kB  00:00:00
(9/31): perl-Pod-Escapes-1.04-295.el7.noarch.rpm                                                                                                                                                                      |  51 kB  00:00:00
(10/31): perl-PathTools-3.40-5.el7.x86_64.rpm                                                                                                                                                                         |  82 kB  00:00:00
(11/31): perl-Pod-Usage-1.63-3.el7.noarch.rpm                                                                                                                                                                         |  27 kB  00:00:00
(12/31): perl-Pod-Perldoc-3.20-4.el7.noarch.rpm                                                                                                                                                                       |  87 kB  00:00:00
(13/31): perl-Socket-2.010-5.el7.x86_64.rpm                                                                                                                                                                           |  49 kB  00:00:00
(14/31): perl-Scalar-List-Utils-1.27-248.el7.x86_64.rpm                                                                                                                                                               |  36 kB  00:00:00
(15/31): perl-Encode-2.51-7.el7.x86_64.rpm                                                                                                                                                                            | 1.5 MB  00:00:01
(16/31): perl-Pod-Simple-3.28-4.el7.noarch.rpm                                                                                                                                                                        | 216 kB  00:00:00
(17/31): perl-Text-ParseWords-3.29-4.el7.noarch.rpm                                                                                                                                                                   |  14 kB  00:00:00
(18/31): perl-Time-Local-1.2300-2.el7.noarch.rpm                                                                                                                                                                      |  24 kB  00:00:00
(19/31): perl-Storable-2.45-3.el7.x86_64.rpm                                                                                                                                                                          |  77 kB  00:00:00
(20/31): perl-constant-1.27-2.el7.noarch.rpm                                                                                                                                                                          |  19 kB  00:00:00
(21/31): perl-macros-5.16.3-295.el7.x86_64.rpm                                                                                                                                                                        |  44 kB  00:00:00
(22/31): perl-parent-0.225-244.el7.noarch.rpm                                                                                                                                                                         |  12 kB  00:00:00
(23/31): perl-threads-1.87-4.el7.x86_64.rpm                                                                                                                                                                           |  49 kB  00:00:00
(24/31): perl-Time-HiRes-1.9725-3.el7.x86_64.rpm                                                                                                                                                                      |  45 kB  00:00:02
(25/31): perl-podlators-2.5.1-3.el7.noarch.rpm                                                                                                                                                                        | 112 kB  00:00:01
(26/31): perl-threads-shared-1.43-6.el7.x86_64.rpm                                                                                                                                                                    |  39 kB  00:00:00
(27/31): perl-libs-5.16.3-295.el7.x86_64.rpm                                                                                                                                                                          | 689 kB  00:00:02
(28/31): perl-5.16.3-295.el7.x86_64.rpm                                                                                                                                                                               | 8.0 MB  00:00:04
(29/31): vim-filesystem-7.4.629-6.el7.x86_64.rpm                                                                                                                                                                      |  11 kB  00:00:00
(30/31): vim-enhanced-7.4.629-6.el7.x86_64.rpm                                                                                                                                                                        | 1.1 MB  00:00:01
(31/31): vim-common-7.4.629-6.el7.x86_64.rpm                                                                                                                                                                          | 5.9 MB  00:00:03
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Total                                                                                                                                                                                                        2.3 MB/s |  18 MB  00:00:07
Running transaction check
Running transaction test
Transaction test succeeded
Running transaction
  Installing : 1:perl-parent-0.225-244.el7.noarch                                                                                                                                                                                       1/31
  Installing : perl-HTTP-Tiny-0.033-3.el7.noarch                                                                                                                                                                                        2/31
  Installing : perl-podlators-2.5.1-3.el7.noarch                                                                                                                                                                                        3/31
  Installing : perl-Pod-Perldoc-3.20-4.el7.noarch                                                                                                                                                                                       4/31
  Installing : 1:perl-Pod-Escapes-1.04-295.el7.noarch                                                                                                                                                                                   5/31
  Installing : perl-Text-ParseWords-3.29-4.el7.noarch                                                                                                                                                                                   6/31
  Installing : perl-Encode-2.51-7.el7.x86_64                                                                                                                                                                                            7/31
  Installing : perl-Pod-Usage-1.63-3.el7.noarch                                                                                                                                                                                         8/31
  Installing : 4:perl-libs-5.16.3-295.el7.x86_64                                                                                                                                                                                        9/31
  Installing : 4:perl-macros-5.16.3-295.el7.x86_64                                                                                                                                                                                     10/31
  Installing : perl-Storable-2.45-3.el7.x86_64                                                                                                                                                                                         11/31
  Installing : perl-Exporter-5.68-3.el7.noarch                                                                                                                                                                                         12/31
  Installing : perl-constant-1.27-2.el7.noarch                                                                                                                                                                                         13/31
  Installing : perl-Socket-2.010-5.el7.x86_64                                                                                                                                                                                          14/31
  Installing : perl-Time-Local-1.2300-2.el7.noarch                                                                                                                                                                                     15/31
  Installing : perl-Carp-1.26-244.el7.noarch                                                                                                                                                                                           16/31
  Installing : 4:perl-Time-HiRes-1.9725-3.el7.x86_64                                                                                                                                                                                   17/31
  Installing : perl-PathTools-3.40-5.el7.x86_64                                                                                                                                                                                        18/31
  Installing : perl-Scalar-List-Utils-1.27-248.el7.x86_64                                                                                                                                                                              19/31
  Installing : perl-File-Temp-0.23.01-3.el7.noarch                                                                                                                                                                                     20/31
  Installing : perl-File-Path-2.09-2.el7.noarch                                                                                                                                                                                        21/31
  Installing : perl-threads-shared-1.43-6.el7.x86_64                                                                                                                                                                                   22/31
  Installing : perl-threads-1.87-4.el7.x86_64                                                                                                                                                                                          23/31
  Installing : perl-Filter-1.49-3.el7.x86_64                                                                                                                                                                                           24/31
  Installing : 1:perl-Pod-Simple-3.28-4.el7.noarch                                                                                                                                                                                     25/31
  Installing : perl-Getopt-Long-2.40-3.el7.noarch                                                                                                                                                                                      26/31
  Installing : 4:perl-5.16.3-295.el7.x86_64                                                                                                                                                                                            27/31
  Installing : gpm-libs-1.20.7-6.el7.x86_64                                                                                                                                                                                            28/31
  Installing : 2:vim-filesystem-7.4.629-6.el7.x86_64                                                                                                                                                                                   29/31
  Installing : 2:vim-common-7.4.629-6.el7.x86_64                                                                                                                                                                                       30/31
  Installing : 2:vim-enhanced-7.4.629-6.el7.x86_64                                                                                                                                                                                     31/31
  Verifying  : perl-HTTP-Tiny-0.033-3.el7.noarch                                                                                                                                                                                        1/31
  Verifying  : perl-threads-shared-1.43-6.el7.x86_64                                                                                                                                                                                    2/31
  Verifying  : perl-Storable-2.45-3.el7.x86_64                                                                                                                                                                                          3/31
  Verifying  : 1:perl-Pod-Escapes-1.04-295.el7.noarch                                                                                                                                                                                   4/31
  Verifying  : perl-Exporter-5.68-3.el7.noarch                                                                                                                                                                                          5/31
  Verifying  : perl-constant-1.27-2.el7.noarch                                                                                                                                                                                          6/31
  Verifying  : perl-PathTools-3.40-5.el7.x86_64                                                                                                                                                                                         7/31
  Verifying  : perl-Socket-2.010-5.el7.x86_64                                                                                                                                                                                           8/31
  Verifying  : 1:perl-parent-0.225-244.el7.noarch                                                                                                                                                                                       9/31
  Verifying  : 4:perl-libs-5.16.3-295.el7.x86_64                                                                                                                                                                                       10/31
  Verifying  : perl-File-Temp-0.23.01-3.el7.noarch                                                                                                                                                                                     11/31
  Verifying  : 1:perl-Pod-Simple-3.28-4.el7.noarch                                                                                                                                                                                     12/31
  Verifying  : perl-Time-Local-1.2300-2.el7.noarch                                                                                                                                                                                     13/31
  Verifying  : 4:perl-macros-5.16.3-295.el7.x86_64                                                                                                                                                                                     14/31
  Verifying  : 4:perl-5.16.3-295.el7.x86_64                                                                                                                                                                                            15/31
  Verifying  : 2:vim-enhanced-7.4.629-6.el7.x86_64                                                                                                                                                                                     16/31
  Verifying  : perl-Carp-1.26-244.el7.noarch                                                                                                                                                                                           17/31
  Verifying  : 4:perl-Time-HiRes-1.9725-3.el7.x86_64                                                                                                                                                                                   18/31
  Verifying  : perl-Scalar-List-Utils-1.27-248.el7.x86_64                                                                                                                                                                              19/31
  Verifying  : perl-Pod-Usage-1.63-3.el7.noarch                                                                                                                                                                                        20/31
  Verifying  : 2:vim-common-7.4.629-6.el7.x86_64                                                                                                                                                                                       21/31
  Verifying  : 2:vim-filesystem-7.4.629-6.el7.x86_64                                                                                                                                                                                   22/31
  Verifying  : perl-Encode-2.51-7.el7.x86_64                                                                                                                                                                                           23/31
  Verifying  : perl-Pod-Perldoc-3.20-4.el7.noarch                                                                                                                                                                                      24/31
  Verifying  : perl-podlators-2.5.1-3.el7.noarch                                                                                                                                                                                       25/31
  Verifying  : perl-File-Path-2.09-2.el7.noarch                                                                                                                                                                                        26/31
  Verifying  : perl-threads-1.87-4.el7.x86_64                                                                                                                                                                                          27/31
  Verifying  : gpm-libs-1.20.7-6.el7.x86_64                                                                                                                                                                                            28/31
  Verifying  : perl-Filter-1.49-3.el7.x86_64                                                                                                                                                                                           29/31
  Verifying  : perl-Getopt-Long-2.40-3.el7.noarch                                                                                                                                                                                      30/31
  Verifying  : perl-Text-ParseWords-3.29-4.el7.noarch                                                                                                                                                                                  31/31

Installed:
  vim-enhanced.x86_64 2:7.4.629-6.el7

Dependency Installed:
  gpm-libs.x86_64 0:1.20.7-6.el7                     perl.x86_64 4:5.16.3-295.el7                 perl-Carp.noarch 0:1.26-244.el7               perl-Encode.x86_64 0:2.51-7.el7                perl-Exporter.noarch 0:5.68-3.el7
  perl-File-Path.noarch 0:2.09-2.el7                 perl-File-Temp.noarch 0:0.23.01-3.el7        perl-Filter.x86_64 0:1.49-3.el7               perl-Getopt-Long.noarch 0:2.40-3.el7           perl-HTTP-Tiny.noarch 0:0.033-3.el7
  perl-PathTools.x86_64 0:3.40-5.el7                 perl-Pod-Escapes.noarch 1:1.04-295.el7       perl-Pod-Perldoc.noarch 0:3.20-4.el7          perl-Pod-Simple.noarch 1:3.28-4.el7            perl-Pod-Usage.noarch 0:1.63-3.el7
  perl-Scalar-List-Utils.x86_64 0:1.27-248.el7       perl-Socket.x86_64 0:2.010-5.el7             perl-Storable.x86_64 0:2.45-3.el7             perl-Text-ParseWords.noarch 0:3.29-4.el7       perl-Time-HiRes.x86_64 4:1.9725-3.el7
  perl-Time-Local.noarch 0:1.2300-2.el7              perl-constant.noarch 0:1.27-2.el7            perl-libs.x86_64 4:5.16.3-295.el7             perl-macros.x86_64 4:5.16.3-295.el7            perl-parent.noarch 1:0.225-244.el7
  perl-podlators.noarch 0:2.5.1-3.el7                perl-threads.x86_64 0:1.87-4.el7             perl-threads-shared.x86_64 0:1.43-6.el7       vim-common.x86_64 2:7.4.629-6.el7              vim-filesystem.x86_64 2:7.4.629-6.el7

Complete!
```


*epel-release*

```
[vagrant@myhost ~]$ sudo yum install epel-release
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: centos.mirror.ate.info
 * extras: centos.mirror.ate.info
 * updates: centos.mirror.ate.info
Resolving Dependencies
--> Running transaction check
---> Package epel-release.noarch 0:7-11 will be installed
--> Finished Dependency Resolution

Dependencies Resolved

=============================================================================================================================================================================================================================================
 Package                                                       Arch                                                    Version                                                 Repository                                               Size
=============================================================================================================================================================================================================================================
Installing:
 epel-release                                                  noarch                                                  7-11                                                    extras                                                   15 k

Transaction Summary
=============================================================================================================================================================================================================================================
Install  1 Package

Total download size: 15 k
Installed size: 24 k
Is this ok [y/d/N]: y
Downloading packages:
epel-release-7-11.noarch.rpm                                                                                                                                                                                          |  15 kB  00:00:00
Running transaction check
Running transaction test
Transaction test succeeded
Running transaction
  Installing : epel-release-7-11.noarch                                                                                                                                                                                                  1/1
  Verifying  : epel-release-7-11.noarch                                                                                                                                                                                                  1/1

Installed:
  epel-release.noarch 0:7-11

Complete!
```

*nginx*

```
[vagrant@myhost ~]$ sudo yum install nginx
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
epel/x86_64/metalink                                                                                                                                                                                                  |  17 kB  00:00:00
 * base: centos.mirror.ate.info
 * epel: ftp-stud.hs-esslingen.de
 * extras: centos.mirror.ate.info
 * updates: centos.mirror.ate.info
epel                                                                                                                                                                                                                  | 4.7 kB  00:00:00
(1/3): epel/x86_64/group_gz                                                                                                                                                                                           |  95 kB  00:00:00
(2/3): epel/x86_64/updateinfo                                                                                                                                                                                         | 1.0 MB  00:00:01
(3/3): epel/x86_64/primary_db                                                                                                                                                                                         | 6.9 MB  00:00:04
Resolving Dependencies
--> Running transaction check
---> Package nginx.x86_64 1:1.16.1-1.el7 will be installed
--> Processing Dependency: nginx-all-modules = 1:1.16.1-1.el7 for package: 1:nginx-1.16.1-1.el7.x86_64
--> Processing Dependency: nginx-filesystem = 1:1.16.1-1.el7 for package: 1:nginx-1.16.1-1.el7.x86_64
--> Processing Dependency: nginx-filesystem for package: 1:nginx-1.16.1-1.el7.x86_64
--> Processing Dependency: redhat-indexhtml for package: 1:nginx-1.16.1-1.el7.x86_64
--> Processing Dependency: system-logos for package: 1:nginx-1.16.1-1.el7.x86_64
--> Processing Dependency: libprofiler.so.0()(64bit) for package: 1:nginx-1.16.1-1.el7.x86_64
--> Running transaction check
---> Package centos-indexhtml.noarch 0:7-9.el7.centos will be installed
---> Package centos-logos.noarch 0:70.0.6-3.el7.centos will be installed
---> Package gperftools-libs.x86_64 0:2.6.1-1.el7 will be installed
---> Package nginx-all-modules.noarch 1:1.16.1-1.el7 will be installed
--> Processing Dependency: nginx-mod-http-image-filter = 1:1.16.1-1.el7 for package: 1:nginx-all-modules-1.16.1-1.el7.noarch
--> Processing Dependency: nginx-mod-http-perl = 1:1.16.1-1.el7 for package: 1:nginx-all-modules-1.16.1-1.el7.noarch
--> Processing Dependency: nginx-mod-http-xslt-filter = 1:1.16.1-1.el7 for package: 1:nginx-all-modules-1.16.1-1.el7.noarch
--> Processing Dependency: nginx-mod-mail = 1:1.16.1-1.el7 for package: 1:nginx-all-modules-1.16.1-1.el7.noarch
--> Processing Dependency: nginx-mod-stream = 1:1.16.1-1.el7 for package: 1:nginx-all-modules-1.16.1-1.el7.noarch
---> Package nginx-filesystem.noarch 1:1.16.1-1.el7 will be installed
--> Running transaction check
---> Package nginx-mod-http-image-filter.x86_64 1:1.16.1-1.el7 will be installed
--> Processing Dependency: gd for package: 1:nginx-mod-http-image-filter-1.16.1-1.el7.x86_64
--> Processing Dependency: libgd.so.2()(64bit) for package: 1:nginx-mod-http-image-filter-1.16.1-1.el7.x86_64
---> Package nginx-mod-http-perl.x86_64 1:1.16.1-1.el7 will be installed
---> Package nginx-mod-http-xslt-filter.x86_64 1:1.16.1-1.el7 will be installed
---> Package nginx-mod-mail.x86_64 1:1.16.1-1.el7 will be installed
---> Package nginx-mod-stream.x86_64 1:1.16.1-1.el7 will be installed
--> Running transaction check
---> Package gd.x86_64 0:2.0.35-26.el7 will be installed
--> Processing Dependency: libjpeg.so.62(LIBJPEG_6.2)(64bit) for package: gd-2.0.35-26.el7.x86_64
--> Processing Dependency: libjpeg.so.62()(64bit) for package: gd-2.0.35-26.el7.x86_64
--> Processing Dependency: libfontconfig.so.1()(64bit) for package: gd-2.0.35-26.el7.x86_64
--> Processing Dependency: libXpm.so.4()(64bit) for package: gd-2.0.35-26.el7.x86_64
--> Processing Dependency: libX11.so.6()(64bit) for package: gd-2.0.35-26.el7.x86_64
--> Running transaction check
---> Package fontconfig.x86_64 0:2.13.0-4.3.el7 will be installed
--> Processing Dependency: fontpackages-filesystem for package: fontconfig-2.13.0-4.3.el7.x86_64
--> Processing Dependency: dejavu-sans-fonts for package: fontconfig-2.13.0-4.3.el7.x86_64
---> Package libX11.x86_64 0:1.6.7-2.el7 will be installed
--> Processing Dependency: libX11-common >= 1.6.7-2.el7 for package: libX11-1.6.7-2.el7.x86_64
--> Processing Dependency: libxcb.so.1()(64bit) for package: libX11-1.6.7-2.el7.x86_64
---> Package libXpm.x86_64 0:3.5.12-1.el7 will be installed
---> Package libjpeg-turbo.x86_64 0:1.2.90-8.el7 will be installed
--> Running transaction check
---> Package dejavu-sans-fonts.noarch 0:2.33-6.el7 will be installed
--> Processing Dependency: dejavu-fonts-common = 2.33-6.el7 for package: dejavu-sans-fonts-2.33-6.el7.noarch
---> Package fontpackages-filesystem.noarch 0:1.44-8.el7 will be installed
---> Package libX11-common.noarch 0:1.6.7-2.el7 will be installed
---> Package libxcb.x86_64 0:1.13-1.el7 will be installed
--> Processing Dependency: libXau.so.6()(64bit) for package: libxcb-1.13-1.el7.x86_64
--> Running transaction check
---> Package dejavu-fonts-common.noarch 0:2.33-6.el7 will be installed
---> Package libXau.x86_64 0:1.0.8-2.1.el7 will be installed
--> Finished Dependency Resolution

Dependencies Resolved

=============================================================================================================================================================================================================================================
 Package                                                               Arch                                             Version                                                         Repository                                      Size
=============================================================================================================================================================================================================================================
Installing:
 nginx                                                                 x86_64                                           1:1.16.1-1.el7                                                  epel                                           562 k
Installing for dependencies:
 centos-indexhtml                                                      noarch                                           7-9.el7.centos                                                  base                                            92 k
 centos-logos                                                          noarch                                           70.0.6-3.el7.centos                                             base                                            21 M
 dejavu-fonts-common                                                   noarch                                           2.33-6.el7                                                      base                                            64 k
 dejavu-sans-fonts                                                     noarch                                           2.33-6.el7                                                      base                                           1.4 M
 fontconfig                                                            x86_64                                           2.13.0-4.3.el7                                                  base                                           254 k
 fontpackages-filesystem                                               noarch                                           1.44-8.el7                                                      base                                           9.9 k
 gd                                                                    x86_64                                           2.0.35-26.el7                                                   base                                           146 k
 gperftools-libs                                                       x86_64                                           2.6.1-1.el7                                                     base                                           272 k
 libX11                                                                x86_64                                           1.6.7-2.el7                                                     base                                           607 k
 libX11-common                                                         noarch                                           1.6.7-2.el7                                                     base                                           164 k
 libXau                                                                x86_64                                           1.0.8-2.1.el7                                                   base                                            29 k
 libXpm                                                                x86_64                                           3.5.12-1.el7                                                    base                                            55 k
 libjpeg-turbo                                                         x86_64                                           1.2.90-8.el7                                                    base                                           135 k
 libxcb                                                                x86_64                                           1.13-1.el7                                                      base                                           214 k
 nginx-all-modules                                                     noarch                                           1:1.16.1-1.el7                                                  epel                                            19 k
 nginx-filesystem                                                      noarch                                           1:1.16.1-1.el7                                                  epel                                            21 k
 nginx-mod-http-image-filter                                           x86_64                                           1:1.16.1-1.el7                                                  epel                                            30 k
 nginx-mod-http-perl                                                   x86_64                                           1:1.16.1-1.el7                                                  epel                                            39 k
 nginx-mod-http-xslt-filter                                            x86_64                                           1:1.16.1-1.el7                                                  epel                                            29 k
 nginx-mod-mail                                                        x86_64                                           1:1.16.1-1.el7                                                  epel                                            57 k
 nginx-mod-stream                                                      x86_64                                           1:1.16.1-1.el7                                                  epel                                            84 k

Transaction Summary
=============================================================================================================================================================================================================================================
Install  1 Package (+21 Dependent packages)

Total download size: 26 M
Installed size: 35 M
Is this ok [y/d/N]: y
Downloading packages:
(1/22): centos-indexhtml-7-9.el7.centos.noarch.rpm                                                                                                                                                                    |  92 kB  00:00:00
(2/22): dejavu-fonts-common-2.33-6.el7.noarch.rpm                                                                                                                                                                     |  64 kB  00:00:00
(3/22): fontpackages-filesystem-1.44-8.el7.noarch.rpm                                                                                                                                                                 | 9.9 kB  00:00:00
(4/22): gd-2.0.35-26.el7.x86_64.rpm                                                                                                                                                                                   | 146 kB  00:00:00
(5/22): fontconfig-2.13.0-4.3.el7.x86_64.rpm                                                                                                                                                                          | 254 kB  00:00:00
(6/22): gperftools-libs-2.6.1-1.el7.x86_64.rpm                                                                                                                                                                        | 272 kB  00:00:00
(7/22): libX11-common-1.6.7-2.el7.noarch.rpm                                                                                                                                                                          | 164 kB  00:00:00
(8/22): libXau-1.0.8-2.1.el7.x86_64.rpm                                                                                                                                                                               |  29 kB  00:00:00
(9/22): libXpm-3.5.12-1.el7.x86_64.rpm                                                                                                                                                                                |  55 kB  00:00:00
(10/22): libjpeg-turbo-1.2.90-8.el7.x86_64.rpm                                                                                                                                                                        | 135 kB  00:00:00
(11/22): libxcb-1.13-1.el7.x86_64.rpm                                                                                                                                                                                 | 214 kB  00:00:00
(12/22): dejavu-sans-fonts-2.33-6.el7.noarch.rpm                                                                                                                                                                      | 1.4 MB  00:00:02
(13/22): libX11-1.6.7-2.el7.x86_64.rpm                                                                                                                                                                                | 607 kB  00:00:01
warning: /var/cache/yum/x86_64/7/epel/packages/nginx-all-modules-1.16.1-1.el7.noarch.rpm: Header V3 RSA/SHA256 Signature, key ID 352c64e5: NOKEY                                                           ] 1.6 MB/s | 8.2 MB  00:00:11 ETA
Public key for nginx-all-modules-1.16.1-1.el7.noarch.rpm is not installed
(14/22): nginx-all-modules-1.16.1-1.el7.noarch.rpm                                                                                                                                                                    |  19 kB  00:00:01
(15/22): nginx-filesystem-1.16.1-1.el7.noarch.rpm                                                                                                                                                                     |  21 kB  00:00:01
(16/22): nginx-mod-http-perl-1.16.1-1.el7.x86_64.rpm                                                                                                                                                                  |  39 kB  00:00:00
(17/22): nginx-mod-http-xslt-filter-1.16.1-1.el7.x86_64.rpm                                                                                                                                                           |  29 kB  00:00:00
(18/22): nginx-1.16.1-1.el7.x86_64.rpm                                                                                                                                                                                | 562 kB  00:00:03
(19/22): nginx-mod-mail-1.16.1-1.el7.x86_64.rpm                                                                                                                                                                       |  57 kB  00:00:00
(20/22): nginx-mod-stream-1.16.1-1.el7.x86_64.rpm                                                                                                                                                                     |  84 kB  00:00:00
(21/22): nginx-mod-http-image-filter-1.16.1-1.el7.x86_64.rpm                                                                                                                                                          |  30 kB  00:00:02
(22/22): centos-logos-70.0.6-3.el7.centos.noarch.rpm                                                                                                                                                                  |  21 MB  00:00:10
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Total                                                                                                                                                                                                        2.5 MB/s |  26 MB  00:00:10
Retrieving key from file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-7
Importing GPG key 0x352C64E5:
 Userid     : "Fedora EPEL (7) <epel@fedoraproject.org>"
 Fingerprint: 91e9 7d7c 4a5e 96f1 7f3e 888f 6a2f aea2 352c 64e5
 Package    : epel-release-7-11.noarch (@extras)
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-7
Is this ok [y/N]: y
Running transaction check
Running transaction test
Transaction test succeeded
Running transaction
  Installing : fontpackages-filesystem-1.44-8.el7.noarch                                                                                                                                                                                1/22
  Installing : dejavu-fonts-common-2.33-6.el7.noarch                                                                                                                                                                                    2/22
  Installing : dejavu-sans-fonts-2.33-6.el7.noarch                                                                                                                                                                                      3/22
  Installing : fontconfig-2.13.0-4.3.el7.x86_64                                                                                                                                                                                         4/22
  Installing : gperftools-libs-2.6.1-1.el7.x86_64                                                                                                                                                                                       5/22
  Installing : libXau-1.0.8-2.1.el7.x86_64                                                                                                                                                                                              6/22
  Installing : libxcb-1.13-1.el7.x86_64                                                                                                                                                                                                 7/22
  Installing : centos-indexhtml-7-9.el7.centos.noarch                                                                                                                                                                                   8/22
  Installing : libjpeg-turbo-1.2.90-8.el7.x86_64                                                                                                                                                                                        9/22
  Installing : libX11-common-1.6.7-2.el7.noarch                                                                                                                                                                                        10/22
  Installing : libX11-1.6.7-2.el7.x86_64                                                                                                                                                                                               11/22
  Installing : libXpm-3.5.12-1.el7.x86_64                                                                                                                                                                                              12/22
  Installing : gd-2.0.35-26.el7.x86_64                                                                                                                                                                                                 13/22
  Installing : centos-logos-70.0.6-3.el7.centos.noarch                                                                                                                                                                                 14/22
  Installing : 1:nginx-filesystem-1.16.1-1.el7.noarch                                                                                                                                                                                  15/22
  Installing : 1:nginx-mod-mail-1.16.1-1.el7.x86_64                                                                                                                                                                                    16/22
  Installing : 1:nginx-mod-http-perl-1.16.1-1.el7.x86_64                                                                                                                                                                               17/22
  Installing : 1:nginx-mod-stream-1.16.1-1.el7.x86_64                                                                                                                                                                                  18/22
  Installing : 1:nginx-mod-http-xslt-filter-1.16.1-1.el7.x86_64                                                                                                                                                                        19/22
  Installing : 1:nginx-1.16.1-1.el7.x86_64                                                                                                                                                                                             20/22
  Installing : 1:nginx-mod-http-image-filter-1.16.1-1.el7.x86_64                                                                                                                                                                       21/22
  Installing : 1:nginx-all-modules-1.16.1-1.el7.noarch                                                                                                                                                                                 22/22
  Verifying  : fontconfig-2.13.0-4.3.el7.x86_64                                                                                                                                                                                         1/22
  Verifying  : 1:nginx-filesystem-1.16.1-1.el7.noarch                                                                                                                                                                                   2/22
  Verifying  : 1:nginx-mod-mail-1.16.1-1.el7.x86_64                                                                                                                                                                                     3/22
  Verifying  : 1:nginx-mod-http-perl-1.16.1-1.el7.x86_64                                                                                                                                                                                4/22
  Verifying  : fontpackages-filesystem-1.44-8.el7.noarch                                                                                                                                                                                5/22
  Verifying  : centos-logos-70.0.6-3.el7.centos.noarch                                                                                                                                                                                  6/22
  Verifying  : dejavu-fonts-common-2.33-6.el7.noarch                                                                                                                                                                                    7/22
  Verifying  : libX11-1.6.7-2.el7.x86_64                                                                                                                                                                                                8/22
  Verifying  : libX11-common-1.6.7-2.el7.noarch                                                                                                                                                                                         9/22
  Verifying  : libxcb-1.13-1.el7.x86_64                                                                                                                                                                                                10/22
  Verifying  : libXpm-3.5.12-1.el7.x86_64                                                                                                                                                                                              11/22
  Verifying  : 1:nginx-mod-stream-1.16.1-1.el7.x86_64                                                                                                                                                                                  12/22
  Verifying  : dejavu-sans-fonts-2.33-6.el7.noarch                                                                                                                                                                                     13/22
  Verifying  : 1:nginx-1.16.1-1.el7.x86_64                                                                                                                                                                                             14/22
  Verifying  : libjpeg-turbo-1.2.90-8.el7.x86_64                                                                                                                                                                                       15/22
  Verifying  : 1:nginx-all-modules-1.16.1-1.el7.noarch                                                                                                                                                                                 16/22
  Verifying  : 1:nginx-mod-http-xslt-filter-1.16.1-1.el7.x86_64                                                                                                                                                                        17/22
  Verifying  : centos-indexhtml-7-9.el7.centos.noarch                                                                                                                                                                                  18/22
  Verifying  : 1:nginx-mod-http-image-filter-1.16.1-1.el7.x86_64                                                                                                                                                                       19/22
  Verifying  : libXau-1.0.8-2.1.el7.x86_64                                                                                                                                                                                             20/22
  Verifying  : gperftools-libs-2.6.1-1.el7.x86_64                                                                                                                                                                                      21/22
  Verifying  : gd-2.0.35-26.el7.x86_64                                                                                                                                                                                                 22/22

Installed:
  nginx.x86_64 1:1.16.1-1.el7

Dependency Installed:
  centos-indexhtml.noarch 0:7-9.el7.centos      centos-logos.noarch 0:70.0.6-3.el7.centos           dejavu-fonts-common.noarch 0:2.33-6.el7     dejavu-sans-fonts.noarch 0:2.33-6.el7              fontconfig.x86_64 0:2.13.0-4.3.el7
  fontpackages-filesystem.noarch 0:1.44-8.el7   gd.x86_64 0:2.0.35-26.el7                           gperftools-libs.x86_64 0:2.6.1-1.el7        libX11.x86_64 0:1.6.7-2.el7                        libX11-common.noarch 0:1.6.7-2.el7
  libXau.x86_64 0:1.0.8-2.1.el7                 libXpm.x86_64 0:3.5.12-1.el7                        libjpeg-turbo.x86_64 0:1.2.90-8.el7         libxcb.x86_64 0:1.13-1.el7                         nginx-all-modules.noarch 1:1.16.1-1.el7
  nginx-filesystem.noarch 1:1.16.1-1.el7        nginx-mod-http-image-filter.x86_64 1:1.16.1-1.el7   nginx-mod-http-perl.x86_64 1:1.16.1-1.el7   nginx-mod-http-xslt-filter.x86_64 1:1.16.1-1.el7   nginx-mod-mail.x86_64 1:1.16.1-1.el7
  nginx-mod-stream.x86_64 1:1.16.1-1.el7

Complete!
```


* Désactivation de SELinux

```
[vagrant@myhost ~]$ sudo vim /etc/sysconfig/selinux

# This file controls the state of SELinux on the system.
# SELINUX= can take one of these three values:
#     enforcing - SELinux security policy is enforced.
#     permissive - SELinux prints warnings instead of enforcing.
#     disabled - No SELinux policy is loaded.
SELINUX=disabled
# SELINUXTYPE= can take one of three values:
#     targeted - Targeted processes are protected,
#     minimum - Modification of targeted policy. Only selected processes are protected.
#     mls - Multi Level Security protection.
SELINUXTYPE=targeted

[vagrant@myhost ~]$ exit
logout
Connection to 127.0.0.1 closed.
PS C:\Users\josep\Desktop\studies\ynov\b2\linux\vagrant> vagrant halt
==> default: Attempting graceful shutdown of VM...
PS C:\Users\josep\Desktop\studies\ynov\b2\linux\vagrant> vagrant up
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Clearing any previously set forwarded ports...
==> default: Clearing any previously set network interfaces...
==> default: Preparing network interfaces based on configuration...
    default: Adapter 1: nat
    default: Adapter 2: hostonly
==> default: Forwarding ports...
    default: 22 (guest) => 2222 (host) (adapter 1)
==> default: Running 'pre-boot' VM customizations...
==> default: Booting VM...
==> default: Waiting for machine to boot. This may take a few minutes...
    default: SSH address: 127.0.0.1:2222
    default: SSH username: vagrant
    default: SSH auth method: private key
==> default: Machine booted and ready!
==> default: Checking for guest additions in VM...
    default: No guest additions were detected on the base box for this VM! Guest
    default: additions are required for forwarded ports, shared folders, host only
    default: networking, and more. If SSH fails on this machine, please install
    default: the guest additions and repackage the box to continue.
    default:
    default: This is not an error message; everything may continue to work properly,
    default: in which case you may ignore this message.
==> default: Setting hostname...
==> default: Configuring and enabling network interfaces...
==> default: Machine already provisioned. Run `vagrant provision` or use the `--provision`
==> default: flag to force provisioning. Provisioners marked to run always will still run.
PS C:\Users\josep\Desktop\studies\ynov\b2\linux\vagrant> vagrant ssh
Last login: Tue Sep 29 09:42:46 2020 from 10.0.2.2
[vagrant@myhost ~]$ sestatus
SELinux status:                 disabled
```

* Firewall (avec firewalld, en utilisant la commande firewall-cmd) activé au boot de la VM (Ne laisse passser que le strict nécessaire (SSH))

```
[vagrant@myhost ~]$ sudo firewalld
[vagrant@myhost ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: eth0 eth1
  sources:
  services: dhcpv6-client ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[vagrant@myhost ~]$ sudo firewall-cmd --add-port=22/tcp --permanent
success
[vagrant@myhost ~]$ sudo firewall-cmd --reload
success

[vagrant@myhost ~]$ sudo systemctl enable firewalld
Created symlink from /etc/systemd/system/dbus-org.fedoraproject.FirewallD1.service to /usr/lib/systemd/system/firewalld.service.
Created symlink from /etc/systemd/system/multi-user.target.wants/firewalld.service to /usr/lib/systemd/system/firewalld.service.

[vagrant@myhost ~]$ exit
logout
Connection to 127.0.0.1 closed.
PS C:\Users\josep\Desktop\studies\ynov\b2\linux\vagrant> vagrant halt
==> default: Attempting graceful shutdown of VM...
PS C:\Users\josep\Desktop\studies\ynov\b2\linux\vagrant> vagrant up
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Clearing any previously set forwarded ports...
==> default: Clearing any previously set network interfaces...
==> default: Preparing network interfaces based on configuration...
    default: Adapter 1: nat
    default: Adapter 2: hostonly
==> default: Forwarding ports...
    default: 22 (guest) => 2222 (host) (adapter 1)
==> default: Running 'pre-boot' VM customizations...
==> default: Booting VM...
==> default: Waiting for machine to boot. This may take a few minutes...
    default: SSH address: 127.0.0.1:2222
    default: SSH username: vagrant
    default: SSH auth method: private key
==> default: Machine booted and ready!
==> default: Checking for guest additions in VM...
    default: No guest additions were detected on the base box for this VM! Guest
    default: additions are required for forwarded ports, shared folders, host only
    default: networking, and more. If SSH fails on this machine, please install
    default: the guest additions and repackage the box to continue.
    default:
    default: This is not an error message; everything may continue to work properly,
    default: in which case you may ignore this message.
==> default: Setting hostname...
==> default: Configuring and enabling network interfaces...
==> default: Machine already provisioned. Run `vagrant provision` or use the `--provision`
==> default: flag to force provisioning. Provisioners marked to run always will still run.
PS C:\Users\josep\Desktop\studies\ynov\b2\linux\vagrant> vagrant ssh
Last login: Tue Sep 29 10:11:25 2020 from 10.0.2.2
[vagrant@myhost ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: eth0 eth1
  sources:
  services: dhcpv6-client ssh
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

#### On repackage en partant de la box centos/7

```
PS C:\Users\josep\Desktop\studies\ynov\b2\linux\vagrant> vagrant package --output centos7-custom.box
==> default: Attempting graceful shutdown of VM...
==> default: Clearing any previously set forwarded ports...
==> default: Exporting VM...
==> default: Compressing package to: C:/Users/josep/Desktop/studies/ynov/b2/linux/vagrant/centos7-custom.box
PS C:\Users\josep\Desktop\studies\ynov\b2\linux\vagrant> vagrant box add centos7-custom centos7-custom.box
==> box: Box file was not detected as metadata. Adding it directly...
==> box: Adding box 'centos7-custom' (v0) for provider:
    box: Unpacking necessary files from: file://C:/Users/josep/Desktop/studies/ynov/b2/linux/vagrant/centos7-custom.box
    box:
==> box: Successfully added box 'centos7-custom' (v0) for 'virtualbox'!
PS C:\Users\josep\Desktop\studies\ynov\b2\linux\vagrant> vagrant box list
centos/7       (virtualbox, 2004.01)
centos7-custom (virtualbox, 0)
```

*On ajoute le nom de la vm (centos7-custom.box) dans le vagrantfile*

```
Vagrant.configure("2")do|config|
  config.vm.box="centos7-custom"
  file_to_disk = 'VagrantDisk.vdi'

  # Ajoutez cette ligne afin d'accélérer le démarrage de la VM (si une erreur 'vbguest' est levée, voir la note un peu plus bas)
  config.vbguest.auto_update = false

  # Désactive les updates auto qui peuvent ralentir le lancement de la machine
  config.vm.box_check_update = false 

  # La ligne suivante permet de désactiver le montage d'un dossier partagé (ne marche pas tout le temps directement suivant vos OS, versions d'OS, etc.)
  config.vm.synced_folder ".", "/vagrant", disabled: true

  config.vm.provider :virtualbox do |vb|
  # Configure 1G de Ram sur la VM
    vb.customize ["modifyvm", :id, "--memory", "1024"]
    vb.name = "centos7-custom"
    vb.customize ['createhd', '--filename', file_to_disk, '--size', 5 * 1024]
    vb.customize ['storageattach', :id, '--storagectl', 'IDE', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', file_to_disk]
  end

  # Définition d'un hostname
  config.vm.hostname = "myhost.local"

  # Définition d'une IP
  config.vm.network "private_network", ip: "192.168.2.11"

  # Appel du script
  config.vm.provision "shell", path: "script.sh"
end
```

*Puis on détruit et on lance la box*

```
PS C:\Users\josep\Desktop\studies\ynov\b2\linux\vagrant> vagrant destroy -f
==> default: Forcing shutdown of VM...
==> default: Destroying VM and associated drives...
PS C:\Users\josep\Desktop\studies\ynov\b2\linux\vagrant> vagrant up
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Importing base box 'centos7-custom'...
==> default: Matching MAC address for NAT networking...
==> default: Setting the name of the VM: vagrantvm_tp2
==> default: Clearing any previously set network interfaces...
==> default: Preparing network interfaces based on configuration...
    default: Adapter 1: nat
    default: Adapter 2: hostonly
==> default: Forwarding ports...
    default: 22 (guest) => 2222 (host) (adapter 1)
==> default: Running 'pre-boot' VM customizations...
==> default: Booting VM...
==> default: Waiting for machine to boot. This may take a few minutes...
    default: SSH address: 127.0.0.1:2222
    default: SSH username: vagrant
    default: SSH auth method: private key
==> default: Machine booted and ready!
==> default: Checking for guest additions in VM...
    default: No guest additions were detected on the base box for this VM! Guest
    default: additions are required for forwarded ports, shared folders, host only
    default: networking, and more. If SSH fails on this machine, please install
    default: the guest additions and repackage the box to continue.
    default:
    default: This is not an error message; everything may continue to work properly,
    default: in which case you may ignore this message.
==> default: Setting hostname...
==> default: Configuring and enabling network interfaces...
==> default: Running provisioner: shell...
    default: Running: C:/Users/josep/AppData/Local/Temp/vagrant-shell20200929-11144-18s11sa.sh
    default: Loaded plugins: fastestmirror
    default: Loading mirror speeds from cached hostfile
    default:  * base: centos.mirror.ate.info
    default:  * epel: mir01.syntis.net
    default:  * extras: centos.mirror.ate.info
    default:  * updates: centos.mirror.ate.info
    default: Package 2:vim-enhanced-7.4.629-6.el7.x86_64 already installed and latest version
    default: Nothing to do
PS C:\Users\josep\Desktop\studies\ynov\b2\linux\vagrant> vagrant ssh
Last login: Tue Sep 29 10:16:54 2020 from 10.0.2.2
[vagrant@myhost ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: eth0 eth1
  sources:
  services: dhcpv6-client ssh
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[vagrant@myhost ~]$ sestatus
SELinux status:                 disabled
```

<br>

## III. Multi-node deployment

<br>

🌞 Créer un Vagrantfile qui lance deux machines virtuelles, les VMs DOIVENT utiliser votre box repackagée comme base :

```
Vagrant.configure("2")do|config|
  config.vm.box="centos7-custom"
  file_to_disk = 'VagrantDisk.vdi'
  file_to_disk2 = 'VagrantDisk2.vdi'

  # Ajoutez cette ligne afin d'accélérer le démarrage de la VM (si une erreur 'vbguest' est levée, voir la note un peu plus bas)
  config.vbguest.auto_update = false

  # Désactive les updates auto qui peuvent ralentir le lancement de la machine
  config.vm.box_check_update = false 

  # La ligne suivante permet de désactiver le montage d'un dossier partagé (ne marche pas tout le temps directement suivant vos OS, versions d'OS, etc.)
  config.vm.synced_folder ".", "/vagrant", disabled: true

  config.vm.define "node1" do |node1|
    node1.vm.network "private_network", ip: "192.168.2.11"
    node1.vm.hostname = "node1.tp2.b2"
    node1.vm.provider :virtualbox do |vb|
      vb.customize ["modifyvm", :id, "--memory", "1024"]
      vb.name = "node1.tp2.b2"
    end
  unless File.exist?(file_to_disk)
    node1.vm.provider "virtualbox" do |vb|
      vb.customize ['createhd', '--filename', file_to_disk, '--size', 5 * 1024]
      vb.customize ['storageattach', :id, '--storagectl', 'IDE', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', file_to_disk]
     end  
    end
  end

  config.vm.define "node2" do |node2|
    node2.vm.network "private_network", ip: "192.168.2.12"
    node2.vm.hostname = "node2.tp2.b2"
    node2.vm.provider :virtualbox do |vb|
      vb.customize ["modifyvm", :id, "--memory", "1024"]
      vb.name = "node2.tp2.b2"      
    end
  unless File.exist?(file_to_disk2)
    node2.vm.provider "virtualbox" do |vb|
      vb.customize ['createhd', '--filename', file_to_disk2, '--size', 5 * 1024]
      vb.customize ['storageattach', :id, '--storagectl', 'IDE', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', file_to_disk2]
     end
    end 
  end

  # Appel du script
  #config.vm.provision "shell", path: "script.sh"
end
```

*Lancement des deux vm*

```
PS C:\Users\josep\Desktop\studies\ynov\b2\linux\vagrant> vagrant up
Bringing machine 'node1' up with 'virtualbox' provider...
Bringing machine 'node2' up with 'virtualbox' provider...
==> node1: Importing base box 'centos7-custom'...
==> node1: Matching MAC address for NAT networking...
==> node1: Setting the name of the VM: node1.tp2.b2
==> node1: Clearing any previously set network interfaces...
==> node1: Preparing network interfaces based on configuration...
    node1: Adapter 1: nat
    node1: Adapter 2: hostonly
==> node1: Forwarding ports...
    node1: 22 (guest) => 2222 (host) (adapter 1)
==> node1: Running 'pre-boot' VM customizations...
==> node1: Booting VM...
==> node1: Waiting for machine to boot. This may take a few minutes...
    node1: SSH address: 127.0.0.1:2222
    node1: SSH username: vagrant
    node1: SSH auth method: private key
==> node1: Machine booted and ready!
==> node1: Checking for guest additions in VM...
    node1: No guest additions were detected on the base box for this VM! Guest
    node1: additions are required for forwarded ports, shared folders, host only
    node1: networking, and more. If SSH fails on this machine, please install
    node1: the guest additions and repackage the box to continue.
    node1:
    node1: This is not an error message; everything may continue to work properly,
    node1: in which case you may ignore this message.
==> node1: Setting hostname...
==> node1: Configuring and enabling network interfaces...
==> node2: Importing base box 'centos7-custom'...
==> node2: Matching MAC address for NAT networking...
==> node2: Setting the name of the VM: node2.tp2.b2
==> node2: Fixed port collision for 22 => 2222. Now on port 2200.
==> node2: Clearing any previously set network interfaces...
==> node2: Preparing network interfaces based on configuration...
    node2: Adapter 1: nat
    node2: Adapter 2: hostonly
==> node2: Forwarding ports...
    node2: 22 (guest) => 2200 (host) (adapter 1)
==> node2: Running 'pre-boot' VM customizations...
==> node2: Booting VM...
==> node2: Waiting for machine to boot. This may take a few minutes...
    node2: SSH address: 127.0.0.1:2200
    node2: SSH username: vagrant
    node2: SSH auth method: private key
==> node2: Machine booted and ready!
==> node2: Checking for guest additions in VM...
    node2: No guest additions were detected on the base box for this VM! Guest
    node2: additions are required for forwarded ports, shared folders, host only
    node2: networking, and more. If SSH fails on this machine, please install
    node2: the guest additions and repackage the box to continue.
    node2:
    node2: This is not an error message; everything may continue to work properly,
    node2: in which case you may ignore this message.
==> node2: Setting hostname...
==> node2: Configuring and enabling network interfaces...
PS C:\Users\josep\Desktop\studies\ynov\b2\linux\vagrant> vagrant ssh node1
Last login: Tue Sep 29 10:16:54 2020 from 10.0.2.2
Last login: Tue Sep 29 10:16:54 2020 from 10.0.2.2
[vagrant@node1 ~]$ lsblk
NAME   MAJ:MIN RM SIZE RO TYPE MOUNTPOINT
sda      8:0    0  40G  0 disk
└─sda1   8:1    0  40G  0 part /
sdb      8:16   0   5G  0 disk
[vagrant@node1 ~]$ exit
logout
Connection to 127.0.0.1 closed.
PS C:\Users\josep\Desktop\studies\ynov\b2\linux\vagrant> vagrant ssh node2
Last login: Tue Sep 29 10:16:54 2020 from 10.0.2.2
Last login: Tue Sep 29 10:16:54 2020 from 10.0.2.2
[vagrant@node2 ~]$ lsblk
NAME   MAJ:MIN RM SIZE RO TYPE MOUNTPOINT
sda      8:0    0  40G  0 disk
└─sda1   8:1    0  40G  0 part /
sdb      8:16   0   5G  0 disk
```

<br>

## IV. Automation here we (slowly) come

<br>

### 🌞 Créer un Vagrantfile qui automatise la résolution du TP1

#### Le script
```
#!/bin/bash

useradd admin -m
useradd backup -M -s /sbin/nologin

usermod -aG wheel admin

useradd web -M -s /sbin/nologin

mkdir /srv/site{1,2}

mkdir /srv/save_web
cp /vagrant/tp2_backup.sh /srv
cp /vagrant/backup /var/spool/cron

chown backup /srv/save_web
chown backup /srv/tp2_backup.sh

chmod 755 /srv/save_web
chmod 755 /srv/tp2_backup.sh

firewall-cmd --add-port=80/tcp --permanent
firewall-cmd --add-port=443/tcp --permanent
firewall-cmd --reload

mv /vagrant/nginx.conf /etc/nginx/

openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt -subj "/C=GB/ST=London/L=London/O=Global Security/OU=IT Department/CN=node1.tp2.b2"

mv server.key /etc/pki/tls/private/node1.tp2.b2.key
chmod 400 /etc/pki/tls/private/node1.tp2.b2.key
chown web:web /etc/pki/tls/private/node1.tp2.b2.key

mv server.crt /etc/pki/tls/certs/node1.tp2.b2.crt
chmod 444 /etc/pki/tls/certs/node1.tp2.b2.crt
chown web:web /etc/pki/tls/certs/node1.tp2.b2.crt

echo '<h1>Hello from site 1</h1>' | tee /srv/site1/index.html
echo '<h1>Hello from site 2</h1>' | tee /srv/site2/index.html
chown web:web /srv/site1 -R
chmod 755 /srv/site1 /srv/site2
chmod 400 /srv/site1/index.html /srv/site2/index.html

echo "192.168.2.12 node2.tp2.b2" >> /etc/hosts

systemctl start nginx
```

#### L'appel du script pour node1 dans vagrantfile

```
[...]

 config.vm.define "node1" do |node1|
    node1.vm.network "private_network", ip: "192.168.2.11"
    node1.vm.hostname = "node1.tp2.b2"
    node1.vm.provision "shell", path: "script.sh"
    node1.vm.provider :virtualbox do |vb|
      vb.customize ["modifyvm", :id, "--memory", "1024"]
      vb.name = "node1.tp2.b2"
    end

[...]
```

#### Test du curl

```
PS C:\Users\josep\Desktop\studies\ynov\b2\linux\vagrant> vagrant ssh node2
Last login: Thu Oct  1 10:07:56 2020 from 10.0.2.2
Last login: Thu Oct  1 10:07:56 2020 from 10.0.2.2
[vagrant@node2 ~]$ curl -kL https://node1.tp2.b2/site1/index.html
<h1>Hello from site 1</h1>
```