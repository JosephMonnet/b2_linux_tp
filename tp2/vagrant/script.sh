#!/bin/bash

useradd admin -m

usermod -aG wheel admin

useradd web -M -s /sbin/nologin

mkdir /srv/site{1,2}

firewall-cmd --add-port=80/tcp --permanent
firewall-cmd --add-port=443/tcp --permanent
firewall-cmd --reload


mv /vagrant/nginx.conf /etc/nginx/

openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt -subj "/C=GB/ST=London/L=London/O=Global Security/OU=IT Department/CN=node1.tp2.b2"

mv server.key /etc/pki/tls/private/node1.tp2.b2.key
chmod 400 /etc/pki/tls/private/node1.tp2.b2.key
chown web:web /etc/pki/tls/private/node1.tp2.b2.key

mv server.crt /etc/pki/tls/certs/node1.tp2.b2.crt
chmod 444 /etc/pki/tls/certs/node1.tp2.b2.crt
chown web:web /etc/pki/tls/certs/node1.tp2.b2.crt

echo '<h1>Hello from site 1</h1>' | tee /srv/site1/index.html
echo '<h1>Hello from site 2</h1>' | tee /srv/site2/index.html
chown web:web /srv/site1 -R
chmod 755 /srv/site1 /srv/site2
chmod 744 /srv/site1/index.html /srv/site2/index.html

echo "192.168.2.12 node2.tp2.b2" >> /etc/hosts

systemctl start nginx