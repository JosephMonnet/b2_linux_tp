#!/bin/bash

bash <(curl -Ss https://my-netdata.io/kickstart.sh) --dont-wait
sed -i "/DISCORD_WEBHOOK_URL=/c\DISCORD_WEBHOOK_URL='https://discord.com/channels/760165742036516866/760165742620180502'" /usr/lib/netdata/conf.d/health_alarm_notify.conf

# --------------------------------------GITEA-----------------------------------------

sudo yum install git
firewall-cmd --add-port=3000/tcp --permanent
firewall-cmd --reload

if [[ ! -f /usr/local/bin/gitea ]]; then*
    wget -O /usr/local/bin/gitea https://dl.gitea.io/gitea/1.12.5/gitea-1.12.5-linux-amd64
    chmod +x gitea
fi

if [[ ! $(cat /usr/local/bin/gitea/gitea-1.12.5-linux-amd64.sha256) == $(sha256sum gitea-1.12.5-linux-amd64 | cut -d' ' -f1) ]]
    echo "mec ton paquet est chelou... rm moi cette merde"
fi

useradd git
usermod -aG wheel giter

mkdir -p /var/lib/gitea/{custom,data,log}
chown -R git:git /var/lib/gitea/
chmod -R 750 /var/lib/gitea/
mkdir /etc/gitea
chown root:git /etc/gitea
chmod 770 /etc/gitea

export GITEA_WORK_DIR=/var/lib/gitea/

cp gitea.service /etc/systemd/system

sudo systemctl start gitea
sudo systemctl enable gitea

# ----------------------------------------NFS----------------------------------------

yum install -y nfs-utils
mkdir /mnt/gitea
mount 192.168.5.11:/home/vagrant/gitea /mnt/gitea

# --------------------------------------BACKUP---------------------------------------

echo "#!/bin/sh
tar -czf backup.tar.gz /etc/gitea/
cp ./backup.tar.gz /mnt/gitea" > /mnt/gitea/backup.sh

echo "0 * * * * /mnt/gitea/backup.sh" > /var/spool/cron/vagrant