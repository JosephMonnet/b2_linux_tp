#!/bin/bash

bash <(curl -Ss https://my-netdata.io/kickstart.sh) --dont-wait
sed -i "/DISCORD_WEBHOOK_URL=/c\DISCORD_WEBHOOK_URL='https://discord.com/channels/760165742036516866/760165742620180502'" /usr/lib/netdata/conf.d/health_alarm_notify.conf

# ----------------------------------------NGINX--------------------------------------------------

cp nginx.conf /etc/nginx

systemctl start nginx

# -----------------------------------------NFS---------------------------------------------------

yum install -y nfs-utils nfs-utils-lib

mkdir /partage
mkdir -p /partage/nginx
mount 192.168.5.11:/saves/nginx /partage/nginx

# --------------------------------------BACKUP---------------------------------------------------

echo "#!/bin/sh
cp /etc/nginx/nginx.conf /mnt/nginx" > /mnt/nginx/backup.sh

echo "0 * * * * /mnt/nginx/backup.sh" > /var/spool/cron/vagrant