#!/bin/bash

bash <(curl -Ss https://my-netdata.io/kickstart.sh) --dont-wait
sed -i "/DISCORD_WEBHOOK_URL=/c\DISCORD_WEBHOOK_URL='https://discord.com/channels/760165742036516866/760165742620180502'" /usr/lib/netdata/conf.d/health_alarm_notify.conf

# -----------------------------------------------BDD MariaDB-----------------------------------------------------

sudo yum install -y mariadb-server
systemctl start mariadb
systemctl enable mariadb

firewall-cmd --add-port=3306/tcp --permanent
firewall-cmd --reload

echo "[mysqld]
bind-address = 192.168.2.11
datadir=/var/lib/mysql
socket=/var/lib/mysql/mysql.sock
# Disabling symbolic-links is recommended to prevent assorted security risks
symbolic-links=0
# Settings user and group are ignored when systemd is used.
# If you need to run mysqld under a different user or group,
# customize your systemd unit file for mariadb according to the
# instructions in http://fedoraproject.org/wiki/Systemd
[mysqld_safe]
log-error=/var/log/mariadb/mariadb.log
pid-file=/var/run/mariadb/mariadb.pid
#
# include all files from the config directory
#
!includedir /etc/my.cnf.d" > /etc/my.cnf

# SET old_passwords=0;
# CREATE USER 'gitea'@'192.168.2.11' IDENTIFIED BY 'gitea';
# CREATE DATABASE giteadb CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci';
# GRANT ALL PRIVILEGES ON giteadb.* TO 'gitea'@'192.168.2.11';
# FLUSH PRIVILEGES;

systemctl restart mariadb.service

# ----------------------------------------NFS-------------------------------------------------

yum install -y nfs-utils nfs-utils-lib

sudo mkdir /partage
sudo mkdir /partage/bdd
mount 192.168.5.11:/saves/bdd /partage/bdd

# --------------------------------------BACKUP------------------------------------------------

echo "#!/bin/sh
mysql -u root -proot giteadb > bdd-dump.sql
mv ./bdd-dump.sql /mnt/db" > /mnt/db/backup.sh

echo "0 * * * * /mnt/db/backup.sh" > /var/spool/cron/vagrant