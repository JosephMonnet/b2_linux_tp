# TP4 : Déploiement multi-noeud

### VM ```centos7-tp4.box``` repackager à partir d'une VM ```centos7``` avec :

* SELINUX ```disabled```
* ```yum update```
* ```yum install epel-release```
* ```yum install vim```
* ```yum install nginx```
* firewall <br>
```sudo firewall-cmd --add-port=80/tcp --permanent``` <br>
```sudo firewall-cmd --add-port=443/tcp --permanent``` <br>
```sudo firewall-cmd --reload``` <br>
```systemctl start firewall``` <br>
```systemctl enable firewall```

<br>

### Tableau d'adressage

| Nom               | IP           | Rôle            |
| ----------------- | ------------ | --------------- |
| node-gitea.tp4.b2 | 192.168.2.11 | Gitea           |
| node-maria.tp4.b2 | 192.168.3.11 | Base de données |
| node-nginx.tp4.b2 | 192.168.4.11 | NGINX           |
| node-nfs.tp4.b2   | 192.168.5.11 | Serveur NFS     |
