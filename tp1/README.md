# TP1 : Déploiement classique

<br>

## 0. Prérequis

### 🌞 Setup de deux machines CentOS7 configurée de façon basique.

#### Les machines doivent être partitionnées avec LVM. Les partitions seront réparties comme suit :

- Ajouter un deuxième disque de 5Go à la machine

*node1*

```
[mrkasparov@node1 ~]$ lsblk
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda               8:0    0    8G  0 disk
├─sda1            8:1    0    1G  0 part /boot
└─sda2            8:2    0    7G  0 part
  ├─centos-root 253:0    0  6,2G  0 lvm  /
  └─centos-swap 253:1    0  820M  0 lvm  [SWAP]
sdb               8:16   0  5,1G  0 disk
sr0              11:0    1 1024M  0 rom
```

*node2*

```
[mrkasparov@node2 ~]$ lsblk
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda               8:0    0    8G  0 disk
├─sda1            8:1    0    1G  0 part /boot
└─sda2            8:2    0    7G  0 part
  ├─centos-root 253:0    0  6,2G  0 lvm  /
  └─centos-swap 253:1    0  820M  0 lvm  [SWAP]
sdb               8:16   0  5,1G  0 disk
sr0              11:0    1 1024M  0 rom
```

- Partitionner le nouveau disque avec LVM

```
[mrkasparov@node1 ~]$ sudo pvcreate /dev/sdb
[sudo] Mot de passe de mrkasparov : 
  Physical volume "/dev/sdb" successfully created.
  
[mrkasparov@node1 ~]$ lsblk
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda               8:0    0    8G  0 disk
├─sda1            8:1    0    1G  0 part /boot
└─sda2            8:2    0    7G  0 part
  ├─centos-root 253:0    0  6,2G  0 lvm  /
  └─centos-swap 253:1    0  820M  0 lvm  [SWAP]
sdb               8:16   0  5,1G  0 disk
sr0              11:0    1 1024M  0 rom

[mrkasparov@node1 ~]$ sudo pvs
  PV         VG     Fmt  Attr PSize  PFree
  /dev/sda2  centos lvm2 a--  <7,00g     0
  /dev/sdb          lvm2 ---  <5,06g <5,06g
  
[mrkasparov@node1 ~]$ sudo vgcreate data /dev/sdb
  Volume group "data" successfully created
  
[mrkasparov@node1 ~]$ sudo vgs
  VG     #PV #LV #SN Attr   VSize  VFree
  centos   1   2   0 wz--n- <7,00g    0
  data     1   0   0 wz--n-  5,05g 5,05g

[mrkasparov@node1 ~]$ sudo lvcreate -L 3G data -n data2
  Logical volume "data2" created.
  
[mrkasparov@node1 ~]$ sudo vgs
  VG     #PV #LV #SN Attr   VSize  VFree
  centos   1   2   0 wz--n- <7,00g    0
  data     1   1   0 wz--n-  5,05g 2,05g
  
[mrkasparov@node1 ~]$ lsblk
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda               8:0    0    8G  0 disk
├─sda1            8:1    0    1G  0 part /boot
└─sda2            8:2    0    7G  0 part
  ├─centos-root 253:0    0  6,2G  0 lvm  /
  └─centos-swap 253:1    0  820M  0 lvm  [SWAP]
sdb               8:16   0  5,1G  0 disk
└─data-data2    253:2    0    3G  0 lvm
sr0              11:0    1 1024M  0 rom

[mrkasparov@node1 ~]$ sudo lvcreate -l 100%FREE data -n data1
  Logical volume "data1" created.
  
[mrkasparov@node1 ~]$ lsblk
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda               8:0    0    8G  0 disk
├─sda1            8:1    0    1G  0 part /boot
└─sda2            8:2    0    7G  0 part
  ├─centos-root 253:0    0  6,2G  0 lvm  /
  └─centos-swap 253:1    0  820M  0 lvm  [SWAP]
sdb               8:16   0  5,1G  0 disk
├─data-data2    253:2    0    3G  0 lvm
└─data-data1    253:3    0  2,1G  0 lvm
sr0              11:0    1 1024M  0 rom

[mrkasparov@node1 ~]$ sudo mkfs -t ext4 /dev/data/data1
mke2fs 1.42.9 (28-Dec-2013)
Étiquette de système de fichiers=
Type de système d'exploitation : Linux
Taille de bloc=4096 (log=2)
Taille de fragment=4096 (log=2)
« Stride » = 0 blocs, « Stripe width » = 0 blocs
134912 i-noeuds, 538624 blocs
26931 blocs (5.00%) réservés pour le super utilisateur
Premier bloc de données=0
Nombre maximum de blocs du système de fichiers=551550976
17 groupes de blocs
32768 blocs par groupe, 32768 fragments par groupe
7936 i-noeuds par groupe
Superblocs de secours stockés sur les blocs :
        32768, 98304, 163840, 229376, 294912

Allocation des tables de groupe : complété
Écriture des tables d'i-noeuds : complété
Création du journal (16384 blocs) : complété
Écriture des superblocs et de l'information de comptabilité du système de
fichiers : complété

[mrkasparov@node1 ~]$ sudo mkfs -t ext4 /dev/data/data2
mke2fs 1.42.9 (28-Dec-2013)
Étiquette de système de fichiers=
Type de système d'exploitation : Linux
Taille de bloc=4096 (log=2)
Taille de fragment=4096 (log=2)
« Stride » = 0 blocs, « Stripe width » = 0 blocs
196608 i-noeuds, 786432 blocs
39321 blocs (5.00%) réservés pour le super utilisateur
Premier bloc de données=0
Nombre maximum de blocs du système de fichiers=805306368
24 groupes de blocs
32768 blocs par groupe, 32768 fragments par groupe
8192 i-noeuds par groupe
Superblocs de secours stockés sur les blocs :
        32768, 98304, 163840, 229376, 294912

Allocation des tables de groupe : complété
Écriture des tables d'i-noeuds : complété
Création du journal (16384 blocs) : complété
Écriture des superblocs et de l'information de comptabilité du système de
fichiers : complété

[mrkasparov@node1 ~]$ sudo mkdir /srv/data1
[mrkasparov@node1 ~]$ sudo mkdir /srv/data2
```

- Les partitions doivent être montées automatiquement au démarrage (fichier /etc/fstab)

```
[mrkasparov@node1 ~]$ sudo nano /etc/fstab

#
# /etc/fstab
# Created by anaconda on Tue Sep 22 11:20:45 2020
#
# Accessible filesystems, by reference, are maintained under '/dev/disk'
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info
#
/dev/mapper/centos-root /                       xfs     defaults        0 0
UUID=83d6ce24-58b9-4766-a8f3-2581ca49042a /boot                   xfs     defaults        0 0
/dev/mapper/centos-swap swap                    swap    defaults        0 0
/dev/data/data1 /srv/data1 ext4 defaults 0 0
/dev/data/data2 /srv/data2 ext4 defaults 0 0

[mrkasparov@node1 ~]$ sudo umount /srv/data1
umount: /srv/data1 : non monté
[mrkasparov@node1 ~]$ sudo mount -av
/                         : ignoré
/boot                     : déjà monté
swap                      : ignoré
/srv/data1               : successfully mounted
/srv/data2               : successfully mounted

[mrkasparov@node1 ~]$ reboot
```


#### Un accès internet

- carte réseau dédiée

*node1 (enp0s3 en NAT)*

```
[mrkasparov@node1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:dd:85:4f brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global noprefixroute dynamic enp0s3
       valid_lft 85040sec preferred_lft 85040sec
    inet6 fe80::b46c:646c:b8b0:514/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:f6:d5:a9 brd ff:ff:ff:ff:ff:ff
    inet 192.168.1.11/24 brd 192.168.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fef6:d5a9/64 scope link
       valid_lft forever preferred_lft forever
```


*node2 (enp0s3 en NAT)*

```
[mrkasparov@node2 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:cc:27:b3 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global noprefixroute dynamic enp0s3
       valid_lft 85166sec preferred_lft 85166sec
    inet6 fe80::b46c:646c:b8b0:514/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:f2:6e:65 brd ff:ff:ff:ff:ff:ff
    inet 192.168.1.12/24 brd 192.168.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fef2:6e65/64 scope link
       valid_lft forever preferred_lft forever
```


- route par défaut

*node1 (Accès au réseau 10.0.2.0/24)*

```
[mrkasparov@node1 ~]$ ip r s
default via 10.0.2.2 dev enp0s3 proto dhcp metric 100
10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.15 metric 100
192.168.1.0/24 dev enp0s8 proto kernel scope link src 192.168.1.11 metric 101
```


*node2 (Accès au réseau 10.0.2.0/24)*

```
[mrkasparov@node2 ~]$ ip r s
default via 10.0.2.2 dev enp0s3 proto dhcp metric 100
10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.15 metric 100
192.168.1.0/24 dev enp0s8 proto kernel scope link src 192.168.1.12 metric 101
```


- Le ping

```
[mrkasparov@node1 ~]$ ping google.fr
PING google.fr (142.250.74.227) 56(84) bytes of data.
64 bytes from par10s40-in-f3.1e100.net (142.250.74.227): icmp_seq=1 ttl=110 time=38.0 ms
64 bytes from par10s40-in-f3.1e100.net (142.250.74.227): icmp_seq=2 ttl=110 time=401 ms
64 bytes from par10s40-in-f3.1e100.net (142.250.74.227): icmp_seq=3 ttl=110 time=76.5 ms
^C
--- google.fr ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2005ms
rtt min/avg/max/mdev = 38.026/171.916/401.206/162.892 ms
```


```
[mrkasparov@node2 ~]$ ping google.fr
PING google.fr (142.250.74.227) 56(84) bytes of data.
64 bytes from par10s40-in-f3.1e100.net (142.250.74.227): icmp_seq=1 ttl=110 time=49.6 ms
64 bytes from par10s40-in-f3.1e100.net (142.250.74.227): icmp_seq=2 ttl=110 time=395 ms
64 bytes from par10s40-in-f3.1e100.net (142.250.74.227): icmp_seq=3 ttl=110 time=61.6 ms
^C
--- google.fr ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2005ms
rtt min/avg/max/mdev = 49.665/169.004/395.685/160.362 ms
```


#### Un accès à un réseau local (les deux machines peuvent se ping)

- carte réseau dédiée

*node1 (enp0s8 en 192.168.1.11)*

```
[mrkasparov@node1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:dd:85:4f brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global noprefixroute dynamic enp0s3
       valid_lft 85040sec preferred_lft 85040sec
    inet6 fe80::b46c:646c:b8b0:514/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:f6:d5:a9 brd ff:ff:ff:ff:ff:ff
    inet 192.168.1.11/24 brd 192.168.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fef6:d5a9/64 scope link
       valid_lft forever preferred_lft forever
```


*node2 (enp0s8 en 192.168.1.12)*

```
[mrkasparov@node2 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:cc:27:b3 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global noprefixroute dynamic enp0s3
       valid_lft 85166sec preferred_lft 85166sec
    inet6 fe80::b46c:646c:b8b0:514/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:f2:6e:65 brd ff:ff:ff:ff:ff:ff
    inet 192.168.1.12/24 brd 192.168.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fef2:6e65/64 scope link
       valid_lft forever preferred_lft forever
```


- route locale

*node1 (Accès au réseau 192.168.1.0/24)*

```
[mrkasparov@node1 ~]$ ip r s
default via 10.0.2.2 dev enp0s3 proto dhcp metric 100
10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.15 metric 100
192.168.1.0/24 dev enp0s8 proto kernel scope link src 192.168.1.11 metric 101
```


*node2 (Accès au réseau 192.168.1.0/24)*

```
[mrkasparov@node2 ~]$ ip r s
default via 10.0.2.2 dev enp0s3 proto dhcp metric 100
10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.15 metric 100
192.168.1.0/24 dev enp0s8 proto kernel scope link src 192.168.1.12 metric 101
```


- Le ping

```
[mrkasparov@node1 ~]$ ping 192.168.1.12
PING 192.168.1.12 (192.168.1.12) 56(84) bytes of data.
64 bytes from 192.168.1.12: icmp_seq=1 ttl=64 time=0.757 ms
64 bytes from 192.168.1.12: icmp_seq=2 ttl=64 time=0.870 ms
^C
--- 192.168.1.12 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1006ms
rtt min/avg/max/mdev = 0.757/0.813/0.870/0.063 ms
```

#### Les machines doivent avoir un nom

*node1*

```
[mrkasparov@localhost ~]$ cat /etc/hostname
```

```
node1.tp1.b2
```

```
reboot
```

```
[mrkasparov@node1 ~]$
```


*node2*

```
[mrkasparov@localhost ~]$ cat /etc/hostname
```

```
node2.tp1.b2
```

```
reboot
```

```
[mrkasparov@node2 ~]$
```

#### Les machines doivent pouvoir se joindre par leurs noms respectifs

*node1*

```
[mrkasparov@node1 ~]$ sudo nano /etc/hosts


127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
192.168.1.11 node1.tp1.b2
192.168.1.12 node2.tp1.b2
```

```
[mrkasparov@node1 ~]$ ping node2.tp1.b2
PING node2.tp1.b2 (192.168.1.12) 56(84) bytes of data.
64 bytes from node2.tp1.b2 (192.168.1.12): icmp_seq=1 ttl=64 time=0.738 ms
64 bytes from node2.tp1.b2 (192.168.1.12): icmp_seq=2 ttl=64 time=0.775 ms
^C
--- node2.tp1.b2 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1006ms
rtt min/avg/max/mdev = 0.738/0.756/0.775/0.033 ms
```


*node2*

```
[mrkasparov@node2 ~]$ sudo nano /etc/hosts


127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
192.168.1.11 node1.tp1.b2
192.168.1.12 node2.tp1.b2
```

```
[mrkasparov@node2 ~]$ ping node1.tp1.b2
PING node1.tp1.b2 (192.168.1.11) 56(84) bytes of data.
64 bytes from node1.tp1.b2 (192.168.1.11): icmp_seq=1 ttl=64 time=0.844 ms
64 bytes from node1.tp1.b2 (192.168.1.11): icmp_seq=2 ttl=64 time=0.925 ms
^C
--- node1.tp1.b2 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 0.844/0.884/0.925/0.050 ms
```


#### Un utilisateur administrateur est créé sur les deux machines (il peut exécuter des commandes sudo en tant que root)

- Création d'un user

*admin1 sur la première VM*

```
[mrkasparov@node1 ~]$ su
Mot de passe :
[root@node1 mrkasparov]# adduser admin1
[root@node1 mrkasparov]# passwd admin1
Changement de mot de passe pour l'utilisateur admin1.
Nouveau mot de passe :
Retapez le nouveau mot de passe :
passwd : mise à jour réussie de tous les jetons d'authentification.
```

*admin2 sur la deuxième VM*

```
[mrkasparov@node2 ~]$ su
Mot de passe :
[root@node2 mrkasparov]# adduser admin2
[root@node2 mrkasparov]# passwd admin2
Changement de mot de passe pour l'utilisateur admin2.
Nouveau mot de passe :
Retapez le nouveau mot de passe :
passwd : mise à jour réussie de tous les jetons d'authentification.
```


- Modification de la conf sudo

*admin1 sur la première VM*

```
[root@node1 mrkasparov]# usermod -aG wheel admin1
[root@node1 mrkasparov]# su - admin1
[admin1@node1 ~]$
```

*admin2 sur la deuxième VM*

```
[root@node2 mrkasparov]# usermod -aG wheel admin2
[root@node2 mrkasparov]# su - admin2
[admin2@node2 ~]$
```


#### Vous n'utilisez QUE ssh pour administrer les machines


- status service sshd

*node1*

```
[admin1@node1 ~]$ sudo systemctl status sshd
[sudo] Mot de passe de admin1 : 
● sshd.service - OpenSSH server daemon
   Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
   Active: active (running) since jeu. 2020-09-24 09:33:49 CEST; 26min ago
     Docs: man:sshd(8)
           man:sshd_config(5)
 Main PID: 1098 (sshd)
   CGroup: /system.slice/sshd.service
           └─1098 /usr/sbin/sshd -D

sept. 24 09:33:48 node1.tp1.b2 systemd[1]: Starting OpenSSH server daemon...
sept. 24 09:33:49 node1.tp1.b2 sshd[1098]: Server listening on :: port 22.
sept. 24 09:33:49 node1.tp1.b2 sshd[1098]: Server listening on 0.0.0.0 port 22.
sept. 24 09:33:49 node1.tp1.b2 systemd[1]: Started OpenSSH server daemon.
sept. 24 09:35:26 node1.tp1.b2 sshd[1362]: Accepted password for mrkasparov from 192.168.1.1 port 52758 ssh2
```


*node2*

```
[admin2@node2 ~]$ sudo systemctl status sshd
[sudo] Mot de passe de admin2 : 
● sshd.service - OpenSSH server daemon
   Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
   Active: active (running) since jeu. 2020-09-24 09:34:05 CEST; 26min ago
     Docs: man:sshd(8)
           man:sshd_config(5)
 Main PID: 1099 (sshd)
   CGroup: /system.slice/sshd.service
           └─1099 /usr/sbin/sshd -D

sept. 24 09:34:05 node2.tp1.b2 systemd[1]: Starting OpenSSH server daemon...
sept. 24 09:34:05 node2.tp1.b2 sshd[1099]: Server listening on :: port 22.
sept. 24 09:34:05 node2.tp1.b2 sshd[1099]: Server listening on 0.0.0.0 port 22.
sept. 24 09:34:05 node2.tp1.b2 systemd[1]: Started OpenSSH server daemon.
sept. 24 09:34:50 node2.tp1.b2 sshd[1364]: Accepted password for mrkasparov from 192.168.1.1 port 52735 ssh2
```

- le pare-feu est configuré pour bloquer toutes les connexions exceptées celles qui sont nécessaires

*node1*

```
[mrkasparov@node1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: dhcpv6-client ssh
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
  
[root@node1 admin1]# nmap -sT node1.tp1.b2

Starting Nmap 6.40 ( http://nmap.org ) at 2020-09-24 10:36 CEST
Nmap scan report for node1.tp1.b2 (192.168.1.11)
Host is up (0.00023s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE
22/tcp open  ssh

Nmap done: 1 IP address (1 host up) scanned in 0.04 seconds
```


*node2*

```
[mrkasparov@node2 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: dhcpv6-client ssh
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
  
[root@node2 admin2]# nmap -sT node2.tp1.b2

Starting Nmap 6.40 ( http://nmap.org ) at 2020-09-24 10:35 CEST
Nmap scan report for node2.tp1.b2 (192.168.1.12)
Host is up (0.00028s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE
22/tcp open  ssh

Nmap done: 1 IP address (1 host up) scanned in 0.03 seconds
```

<br>

## I. Setup serveur Web

### 🌞 Installer le serveur web NGINX sur node1.tp1.b2 (avec une commande yum install).

```
[mrkasparov@node1 ~]$ sudo yum install nginx
```

### 🌞 Faites en sorte que :

- Les sites web doivent se trouver dans /srv/site1 et /srv/site2 (précédemment data1 et data2)

*Le premier site web dans data1*
```
[mrkasparov@node1 data1]$ pwd
/srv/data1
[mrkasparov@node1 data1]$ ls
index.html  lost+found
```

*Le second site web dans data2*
```
[mrkasparov@node1 data2]$ pwd
/srv/data2
[mrkasparov@node1 data2]$ ls
index.html  lost+found
```

- NGINX doit utiliser un utilisateur dédié et un groupe que vous avez créé à cet effet

```
[mrkasparov@node1 ~]$ sudo adduser adminweb
[sudo] Mot de passe de mrkasparov : 
[mrkasparov@node1 ~]$ sudo passwd adminweb
Changement de mot de passe pour l'utilisateur adminweb.
Nouveau mot de passe :
Retapez le nouveau mot de passe :
passwd : mise à jour réussie de tous les jetons d'authentification.
[root@node1 mrkasparov]# usermod -aG wheel adminweb

[adminweb@node1 data2]$ sudo groupadd groupe_nginx 
```

- Ces dossiers doivent appartenir à un utilisateur et un groupe spécifique et les permissions doivent être le plus restrictif possible

*Attribution d'utilisateur et groupe*

```
[adminweb@node1 ~]$ sudo chown adminweb:groupe_nginx /srv/data1/
[adminweb@node1 ~]$ sudo chown adminweb:groupe_nginx /srv/data1/index.html
[adminweb@node1 ~]$ sudo chown adminweb:groupe_nginx /srv/data2/
[adminweb@node1 ~]$ sudo chown adminweb:groupe_nginx /srv/data2/index.html
```

*Permissions les plus restrictive possible :*

*Sur les dossiers*

```
[adminweb@node1 srv]$ sudo chmod 511 data1
[sudo] Mot de passe de adminweb : 
[adminweb@node1 srv]$ ls -al
total 8
drwxr-xr-x.  4 root     root           32 24 sept. 11:46 .
dr-xr-xr-x. 17 root     root          244 22 sept. 12:15 ..
dr-x--x--x   3 adminweb groupe_nginx 4096 25 sept. 09:30 data1
drwxr-xr-x   3 adminweb groupe_nginx 4096 25 sept. 09:33 data2

[adminweb@node1 srv]$ sudo chmod 511 data2
[adminweb@node1 srv]$ ls -al
total 8
drwxr-xr-x.  4 root     root           32 24 sept. 11:46 .
dr-xr-xr-x. 17 root     root          244 22 sept. 12:15 ..
dr-x--x--x   3 adminweb groupe_nginx 4096 25 sept. 09:30 data1
dr-x--x--x   3 adminweb groupe_nginx 4096 25 sept. 09:33 data2
```

```
[adminweb@node1 srv]$ ls -al
total 8
drwxr-xr-x.  4 root     root           32 24 sept. 11:46 .
dr-xr-xr-x. 17 root     root          244 22 sept. 12:15 ..
dr-x--x--x   3 adminweb groupe_nginx 4096 25 sept. 09:30 data1
dr-x--x--x   3 adminweb groupe_nginx 4096 25 sept. 09:33 data2
```

*Site 1 (/srv/data1)*

```
[adminweb@node1 data1]$ sudo chmod 644 index.html
[adminweb@node1 data1]$ ls -al
total 24
drwxr-xr-x  3 adminweb groupe_nginx  4096 25 sept. 09:30 .
drwxr-xr-x. 4 root     root            32 24 sept. 11:46 ..
-rw-r--r--  1 adminweb groupe_nginx   103 25 sept. 09:34 index.html
drwx------  2 root     root         16384 24 sept. 11:44 lost+found
```

*Site 2 (/srv/data2)*

```
[adminweb@node1 data2]$ sudo chmod 644 index.html
[adminweb@node1 data2]$ ls -al
total 24
drwxr-xr-x  3 adminweb groupe_nginx  4096 25 sept. 09:33 .
drwxr-xr-x. 4 root     root            32 24 sept. 11:46 ..
-rw-r--r--  1 adminweb groupe_nginx   299 25 sept. 09:34 index.html
drwx------  2 root     root         16384 24 sept. 11:44 lost+found
```

*nginx.conf et nginx.default*

```
[mrkasparov@node1 nginx]$ sudo chown adminweb:groupe_nginx nginx.conf nginx.conf.default
[mrkasparov@node1 nginx]$ ls -al
total 84
drwxr-xr-x   4 root     root         4096 24 sept. 12:29 .
drwxr-xr-x. 76 root     root         8192 25 sept. 12:04 ..
drwxr-xr-x   2 root     root            6  3 oct.   2019 conf.d
drwxr-xr-x   2 root     root            6  3 oct.   2019 default.d
-rw-r--r--   1 root     root         1077  3 oct.   2019 fastcgi.conf
-rw-r--r--   1 root     root         1077  3 oct.   2019 fastcgi.conf.default
-rw-r--r--   1 root     root         1007  3 oct.   2019 fastcgi_params
-rw-r--r--   1 root     root         1007  3 oct.   2019 fastcgi_params.default
-rw-r--r--   1 root     root         2837  3 oct.   2019 koi-utf
-rw-r--r--   1 root     root         2223  3 oct.   2019 koi-win
-rw-r--r--   1 root     root         5231  3 oct.   2019 mime.types
-rw-r--r--   1 root     root         5231  3 oct.   2019 mime.types.default
-rw-r--r--   1 adminweb groupe_nginx 2471  3 oct.   2019 nginx.conf
-rw-r--r--   1 adminweb groupe_nginx 2656  3 oct.   2019 nginx.conf.default
-rw-r--r--   1 root     root          636  3 oct.   2019 scgi_params
-rw-r--r--   1 root     root          636  3 oct.   2019 scgi_params.default
-rw-r--r--   1 root     root          664  3 oct.   2019 uwsgi_params
-rw-r--r--   1 root     root          664  3 oct.   2019 uwsgi_params.default
-rw-r--r--   1 root     root         3610  3 oct.   2019 win-utf
```

- Les sites doivent être servis en HTTPS sur le port 443 et en HTTP sur le port 80


*fichier de configuration nginx*

```
[adminweb@node1 ~]$ sudo nano /etc/nginx/nginx.conf

# For more information on configuration, see:
#   * Official English Documentation: http://nginx.org/en/docs/
#   * Official Russian Documentation: http://nginx.org/ru/docs/

user adminweb;
worker_processes auto;
error_log /var/log/nginx/error.log;
pid /run/nginx.pid;

events {
    worker_connections 1024;
}

http {
    server {
        listen       80;
        server_name  node1.tp1.b2;

        location / {
            return 301 /data1;
        }

        location /data1 {
            alias /srv/data1;
        }

        location /data2 {
            alias /srv/data2;
        }
    }

    server {
        listen       443 ssl;
        server_name  node1.tp1.b2;

        ssl_certificate "/etc/pki/nginx/server.crt";
        ssl_certificate_key /etc/pki/nginx/private/server.key;

        location / {
            return 301 /data1;
        }

        location /data1 {
            alias /srv/data1;
        }
        location /data2 {                                                                                                    alias /srv/data2;                                                                                            }                                                                                                            }                                                                                                                                                                                                                                 server {                                                                                                             listen       443 ssl;                                                                                            server_name  node1.tp1.b2;                                                                                                                                                                                                        ssl_certificate "/etc/pki/nginx/server.crt";                                                                     ssl_certificate_key /etc/pki/nginx/private/server.key;                                                                                                                                                                            location / {                                                                                                         return 301 /data1;                                                                                           }                                                                                                                                                                                                                                 location /data1 {                                                                                                    alias /srv/data1;                                                                                            }                                                                                                                                                                                                                                 location /data2 {                                                                                                    alias /srv/data2;                                                                                            }                                                                                                            }                                                                                                            }                          
```

*Génération des clefs SSL*

```
[adminweb@node1 nginx]$ openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt 

[adminweb@node1 nginx]$ sudo mv server.key /etc/pki/nginx/private

[adminweb@node1 nginx]$ ls
private  server.crt

[adminweb@node1 nginx]$ cd private/
[adminweb@node1 private]$ ls
server.key
```


*ajout d'une nouvelle règle firewall*

```
[adminweb@node1 ~]$ sudo firewall-cmd --add-port=443/tcp --permanent
success
[adminweb@node1 ~]$ sudo firewall-cmd --reload
success
[adminweb@node1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: dhcpv6-client ssh
  ports: 22/tcp 443/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```


*Test http depuis node2*

```
[mrkasparov@node2 ~]$ curl node1.tp1.b2 -L
<!DOCTYPE html>

<html>

<head> Serveur Web </head>

<body>
<h1> Le site n°1 </h1>
</body>

</html>
```

*Test https depuis node2*

```
[mrkasparov@node2 ~]$ curl -kL https://node1.tp1.b2:443
<!DOCTYPE html>

<html>

<head> Serveur Web </head>

<body>
<h1> Le site n°1 </h1>
</body>

</html>
```

<br>

## II. Script de sauvegarde


### 🌞 Script

```
#!/bin/bash

DATE=$(date +%y-%m-%d-%H-%M-%S)
REP_SITE="$1"
NOM_SITE=${REP_SITE:5}

save() {
        tar zcvf "/srv/save_web/${NOM_SITE}_${DATE}.tar.gz" ${NOM_SITE}
}

save_limit() {
        find /srv/save_web -type f | wc -l | awk '{print $1}'
}

COUNT=$(save_limit)
OLD=$(ls -t /srv/save_web | tail -1)
LIMIT="7"

if [ "$COUNT" -lt "$LIMIT" ]
then
        echo "Sauvegarde en cours..."
        save
        echo "Sauvegarde effectué"
else
        echo "Limite atteinte... Suppression de la plus ancienne sauvegarde"
        rm /srv/save_web/$OLD
        save
        echo "Sauvegarde effectué"
fi
```


#### Doit s'exécuter sous l'identité d'un utilisateur dédié appelé backup

```
[adminweb@node1 ~]$ sudo adduser backup
[adminweb@node1 ~]$ sudo passwd backup
Changement de mot de passe pour l'utilisateur backup.
Nouveau mot de passe :
Retapez le nouveau mot de passe :
passwd : mise à jour réussie de tous les jetons d'authentification.
[adminweb@node1 ~]$ sudo usermod -aG wheel backup
```


#### Doit posséder des permissions minimales à son bon fonctionnement

```
[adminweb@node1 srv]$ ls -l
total 12
dr-x--x--x 3 adminweb groupe_nginx 4096 25 sept. 09:30 data1
dr-x--x--x 3 adminweb groupe_nginx 4096 25 sept. 09:33 data2
drwxr-xr-x 2 root     root          272 28 sept. 14:23 save_web
-rw-r--r-- 1 root     root          554 28 sept. 14:33 tp1_backup.sh

[adminweb@node1 srv]$ sudo groupadd backup_group
[sudo] Mot de passe de adminweb :
[adminweb@node1 srv]$ sudo chown backup:backup_group /srv/tp1_backup.sh
[adminweb@node1 srv]$ sudo chown backup:backup_group /srv/save_web
[adminweb@node1 srv]$ sudo usermod -a -G groupe_nginx backup

[adminweb@node1 srv]$ ls -l
total 12
dr-x--x--x 3 adminweb groupe_nginx 4096 25 sept. 09:30 data1
dr-x--x--x 3 adminweb groupe_nginx 4096 25 sept. 09:33 data2
drwxr-xr-x 2 backup   backup_group  272 28 sept. 14:23 save_web
-rw-r--r-- 1 backup   backup_group  554 28 sept. 14:33 tp1_backup.sh

[adminweb@node1 srv]$ sudo chmod 774 data1
[adminweb@node1 srv]$ sudo chmod 774 data2
[backup@node1 srv]$ sudo chmod 700 save_web/
[backup@node1 srv]$ sudo chmod 640 tp1_backup.sh

[backup@node1 srv]$ ls -l
total 12
drwxrwxr-- 3 adminweb groupe_nginx 4096 25 sept. 09:30 data1
drwxrwxr-- 3 adminweb groupe_nginx 4096 25 sept. 09:33 data2
drwx------ 2 backup   backup_group  272 28 sept. 15:16 save_web
-rw-r----- 1 backup   backup_group  554 28 sept. 14:33 tp1_backup.sh
```


#### Ne doit comporter AUCUNE commande sudo

```
[backup@node1 save_web]$ ls
data1_20-09-28-15-09-38.tar.gz  data1_20-09-28-15-45-45.tar.gz  data2_20-09-28-15-51-42.tar.gz
data1_20-09-28-15-12-48.tar.gz  data1_20-09-28-15-49-13.tar.gz
data1_20-09-28-15-16-24.tar.gz  data2_20-09-28-15-51-24.tar.gz

[backup@node1 srv]$ sh tp1_backup.sh /srv/data2
Limite atteinte... Suppression de la plus ancienne sauvegarde
rm : supprimer fichier (protégé en écriture) « /srv/save_web/data1_20-09-28-15-09-38.tar.gz » ? o
tar: Suppression de « / » au début des noms des membres
/srv/data2/
/srv/data2/index.html
/srv/data2/lost+found/
Sauvegarde effectué
tar: Suppression de « / » au début des noms des membres
/srv/data2/
/srv/data2/index.html
/srv/data2/lost+found/
7

[backup@node1 srv]$ cd save_web/
[backup@node1 save_web]$ ls
data1_20-09-28-15-12-48.tar.gz  data1_20-09-28-15-49-13.tar.gz  data2_20-09-28-15-51-58.tar.gz
data1_20-09-28-15-16-24.tar.gz  data2_20-09-28-15-51-24.tar.gz
data1_20-09-28-15-45-45.tar.gz  data2_20-09-28-15-51-42.tar.gz
```


### 🌞 Utiliser la crontab pour que le script s'exécute automatiquement toutes les heures

```
[backup@node1 srv]$ crontab -l
00 * * * * sh /srv/tp1_backup.sh /srv/data1
00 * * * * sh /srv/tp1_backup.sh /srv/data2
```

### 🌞 Prouver que vous êtes capables de restaurer un des sites dans une version antérieure, et fournir une marche à suivre pour restaurer une sauvegarde donnée

*Suppression puis récupération du fichier toto*

```
[backup@node1 srv]$ cd data1
[backup@node1 data1]$ touch toto
[backup@node1 data1]$ ls
index.html  lost+found  toto
[backup@node1 data1]$ cd ..
[backup@node1 srv]$ sh tp1_backup.sh /srv/data1
Sauvegarde en cours...
data1/
data1/toto
data1/lost+found/
data1/index.html
Sauvegarde effectué
[backup@node1 srv]$ cd data1
[backup@node1 data1]$ ls
index.html  lost+found  toto
[backup@node1 data1]$ rm toto
[backup@node1 data1]$ cd ..
[backup@node1 srv]$ ls
data1  data2  save_web  tp1_backup.sh
[backup@node1 srv]$ cd save_web/
[backup@node1 save_web]$ ls
data1_20-09-28-18-49-19.tar.gz
[backup@node1 save_web]$ sudo tar -xzvf data1_20-09-28-18-49-19.tar.gz -C /srv/
[sudo] Mot de passe de backup : 
data1/
data1/toto
data1/lost+found/
data1/index.html
[backup@node1 save_web]$ cd ..
[backup@node1 srv]$ cd data1
[backup@node1 data1]$ ls
index.html  lost+found  toto
```

<br>

## III. Monitoring, alerting

### 🌞 Mettre en place l'outil Netdata en suivant les instructions officielles et s'assurer de son bon fonctionnement.

*Installation de Netdata*

```
[backup@node1 srv]$ bash <(curl -Ss https://my-netdata.io/kickstart.sh) 

 --- We are done! ---

  ^
  |.-.   .-.   .-.   .-.   .-.   .  netdata                          .-.   .-
  |   '-'   '-'   '-'   '-'   '-'   is installed and running now!  -'   '-'
  +----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+--->

  enjoy real-time performance and health monitoring...
```

### 🌞 Configurer Netdata pour qu'ils vous envoient des alertes dans un salon Discord dédié

```
[backup@node1 srv]$ sudo /etc/netdata/edit-config health_alarm_notify.conf
Copying '/usr/lib/netdata/conf.d/health_alarm_notify.conf' to '/etc/netdata/health_alarm_notify.conf' ...
Editing '/etc/netdata/health_alarm_notify.conf' ...

[...]

#------------------------------------------------------------------------------
# discord (discordapp.com) global notification options

# multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discordapp.com/api/webhooks/760166159986196480/UKo9bkGPF-dGiwzmvuqdnjs-jsZV_B6wwa46vMqNAth8DtGDMJxw-6DHt8OR4ysZskCi"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="alarm"


#------------------------------------------------------------------------------
```

*Test*

```
[backup@node1 srv]$ export NETDATA_ALARM_NOTIFY_DEBUG=1
[backup@node1 srv]$ /usr/libexec/netdata/plugins.d/alarm-notify.sh test

# SENDING TEST WARNING ALARM TO ROLE: sysadmin
2020-09-28 19:12:12: alarm-notify.sh: DEBUG: Loading config file '/usr/lib/netdata/conf.d/health_alarm_notify.conf'...
2020-09-28 19:12:12: alarm-notify.sh: DEBUG: Loading config file '/etc/netdata/health_alarm_notify.conf'...
2020-09-28 19:12:12: alarm-notify.sh: DEBUG: Cannot find aws command in the system path.  Disabling Amazon SNS notifications.
--- BEGIN curl command ---
/usr/bin/curl -X POST --data-urlencode $'payload=        {\n            "channel": "#alarm",\n            "username": "netdata on node1.tp1.b2",\n            "text": "node1.tp1.b2 needs attention, `test.chart` (_test.family_), *test alarm = new value*",\n            "icon_url": "https://registry.my-netdata.io/images/banner-icon-144x144.png",\n            "attachments": [\n                {\n                    "color": "warning",\n                    "title": "test alarm = new value",\n                    "title_link": "https://registry.my-netdata.io/goto-host-from-alarm.html?host=node1.tp1.b2&chart=test.chart&family=test.family&alarm=test_alarm&alarm_unique_id=1&alarm_id=1&alarm_event_id=1&alarm_when=1601313132",\n                    "text": "this is a test alarm to verify notifications work",\n                    "fields": [\n                        {\n                            "title": "test.chart",\n                            "value": "test.family"\n                        }\n                    ],\n                    "thumb_url": "https://registry.my-netdata.io/images/alert-128-orange.png",\n                    "footer_icon": "https://registry.my-netdata.io/images/banner-icon-144x144.png",\n                    "footer": "node1.tp1.b2",\n                    "ts": 1601313132\n                }\n            ]\n        }' https://discordapp.com/api/webhooks/760166159986196480/UKo9bkGPF-dGiwzmvuqdnjs-jsZV_B6wwa46vMqNAth8DtGDMJxw-6DHt8OR4ysZskCi/slack
--- END curl command ---
--- BEGIN received response ---
ok
--- END received response ---
RECEIVED HTTP RESPONSE CODE: 200
2020-09-28 19:12:16: alarm-notify.sh: INFO: sent discord notification for: node1.tp1.b2 test.chart.test_alarm is WARNING to 'alarm'
--- BEGIN sendmail command ---
/usr/sbin/sendmail -t
--- END sendmail command ---
2020-09-28 19:12:16: alarm-notify.sh: INFO: sent email notification for: node1.tp1.b2 test.chart.test_alarm is WARNING to 'root'
# OK

# SENDING TEST CRITICAL ALARM TO ROLE: sysadmin
2020-09-28 19:12:16: alarm-notify.sh: DEBUG: Loading config file '/usr/lib/netdata/conf.d/health_alarm_notify.conf'...
2020-09-28 19:12:16: alarm-notify.sh: DEBUG: Loading config file '/etc/netdata/health_alarm_notify.conf'...
2020-09-28 19:12:16: alarm-notify.sh: DEBUG: Cannot find aws command in the system path.  Disabling Amazon SNS notifications.
--- BEGIN curl command ---
/usr/bin/curl -X POST --data-urlencode $'payload=        {\n            "channel": "#alarm",\n            "username": "netdata on node1.tp1.b2",\n            "text": "node1.tp1.b2 is critical, `test.chart` (_test.family_), *test alarm = new value*",\n            "icon_url": "https://registry.my-netdata.io/images/banner-icon-144x144.png",\n            "attachments": [\n                {\n                    "color": "danger",\n                    "title": "test alarm = new value",\n                    "title_link": "https://registry.my-netdata.io/goto-host-from-alarm.html?host=node1.tp1.b2&chart=test.chart&family=test.family&alarm=test_alarm&alarm_unique_id=1&alarm_id=1&alarm_event_id=2&alarm_when=1601313136",\n                    "text": "this is a test alarm to verify notifications work",\n                    "fields": [\n                        {\n                            "title": "test.chart",\n                            "value": "test.family"\n                        }\n                    ],\n                    "thumb_url": "https://registry.my-netdata.io/images/alert-128-red.png",\n                    "footer_icon": "https://registry.my-netdata.io/images/banner-icon-144x144.png",\n                    "footer": "node1.tp1.b2",\n                    "ts": 1601313136\n                }\n            ]\n        }' https://discordapp.com/api/webhooks/760166159986196480/UKo9bkGPF-dGiwzmvuqdnjs-jsZV_B6wwa46vMqNAth8DtGDMJxw-6DHt8OR4ysZskCi/slack
--- END curl command ---
--- BEGIN received response ---
ok
--- END received response ---
RECEIVED HTTP RESPONSE CODE: 200
2020-09-28 19:12:20: alarm-notify.sh: INFO: sent discord notification for: node1.tp1.b2 test.chart.test_alarm is CRITICAL to 'alarm'
--- BEGIN sendmail command ---
/usr/sbin/sendmail -t
--- END sendmail command ---
2020-09-28 19:12:20: alarm-notify.sh: INFO: sent email notification for: node1.tp1.b2 test.chart.test_alarm is CRITICAL to 'root'
# OK

# SENDING TEST CLEAR ALARM TO ROLE: sysadmin
2020-09-28 19:12:20: alarm-notify.sh: DEBUG: Loading config file '/usr/lib/netdata/conf.d/health_alarm_notify.conf'...
2020-09-28 19:12:20: alarm-notify.sh: DEBUG: Loading config file '/etc/netdata/health_alarm_notify.conf'...
2020-09-28 19:12:20: alarm-notify.sh: DEBUG: Cannot find aws command in the system path.  Disabling Amazon SNS notifications.
--- BEGIN curl command ---
/usr/bin/curl -X POST --data-urlencode $'payload=        {\n            "channel": "#alarm",\n            "username": "netdata on node1.tp1.b2",\n            "text": "node1.tp1.b2 recovered, `test.chart` (_test.family_), *test alarm (alarm was raised for 3 seconds)*",\n            "icon_url": "https://registry.my-netdata.io/images/banner-icon-144x144.png",\n            "attachments": [\n                {\n                    "color": "good",\n                    "title": "test alarm (alarm was raised for 3 seconds)",\n                    "title_link": "https://registry.my-netdata.io/goto-host-from-alarm.html?host=node1.tp1.b2&chart=test.chart&family=test.family&alarm=test_alarm&alarm_unique_id=1&alarm_id=1&alarm_event_id=3&alarm_when=1601313140",\n                    "text": "this is a test alarm to verify notifications work",\n                    "fields": [\n                        {\n                            "title": "test.chart",\n                            "value": "test.family"\n                        }\n                    ],\n                    "thumb_url": "https://registry.my-netdata.io/images/check-mark-2-128-green.png",\n                    "footer_icon": "https://registry.my-netdata.io/images/banner-icon-144x144.png",\n                    "footer": "node1.tp1.b2",\n                    "ts": 1601313140\n                }\n            ]\n        }' https://discordapp.com/api/webhooks/760166159986196480/UKo9bkGPF-dGiwzmvuqdnjs-jsZV_B6wwa46vMqNAth8DtGDMJxw-6DHt8OR4ysZskCi/slack
--- END curl command ---
--- BEGIN received response ---
ok
--- END received response ---
RECEIVED HTTP RESPONSE CODE: 200
2020-09-28 19:12:29: alarm-notify.sh: INFO: sent discord notification for: node1.tp1.b2 test.chart.test_alarm is CLEAR to 'alarm'
--- BEGIN sendmail command ---
/usr/sbin/sendmail -t
--- END sendmail command ---
2020-09-28 19:12:29: alarm-notify.sh: INFO: sent email notification for: node1.tp1.b2 test.chart.test_alarm is CLEAR to 'root'
# OK
```

![](./img/screen_1.png)
